## Javascript
---
![javascript](/file/files/js.png)  

Es un lenguaje que principalmente esta destinado al desarrollo de elementos web interactivos y que tienen como fin mejorar la experiencia del usuario de una aplicación web. Actualmente se mantiene en el top 3 de los lenguajes más populares del mundo y uno de los lenguajes con más documentación y extensiones en el mercado, por lo que también es muy conveniente a la hora de reutilizar código.

Entre otras características y ventajas que llevaron a la selección del lenguaje es su velocidad ya que se ejecuta inmediatamente en el navegador; su simplicidad de sintaxis; su compatibilidad con cualquier página web; su característica de client-side; la cantidad de frameworks para la creación de interfaces sencillas; su versatilidad con el manejo de métodos y sus constantes actualizaciones desde ECMAScript 5.

Javascript será el lenguaje predominante en el desarrollo del frontend y del backend.



## Python
---
![python](/file/files/python.png)  

Es un lenguaje de programación interpretado y ampliamente utilizado para el desarrollo de aplicaciones web, software, ciencia de datos y proyectos de Machine Learning. Debido a su amplia documentación y campos de trabajo es una de las mejores opciones del mercado para desarrollo de proyectos. 

Actualmente, y según varias encuestas, ej. Stackoverflow, es el lenguaje más popular y utilizado del mundo. Entre las ventajas principales que llevaron a su selección es que es multiparadigma y multipropósito; posee una amplia colección de bibliotecas y frameworks; su propia biblioteca estándar es bastante extensa y posee gran cantidad y variedad de módulos integrados; Python es compatible con todos los sistemas operativos del mercado; su curva de aprendizaje es bastante baja; posee una comunidad muy extensa y sobre todo es open source.

Python será el lenguaje seleccionado para desarrollar el microservicio extra.
