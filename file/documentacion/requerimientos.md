### Requerimientos funcionales
 
---
#### Requisitos administradores
---
__RQF001__ – El administrador podrá dar de alta, baja, congelar, visualizar y actualizar cuentas de usuarios

__RQF002__ – El sistema solicitará una descripción de la acción realizada por el administrador en las cuentas de los usuarios

__RQF003__ – El sistema almacenará un historial de las acciones realizadas por los usuarios de tipo administrador

__RQF004__ – El sistema implementará credenciales de acceso para las páginas que compongan la aplicación

__RQF005__ – El sistema le permitirá al administrador visualizar un área de reportes con 11 opciones disponibles de reportes: Usuarios Suscritos a X equipo, Usuario Con o Sin Membresía, Usuarios que Mas membresías han adquirido, Usuarios que más dinero han gastado, Usuarios de X País, Usuarios de X género, Usuarios con al menos X años de edad, Empleados que MAS/MENOS noticias han publicado, Empleados que MAS/MENOS noticias han publicado de X Equipo, Bitácoras de los administradores

__RQF006__ – El sistema le permitirá al administrador filtrar datos en el área de reportes para personalizar estos mismos

__RQF007__ – El sistema le permitirá al administrador generar pdf a partir de los reportes

__RQF008__ – El sistema le permitirá al administrador modificar (crear, eliminar, ver y modificar) información estadística de: estadios, jugadores, equipos, técnicos, partidos, competencias, árbitros, incidencias

__RQF009__ - El sistema permitirá al usuario administrador gestionar las configuraciones del sistema 

__RQF010__ - El sistema permite al usuario crear quinielas


---
#### Requisitos empleados
---

__RQF011__ – El sistema le permitirá a los empleados modificar (crear, eliminar, ver y modificar) información estadística de: estadios, jugadores, equipos, técnicos, partidos, competencias, árbitros, incidencias

__RQF012__ – El sistema les permitirá a los empleados transferir jugadores entre equipos

__RQF013__ – El sistema permitirá al usuario ver el historial de equipos de cada jugador

__RQF014__ – El sistema les permitirá a los empleados transferir técnicos entre equipos

__RQF015__ – El sistema permitirá al usuario manejar el historial de equipos de cada técnico

__RQF016__ – El sistema les permitirá a los empleados cambiar los estados de los partidos (Sin iniciar, en curso, finalizado, suspendido)

__RQF017__ – El sistema les permitirá a los empleados agregar incidencias a los partidos que se encuentren en estado “en curso”

__RQF018__ – El sistema les permitirá a los empleados cambiar los estados de los jugadores y los técnicos que se encuentren en un partido con estado “en curso”

__RQF019__ – El sistema les permitirá a los usuarios publicar noticias de equipos

__RQF020__ - El sistema debe permitir postear noticias

__RQF021__ - El sistema debe permitir editar noticias

__RQF022__ - El sistema debe permitir eliminar noticias

__RQF023__ - El sistema debe ser capaz de mostrar las noticias de acuerdo con las subscripciones de los usuarios

__RQF024__ - El sistema permite al usuario crear, modificar, eliminar y ver datos deportivos e información relacionada

__RQF025__ - El sistema permite a los empleados y administradores ingresar información estadística a la base de datos (estadios, jugadores, técnicos, equipos, etc)

__RQF026__ -  El sistema permitirá al usuario empleado gestionar las estadísticas recopiladas 

---
#### Requisitos clientes sin membresía
---

__RQF027__ – El sistema les permitirá a los clientes con membresía “gratuita” visualizar la información de los partidos

__RQF028__ – El sistema les permitirá a los clientes con membresía “gratuita” filtrar la información de los partidos a visualizar por estado

__RQF029__ – El sistema les permitirá a los clientes con membresía “gratuita” modificar la información de su usuario a excepción del correo electrónico

__RQF030__ – El sistema les permitirá a los clientes con membresía “gratuita” visualizar el historial de sus membresías

__RQF031__ – El sistema les permitirá a los clientes con membresía “gratuita” adquirir una membresía

__RQF032__ - El sistema permitirá al usuario cliente acceder a las estadísticas y verificar datos 

__RQF033__ - El sistema permite al usuario tener la opción de cerrar sesión una vez logueado

__RQF034__ - El sistema permite al usuario ver su nombre de usuario una vez logueado

---
#### Requisitos clientes con membresía
---

__RQF035__ - El sistema permite al usuario consultar predicciones, media vez cuente con una membresía 
Dichas predicciones son realizadas respecto al posible ganador entre dos equipos ingresados

__RQF036__ - El sistema permite al usuario realizar quinielas

__RQF037__ - El sistema permite al usuario modificar quinielas realizadas

__RQF038__ - Como cliente con membresía puedo ver el estado de mi membresía

__RQF039__ -  Como cliente con membresía puedo darme de baja

__RQF040__ -  Como cliente con membresía puedo pagar el próximo mes

__RQF041__ -  Como cliente con membresía puedo ver el historial de mis membresías anteriores

__RQF042__ -  Como cliente con membresía quiero ver resultados y hacer consultas

__RQF043__ -  Como cliente con membresía puedo seguir a mis equipos favoritos

__RQF044__ - Como cliente con membresía puedo consultar datos estadísticos

__RQF045__ - Como cliente con membresía puedo filtrar las noticias de los equipos

__RQF046__ - Como cliente con membresía puedo ver todas las noticias

__RQF047__ - Como cliente con membresía puede utilizar las quinielas de un partido

---
#### Requerimientos General de Usuarios
---

__RQF048__ - El sistema debe permitir actualizar la información propia de cada usuario

__RQF049__ - El sistema debe permitir tener fotografia para el perfil del usuario

---
#### Requerimientos no funcionales
---
__RQNF001__ – El sistema debe de ser responsive

__RQNF002__ – El sistema debe de ser desarrollado utilizando IaC

__RQNF003__ – El sistema debe de estar implementado con microservicios

__RQNF004__ – El proyecto debe de llevar un control de versionamiento utilizando buenas prácticas como Gitflow

__RQNF005__ – Las funcionalidades de los microservicios deben de ser independientes entre los microservicios

__RQNF006__ – Las rutas de los microservicios deben de estar especificadas utilizando Swagger

__RQNF007__ – El sistema debe contar con un servidor JWT para la autenticación de usuarios

__RQNF008__ – El sistema debe de estar desplegado en Kubernetes utilizanado contenedores

__RQNF009__ – La documentación del proyecto debe de redactarse en formato Markdown

__RQNF010__ – El sistema debe pasar por un proceso de DevOps

__RQNF011__ – El sistema debe de implementar pruebas unitarias

__RQNF012__ – El desarrollo del sistema debe de plasmarse en una herramienta para el manejo de tickets

__RQNF013__ - El sistema deberá registrar tres roles de usuarios

__RQNF014__ - El sistema deberá tener la capacidad de rediseñar las dimensiones de la página web dependiendo las dimensiones del dispositivo de donde es visitada

__RQNF015__ - El sistema deberá de tener la capacidad de si un servicio falla, las funcionalidades ajenas a dicho servicio deben seguir funcionando

__RQNF016__ - El sistema deberá de tener la capacidad de conectarse a diferentes microservicios

__RQNF017__ - El sistema deberá contar con un servidor de tokens para poder autenticar los servicios y asi garantizar la seguridad

__RQNF018__ - El sistema deberá de contar con un sistema de pruebas automáticas antes de subir cambios a producción 

__RQNF019__ - El sistema debería de tener la capacidad de ejecutarse en cualquier sistema operativo. 

__RQFN020__ - El sistema valida que no se viole ninguna regla para la integridad de datos en caso de se desee realizar alguna eliminación de alguno de estos. Mostrará una alerta en caso de que esto ocurra

__RQFN021__ - El sistema permite otorgar a los usuarios ganadores de quiniela una membresía de 2 meses

__RQFN022__ - El sistema permite bloquear el ingreso o modificación de quinielas realizadas sobre partidos ya iniciados o finalizados

__RQFN023__ - El sistema mantiene un constante monitoreo y actualización de predicciones según los datos ingresados

__RQNF024__ - El sistema debe ser presentado basado en los principios de UIX

__RQNF025__ - El sistema debe presentar los datos de manera consistente

__RQNF026__ - El sistema debe ser capaz de manejar los datos sensibles de manera encriptada

__RQNF027__ - El Sistema debe ser capaz de redirigir a la página de acceso denegado al no tener permisos de acceso.
