# Herramientas de control de tiempo de trabajo 

## Trello

Trello es una herramienta de gestión de proyectos que permite organizar tareas en listas y sub-listas. Permite crear tarjetas con información de cada tarea, asignarlas a usuarios, adjuntar archivos, etc.

### Tableros

Un tablero es un espacio de trabajo donde puedes organizar tareas en listas. Puedes crear tantos tableros como quieras.

### Listas

las diferetes fases de una tarea. Empieza con algo sencillo como Pendiente, En progreso y Terminado. Luego, añade listas personalizadas para cada proyecto. 

### Tarjetas

Cada tarjeta representa una tarea. Puedes añadir información, adjuntar archivos, asignar tarjetas a usuarios y mucho más.  

### Miembros

Los miembros son los usuarios que trabajan en tu tablero. Puedes añadirlos a tu tablero y asignarles tarjetas.

### Etiquetas

Las etiquetas te permiten clasificar las tarjetas de tu tablero. Puedes crear etiquetas personalizadas para cada proyecto.

### Comentarios

Los comentarios te permiten comunicarte con los miembros de tu tablero. Puedes añadir comentarios a las tarjetas y adjuntar archivos.

### Filtros

Los filtros te permiten ver solo las tarjetas que te interesan. Puedes crear filtros personalizados para cada proyecto.

### Notificaciones

Las notificaciones te permiten mantenerte al día con los cambios en tu tablero. Puedes elegir qué notificaciones quieres recibir y cómo quieres recibirlas.

### Fechas de vencimiento

Las fechas de vencimiento te permiten establecer una fecha límite para las tarjetas de tu tablero. Puedes elegir si quieres que se te notifique cuando una tarjeta esté a punto de vencer.

### Archivos adjuntos

Puedes adjuntar archivos a las tarjetas de tu tablero. Puedes adjuntar archivos de hasta 250 MB.
