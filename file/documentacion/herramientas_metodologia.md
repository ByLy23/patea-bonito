# Herramientas de metodologia

---

## Scrumban

es una metodología de gestión de proyectos que combina dos estrategias ágiles comunes: Scrum y Kanban. Scrumban se desarrolló inicialmente para ayudar a los equipos en su transición de **Scrum** a **Kanban** o viceversa. Si los equipos tienen más experiencia con una de las estrategias que con la otra, esta técnica les servirá para cambiar gradualmente de una metodología a la otra.

### Funciones de Scrum en Scrumban

- La iteracion de los provesos se produce en intervalos constantes, normalmente al final de los sprints en las reuniones retrospectivas y de revisión del sprint.
- El trabajo se prioriza según la complejidad de la tarea y la demanda del producto.
- El equipo entero se pone de acuerdo y se alinea con respecto a qué significará “trabajo terminado”, para que todos entiendan qué implica finalizar una tarea. De este modo, los resultados finales quedan claramente plasmados y definidos.

### Funciones de Kanban en Scrumban

- Se trabaja con un conjunto claro de tareas que se deben realizar. Una vez que un miembro del equipo empieza a trabajar con una tarea, la extrae del backlog y la ingresa a su propia carga de trabajo actual.
- Hay límites estrictos con respecto a la cantidad de tareas en progreso, para evitar el exceso de trabajo en el equipo.
- Por lo general, las tareas se representan con tarjetas que avanzan a través de diferentes etapas del proceso en un tablero Kanban.

### Funciones Exclusivas de Scrumban

- No hay jerarquías. Es decir, todos los integrantes del equipo de desarrolladores tienen las mismas oportunidades que cualquier otra persona para elegir o tomar decisiones. Además, el grupo no tiene un líder claro. Más bien, el equipo se gestiona solo.
- Los proyectos Scrumban no tienen que tener necesariamente un vencimiento. Los sprints, por lo general, se trabajan en períodos incrementales de dos semanas, para que los miembros del equipo se puedan centrar en las tareas de cada sprint específico, hasta que llegue el momento de la revisión y de reiterar. Esto es lo que convierte a Scrumban en una excelente opción para proyectos a largo plazo o proyectos con un objetivo ambiguo.

### Implementacion de Scrumban

- Se implementaran formas para calcular los pesos de las tareas a realizar con la metodologia de Poker Planning, con la cual se puede identificar por medio del grupo de desarrollo la dificultad de las tareas a realizar.
- Daily meetings para poder establecer tareas diarias ademas de ver si alguien esta bloqueado con alguna tarea y como se puede resolver.
- Sprints de entregas continuas, con los cuales podemos dar valor a nuestra aplicacion.
- Tablero de tareas utilizando la herramienta Trello, para colocar los tickets de las tareas pendientes, bloqeadas, terminadas y en fase de pruebas.

---

# Gitflow

Gitflow es un flujo de trabajo de Git heredado que fue en origen una estrategia innovadora y revolucionaria para gestionar ramas de Git. Gitflow ha perdido popularidad en beneficio de los flujos de trabajo basados en troncos, que ahora se consideran prácticas recomendadas para el desarrollo continuo de software y las prácticas de DevOps. Además, puede ser difícil utilizar Gitflow con CI/CD. Esta entrada presenta el uso de Gitflow con fines de historial.

## Funcionamiento

### Ramas principales y de desarrollo

En lugar de una única rama main, este flujo de trabajo utiliza dos ramas para registrar el historial del proyecto. La rama main o principal almacena el historial de publicación oficial y la rama develop o de desarrollo sirve como rama de integración para las funciones. Asimismo, conviene etiquetar todas las confirmaciones de la rama main con un número de versión.

![Gitflow 1](imagenes/gitflow1.jpeg)

### Ramas de funcion

Todas las funciones nuevas deben residir en su propia rama, que se puede enviar al repositorio central para copia de seguridad/colaboración. Sin embargo, en lugar de ramificarse de main, las ramas feature utilizan la rama develop como rama primaria. Cuando una función está terminada, se vuelve a fusionar en develop. Las funciones no deben interactuar nunca directamente con main.

![Gitflow 2](imagenes/gitflow2.jpeg)

### Ramas de publicacion

Cuando develop haya adquirido suficientes funciones para una publicación (o se acerque una fecha de publicación predeterminada), debes bifurcar una rama release (o de publicación) a partir de develop. Al crear esta rama, se inicia el siguiente ciclo de publicación, por lo que no pueden añadirse nuevas funciones una vez pasado este punto (en esta rama solo deben producirse las soluciones de errores, la generación de documentación y otras tareas orientadas a la publicación).

![Gitflow 3](imagenes/gitflow3.jpeg)

### Ramas de correccion

Las ramas de mantenimiento, de corrección o de hotfix sirven para reparar rápidamente las publicaciones de producción. Las ramas hotfix son muy similares a las ramas release y feature, salvo por el hecho de que se basan en la rama main y no en la develop. Esta es la única rama que debería bifurcarse directamente a partir de main. Cuando se haya terminado de aplicar la corrección, debería fusionarse en main y develop (o la rama release actual), y main debería etiquetarse con un número de versión actualizado.

![Gitflow 4](imagenes/gitflow4.jpeg)
