# Herramientas de desarrollo a utilizar

> ## React:

    Para el desarrollo de la interfaz de usuario se utilizó React, la cual es una biblioteca de JavaScript, que sirve para la construcción de interfaces de usuario. 

    A continuación se presentan algunas características y razones por las que se consideró esta herramienta como una buena solución para el desarrollo del proyecto. 

- > ### Características

  - #### _Virtual DOM:_
    Esta característica permite a la aplicación web tener mayor rapidez en la carga de las páginas y facilita el mantennimiento de la aplicación. Esto es debido a que a la hora de realizar cambios, mediante el uso de DOM virtual permite visualizar los cambios de los datos sin necesidad de renderizar toda la página, sino que solamente el componente que se ha actualizado. 

  - #### _Isomorfismo:_
    Es la capacidad de ejecutar el código tanto en el cliente como el servidor. También se conoce como "Javascript Universal". Sirve principalmente para solucionar problemas de posicionamiento tradicionales de las aplicaciones Javascript.


  - #### _Integración con Redux:_
    Esta opción permite conseguir mayor funcionalidad y facilitar el desarrollo. Esto debido a que cada componente React tiene su propio estado, los cuales se van modificando a lo largo de su ciclo de vida y mediante el uso de Redux se puede emitir actualizaciones de los estados en respuesta a acciones. 

  - #### _Utilizando Material:_
    Material UI es un marco front-end de código abierto para componentes React. Esta herramienta permite facilitar la navegación a través del marco, también recibe actualizaciones periódicas, por lo que los componentes consisten en diversos diseños y colores, que permiten obtener una aplicación estéticamente atractiva y adaptable, dado que los componentes que ofrece este marco son responsive.  


- > ### Variantes Positivas
      Uno de los factores que hace muy atractivo a React es su versatilidad, ya que lo podemos encontrar como una herramienta para el desarrollo Web Front End (ReactJS). También hay que destacar la opción enfocada en el desarrollo móvil, conocida React Native, que permite crear Apps para iOS y Android.

    - #### _Programación Reactiva:_ 
      Esta tendencia en el mundo de la programación se basa en el flujo de datos asíncronos. Ha ganado gran aceptación en el último tiempo y muchas librerías y frameworks usan este concepto.

    - #### _Se basa en componentes:_
      La posibilidad de crear componentes en React es una de las claves que permiten mejor escalabilidad en las aplicaciones que se desarrollan. Con esta librería es posible crear desde componente muy simples hasta estructuras más complejas. Todo dependerá de nuestros conocimientos y de los requerimientos del proyecto.

    - #### _Tiene una Gran Comunidad:_
      Para el mundo Web esta es la librería que más crecimiento ha tenido en los últimos años. Un gran número de desarrolladores la utilizan en todo el planeta y esto hace que encontremos buen volumen de documentación, ejemplos y tutoriales. También es para destacar que esta librería es usado en el desarrollo de importantes sitios como Facebook e Instagram, entre otros.

    - #### _Evolución y Actualización:_
      Si pensamos entrar en el mundo de React es importante que tengamos en cuenta que no es un producto que se mantenga quieto. Y esta es una de las características que mantiene viva a esta librería, tiene actualizaciones frecuentes que corrigen y mejoran el producto. Esto también hace que los desarrolladores debamos estar al día con todas las novedades.



> ## Node - JS:

    Dado como un entorno de ejecución de JavaScript orientado a eventos asíncronos, Node.js está diseñado para crear aplicaciones network escalables. La idea principal de Node.js es usar el modelo de entrada y salida sin bloqueo y controlado por eventos para seguir siendo liviano y eficiente frente a las aplicaciones en tiempo real de uso de datos que se ejecutan en los dispositivos.

- > ### Características

  - #### _Velocidad:_
    Node.js está construido sobre el motor de JavaScript V8 de Google Chrome, por eso su biblioteca es muy rápida en la ejecución de código.

  - #### _Sin búfer:_
    Las aplicaciones de Node.js generan los datos en trozos (chunks), nunca los almacenan en búfer.
    Asíncrono y controlado por eventos.  Como hemos dicho anteriormente, las APIs de la biblioteca de Node.js son asíncronas, sin bloqueo. Un servidor basado en Node.js no espera que una API devuelva datos. El servidor pasa a la siguiente API después de llamarla, y un mecanismo de notificación de eventos ayuda al servidor a obtener una respuesta de la llamada a la API anterior

  - #### _Un subproceso escalable:_
    Node.js utiliza un modelo de un solo subproceso con bucle de eventos. Gracias al mecanismo de eventos, el servidor responde sin bloqueos, como hemos dicho. Esto hace que el servidor sea altamente escalable comparando con los servidores tradicionales como el Servidor HTTP de Apache.

> ## Amazon Bucket

    Para la gestión de imagenes, se considero ideal el uso de bucket de amazon, dado que esta herramienta es un contenedor para objetos almacenados en Amazon S3. Puede almacenar cualquier cantidad de objetos en un bucket y puede tener hasta 100 buckets en su cuenta. Además que dicho servicio se encuentra entre las opciones gratuitas que ofrece Amazon. 
    Entre otras razones por las que utilizamos este servicio, podemos encontrar las siguientes características. 

  - > ### Características

    - Los nombres de los buckets son globales, lo que significa que el nombre es único en todas las cuentas de AWS.

    - Existe servicios dentro de la capa gratuita.

    - Estos bucket pueden ser publicos o privados.

> ## Docker

    Docker es una plataforma de software que le permite crear, probar e implementar aplicaciones rápidamente. Docker empaqueta software en unidades estandarizadas llamadas contenedores que incluyen todo lo necesario para que el software se ejecute, incluidas bibliotecas, herramientas de sistema, código y tiempo de ejecución. Con Docker, puede implementar y ajustar la escala de aplicaciones rápidamente en cualquier entorno con la certeza de saber que su código se ejecutará. La ejecución de Docker en AWS les ofrece a desarrolladores y administradores una manera muy confiable y económica de crear, enviar y ejecutar aplicaciones distribuidas en cualquier escala.

    Entre otras razones por las que utilizamos esta plataforma, podemos encontrar las siguientes características. 

  - > ### Características

    - Modelo de implementación basado en imágenes.

    - Modularidad, capacidad de separar una parte de la aplicación para actualizarla o repararla, sin necesidad de desabilitarla.

    - Capas y control de versiones de imágenes.

    - Restauración de versiones anteriores.

  > ## DockerHub

    Para la publicación de las imágenes realizadas, se utilizó docker hub como herramienta, dado que este es un repositorio de imágenes de contenedores más grande del mundo con una variedad de fuentes de contenido y proveedores de software independientes.  Con esta herramita se puede acceder a imagenes desde cualquier sistema.   

> ## Git

    Esta herramienta fue utilizada para el control de versiones. Además ser una herramienta conocida por parte de los desarrolladores de este proyecto, esta herramienta cuenta con diversas características útiles par el proyecto como las que se muestran a continuación.  

  - > ### Características

    - Es gratuito y código abierto
    - Diseñado para manejar todo, con rapidez y eficiencia, no importando el tamaño del proyecto.
    - Ocupa poco espacio y tiene un rendimiento ultrarápido. 

  > ## GitLab CI/CD

    Para la integración y entrega continua se consideró utilizar la herramiente Gitlab CI/CD, dado que es una herramienta utilizada para el desarrollo de software mediante el uso de métodos continuos. Entre las ventajas de utilizar esta herramienta podemos encontrar que es veloz y permite llevar funciones de manera muy rápida, se caracteriza por ser open source y permite realizar múltiples funciones en un mismo sitio web. 

  > ## GitKraken

    Esta herramienta se utilizó en el proyecto para el mmanejo de git, de una manera sencilla y ordenada. 


> ## Azure Kubernetes 

    Para la realización de kubernetes, se consideró esta herramienta para llevar la implementación de los servicios de la aplicación de una forma más rápida, dado que esta además permite la administración automatizada y escalabilidad de clústeres de Kubernetes para orquestación de contenedores de nivel empresarial, permite también la productividad del desarrollador de un extremo a otro con depuración, CI/CD, además que también permite la compatibilidad con recursos de linux, windows server entre otras. 

> ## Flask 

    Ser utilizó este framework para implememntar un servidor en python, dad que permite crear aplicaciones sencillas y rápidas, cuenta con un acelerador de tareas, por lo que en pocas líneas de código, se puede lograr la ejecución de aplicaciones de una forma rápida. 

> ## Mysql 
    Para este proyecto se consideró una base de datos transaccional, por lo que la mejor opción considerada fue MySql, esto debido a su facilidad de implementación en la nube y las opciones gratuitas que se pueden encontrar para la implementación en esta base de datos. 

> ## Jest 
    Para las pruebas unitarias se optó por la utilización de jest.


# JWT
JWT es un objeto de tipo JSON, una herramienta cuyo objetivo es establecer una transmisión de información segura de dos o más campos. A partir de estos se puede propagar información segura y efectiva, que incluso es verificada, pues cuenta con una firma de forma virtual.
Este conjunto de información toma la referencia de web token, bajo los estándares de JSON.

JSON Web Token puede funcionar desde cualquier espacio, ya que su alcance es muy extenso. Puede establecerse en sitios como URL, como respuestas de POST dentro de un header HTTP. 

A diferencia de otras librerías de autenticación de informática, JWT están conformados por claims o demandas, con las que realiza la transmisión de información de uno u otro lado. Estos datos pueden ser relevantes a la forma de acceso, al tiempo valido, a los permisos y a cualquier otro concepto que se relacione con la data de dos espacios en la web

## Estructura 

JWT es una cadena compuesta por tres partes separadas por un punto ‘. ’ y se serializa usando la base 64. Las tres partes que componen el token son: 
	
-	Header: En primer lugar, el header es el primer componente del token y consiste en dos partes: el tipo de token que, en este caso, es JWT, y el algoritmo que se está utilizando, que puede ser RSA o SHA256.
-	Payload: En el payload se encuentran los claims de token. Estas son sobre una entidad y otro tipo de información que lo acompaña. Los tipos de demandas son: registrado, privado y público.
-	Signature: En el componente de signature deberás firmar el header codificado, el payload codificado, el secret y el algoritmo que se ha establecido en el header.