import Modal from "@mui/material/Modal";
import * as React from "react";
import Typography from "@mui/material/Typography";
import {
  TextField,
  Box,
  Button,
} from "@mui/material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function ModalEdit({ data = {}, open, setData, setOpen, handleEdit }) {
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={style}
          gap="30px"
          display="grid"
          gridTemplateColumns="repeat(4, minmax(0, 1fr))"
        >
          <Typography
            sx={{ gridColumn: "span 4" }}
            id="modal-modal-title"
            variant="h2"
            component="h2"
          >
            Editar Usuario
          </Typography>

          <TextField
            sx={{ gridColumn: "span 2" }}
            fullWidth
            id="outlined-basic"
            label="nombre"
            variant="outlined"
            onChange={(e) => {
              setData({ ...data, nombre: e.target.value });
            }}
            value={data.nombre}
          />
          <TextField
            sx={{ gridColumn: "span 2" }}
            fullWidth
            id="outlined-basic"
            label="apellido"
            variant="outlined"
            onChange={(e) => {
              setData({ ...data, apellido: e.target.value });
            }}
            value={data.apellido}
          />
          <TextField
            sx={{ gridColumn: "span 4" }}
            fullWidth
            id="outlined-basic"
            label="email"
            variant="outlined"
            disabled
            onChange={(e) => {
              setData({ ...data, email: e.target.value });
            }}
            value={data.correo}
          />
          <TextField
            sx={{ gridColumn: "span 2" }}
            fullWidth
            id="outlined-basic"
            label="Pais"
            variant="outlined"
            disabled
            value={data.pais}
            onChange={(e) => {
              setData({ ...data, edad: e.target.value });
            }}
          />
          <TextField
            sx={{ gridColumn: "span 2" }}
            fullWidth
            id="outlined-basic"
            label="telefono"
            variant="outlined"
            value={data.telefono}
            onChange={(e) => {
              setData({ ...data, telefono: e.target.value });
            }}
          />
          <TextField
            sx={{ gridColumn: "span 2" }}
            fullWidth
            id="outlined-basic"
            label="genero"
            disabled
            variant="outlined"
            value={data.genero}
            onChange={(e) => {
              setData({ ...data, genero: e.target.value });
            }}
          />
          <TextField
            fullWidth
            id="outlined-basic"
            type="text"
            disabled
            label="fecha"
            variant="outlined"
            value={data.fecha_nac}
            sx={{ gridColumn: "span 2" }}
            onChange={(e) => {
              setData({ ...data, fecha_nac: e.target.value });
            }}
          />
          <Box
            sx={{ gridColumn: "span 4" }}
            display="flex"
            justifyContent="flex-end"
          >
            <Button
              variant="contained"
              color="primary"
              sx={{ marginRight: "15px" }}
              onClick={() => handleEdit(data)}
            >
              Actualizar
            </Button>
            <Button variant="contained" color="secondary" onClick={handleClose}>
              Cancelar
            </Button>
          </Box>
        </Box>
      </Modal>
    </div>
  );
}
