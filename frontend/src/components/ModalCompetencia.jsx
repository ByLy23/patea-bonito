import Modal from '@mui/material/Modal'
import * as React from 'react'
import Typography from '@mui/material/Typography'
import { TextField, Box, Button, FormControl, InputLabel, Select, MenuItem } from '@mui/material'

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
}

export default function ModalCompetencia({ data = {}, open, setData, setOpen, handleEdit }) {
  const handleClose = () => setOpen(false)

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style} gap="30px" display="grid" gridTemplateColumns="repeat(4, minmax(0, 1fr))">
          <Typography
            sx={{ gridColumn: 'span 4' }}
            id="modal-modal-title"
            variant="h2"
            component="h2"
          >
            Editar Estado Competencia
          </Typography>
          <TextField
            sx={{ gridColumn: 'span 4' }}
            fullWidth
            id="outlined-basic"
            label="nombre"
            variant="outlined"
            value={data.nombre}
            disabled
          />
          <FormControl fullWidth sx={{ gridColumn: 'span 4' }}>
            <InputLabel id="demo-simple-select-label546" variant="filled">
              Estado
            </InputLabel>
            <Select
              labelId="demo-simple-select-label546"
              label="Estado"
              variant="outlined"
              id="demo-simple-select"
              onChange={(e) => {
                setData({ ...data, estado: e.target.value })
              }}
              name="estado"
            >
              <MenuItem value="1">En progreso</MenuItem>
              <MenuItem value="2">Finalziada</MenuItem>
              <MenuItem value="3">Programada</MenuItem>
            </Select>
          </FormControl>
          <Box sx={{ gridColumn: 'span 4' }} display="flex" justifyContent="flex-end">
            <Button
              variant="contained"
              color="primary"
              sx={{ marginRight: '15px' }}
              onClick={() => handleEdit(data)}
            >
              Actualizar Estado
            </Button>
            <Button variant="contained" color="secondary" onClick={handleClose}>
              Cancelar
            </Button>
          </Box>
        </Box>
      </Modal>
    </div>
  )
}
