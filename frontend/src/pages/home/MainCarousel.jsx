import { Box, Typography, IconButton, useMediaQuery } from "@mui/material";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import NavigateBeforeIcon from "@mui/icons-material/NavigateBefore";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import CardMedia from "@mui/material/CardMedia";
import logo from "../../assets/Logo_con_slogan.png";

// imports all images from assets folder

const MainCarousel = () => {
  const isNonMobile = useMediaQuery("(min-width:600px)");
  return (
    <Carousel
      infiniteLoop={true}
      showThumbs={false}
      showIndicators={false}
      showStatus={false}
      renderArrowPrev={(onClickHandler, hasPrev, label) => (
        <IconButton
          onClick={onClickHandler}
          sx={{
            position: "absolute",
            top: "50%",
            left: "0",
            color: "white",
            padding: "5px",
            zIndex: "10",
          }}
        >
          <NavigateBeforeIcon sx={{ fontSize: 40 }} />
        </IconButton>
      )}
      renderArrowNext={(onClickHandler, hasNext, label) => (
        <IconButton
          onClick={onClickHandler}
          sx={{
            position: "absolute",
            top: "50%",
            right: "0",
            color: "white",
            padding: "5px",
            zIndex: "10",
          }}
        >
          <NavigateNextIcon sx={{ fontSize: 40 }} />
        </IconButton>
      )}
    >
      <Box key={`carousel-image-${1}`}>
        <CardMedia
          image="https://tallerpi.s3.amazonaws.com/Astro%2C+Una+Forma+moderna+de+crear+sitios+Web+-+%C2%BFQu%C3%A9+es+Astro.mkv"
          muted
          component="video"
          autoPlay
          controls
          alt={`carousel-${1}`}
          style={{
            width: "100%",
            height: "550px",
            objectFit: "cover",
            backgroundAttachment: "fixed",
          }}
        />
        <Box
          color="white"
          padding="20px"
          borderRadius="1px"
          textAlign="left"
          backgroundColor="rgb(0, 0, 0, 0.4)"
          position="absolute"
          top="46%"
          left={isNonMobile ? "10%" : "0"}
          right={isNonMobile ? undefined : "0"}
          margin={isNonMobile ? undefined : "0 auto"}
          maxWidth={isNonMobile ? undefined : "240px"}
        >
          <Typography variant="h3">Video Presentación</Typography>
        </Box>
      </Box>
      <Box key={`carousel-image-${2}`}>
        <img
          src={logo}
          alt={`carousel-${2}`}
          style={{
            width: "100%",
            height: "550px",
            objectFit: "cover",
            backgroundAttachment: "fixed",
          }}
        />

      </Box>
    </Carousel>
  );
};

export default MainCarousel;
