import AssessmentIcon from "@mui/icons-material/Assessment";
import AssessmentTwoToneIcon from "@mui/icons-material/AssessmentTwoTone";
import AssessmentOutlinedIcon from "@mui/icons-material/AssessmentOutlined";
import { useState } from "react";
import { ProSidebar, Menu, MenuItem } from "react-pro-sidebar";
import { Box, IconButton, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import "react-pro-sidebar/dist/css/styles.css";
import { shades } from "../../style/theme";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import PeopleIcon from "@mui/icons-material/People";
import MenuOutlinedIcon from "@mui/icons-material/MenuOutlined";
import PersonAddAlt1Icon from "@mui/icons-material/PersonAddAlt1";
import GroupsIcon from "@mui/icons-material/Groups";
import SupervisorAccountIcon from "@mui/icons-material/SupervisorAccount";
import StadiumIcon from "@mui/icons-material/Stadium";
import AddIcon from "@mui/icons-material/Add";
import SportsSoccerIcon from "@mui/icons-material/SportsSoccer";
import EmojiEventsIcon from "@mui/icons-material/EmojiEvents";
import EmojiPeopleRoundedIcon from "@mui/icons-material/EmojiPeopleRounded";
import SportsIcon from "@mui/icons-material/Sports";
import { useSelector } from "react-redux";
import FlagRoundedIcon from "@mui/icons-material/FlagRounded";
import { Man3 } from "@mui/icons-material";
import useMediaQuery from "@mui/material/useMediaQuery";

const Item = ({ title, to, icon, selected, setSelected }) => {
  return (
    <MenuItem
      active={selected === title}
      style={{
        color: shades.grey[100],
      }}
      onClick={() => setSelected(title)}
      icon={icon}
    >
      <Typography>{title}</Typography>
      <Link to={"/admin" + to} />
    </MenuItem>
  );
};

const Sidebar = () => {
  const usuario = useSelector((state) => state.sesion.usuario);
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const [isCollapsed, setIsCollapsed] = useState(!isNonMobile);
  const [selected, setSelected] = useState("Dashboard");
  return (
    <Box
      sx={{
        "& .pro-sidebar-inner": {
          background: `${shades.primary[400]} !important`,
        },
        "& .pro-icon-wrapper": {
          backgroundColor: "transparent !important",
        },
        "& .pro-inner-item": {
          padding: "5px 35px 5px 20px !important",
        },
        "& .pro-inner-item:hover": {
          color: "#DCBBA1 !important",
        },
        "& .pro-menu-item.active": {
          color: "#B78454 !important",
        },
      }}
    >
      <ProSidebar collapsed={isCollapsed}>
        <Menu iconShape="square">
          {/* LOGO AND MENU ICON */}
          <MenuItem
            onClick={() => setIsCollapsed(!isCollapsed)}
            icon={isCollapsed ? <MenuOutlinedIcon /> : undefined}
            style={{
              margin: "10px 0 20px 0",
              color: shades.grey[100],
            }}
          >
            {!isCollapsed && (
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                ml="15px"
              >
                <Typography variant="h5" color={shades.grey[100]}>
                  ADMINISTRADOR
                </Typography>
                <IconButton onClick={() => setIsCollapsed(!isCollapsed)}>
                  <MenuOutlinedIcon />
                </IconButton>
              </Box>
            )}
          </MenuItem>

          {!isCollapsed && (
            <Box mb="25px">
              <Box display="flex" justifyContent="center" alignItems="center">
                <img
                  alt="profile-user"
                  width="120px"
                  height="120px"
                  src={usuario.fotografia}
                  style={{ cursor: "pointer", borderRadius: "50%" }}
                />
              </Box>
              <Box textAlign="center">
                <Typography
                  variant="h3"
                  color={shades.grey[100]}
                  fontWeight="bold"
                  sx={{ m: "10px 0 0 0" }}
                >
                  {usuario.nombre} {usuario.apellido}
                </Typography>
                <Typography variant="h5" color={shades.primary[700]}>
                  {usuario.correo}
                </Typography>
              </Box>
            </Box>
          )}

          <Box paddingLeft={isCollapsed ? undefined : "10%"}>
            <Item
              title="Home"
              to="/"
              icon={<HomeOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
            />

            <Typography
              variant="h6"
              color={shades.grey[300]}
              sx={{ m: "15px 0 5px 20px" }}
            >
              Usuarios
            </Typography>
            <Item
              title="Crear Usuario"
              to="/addUsuario"
              icon={<PersonAddAlt1Icon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Clientes"
              to="/showClientes"
              icon={<PeopleIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Empleados"
              to="/showEmpleados"
              icon={<GroupsIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Administradores"
              to="/showAdmins"
              icon={<SupervisorAccountIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Typography
              variant="h6"
              color={shades.grey[300]}
              sx={{ m: "15px 0 5px 20px" }}
            >
              Reportes
            </Typography>
            <Item
              title="Empleados"
              to="/reporteEmpleados"
              icon={<AssessmentIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Empleados equipo"
              to="/reporteEmpleados2"
              icon={<AssessmentIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Clientes"
              to="/reporteClientes"
              icon={<AssessmentOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Clientes equipo"
              to="/reporteClientes2"
              icon={<AssessmentOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Bitacora Admin"
              to="/reporteAdmin"
              icon={<AssessmentTwoToneIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Typography
              variant="h6"
              color={shades.grey[300]}
              sx={{ m: "15px 0 5px 20px" }}
            >
              Datos Deportivos
            </Typography>

            <Item
              title="Registrar Jugador"
              to="/addJugador"
              icon={<AddIcon />}
              selected={selected}
              setSelected={setSelected}
            />

            <Item
              title="Crear Estadio"
              to="/addEstadio"
              icon={<AddIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Crear Equipo"
              to="/addEquipo"
              icon={<AddIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Crear Arbitro"
              to="/addArbitro"
              icon={<AddIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Crear Competencia"
              to="/addCompetencia"
              icon={<AddIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Crear Tecnico"
              to="/addTecnico"
              icon={<AddIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Crear Partido"
              to="/addPartido"
              icon={<AddIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Jugadores"
              to="/showJugadores"
              icon={<EmojiPeopleRoundedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Estadios"
              to="/showEstadios"
              icon={<StadiumIcon />}
              selected={selected}
              setSelected={setSelected}
            />

            <Item
              title="Equipos"
              to="/showEquipos"
              icon={<SportsSoccerIcon />}
              selected={selected}
              setSelected={setSelected}
            />

            <Item
              title="Arbitros"
              to="/showArbitros"
              icon={<SportsIcon />}
              selected={selected}
              setSelected={setSelected}
            />

            <Item
              title="Competencias"
              to="/showCompetencias"
              icon={<EmojiEventsIcon />}
              selected={selected}
              setSelected={setSelected}
            />

            <Item
              title="Tecnicos"
              to="/showTecnicos"
              icon={<Man3 />}
              selected={selected}
              setSelected={setSelected}
            />

            <Item
              title="Crear Partido"
              to="/addPartido"
              icon={<AddIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <Item
              title="Partidos"
              to="/showPartidos"
              icon={<FlagRoundedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
          </Box>
        </Menu>
      </ProSidebar>
    </Box>
  );
};

export default Sidebar;
