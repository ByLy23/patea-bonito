import React from "react";
import { Box, Typography, Button, Select, MenuItem } from "@mui/material";
import Header from "../../../components/Header";
import StatBox from "../../../components/StatBox";
import WorkspacePremiumRoundedIcon from "@mui/icons-material/WorkspacePremiumRounded";
import Swal from "sweetalert2";
import CreditCardOffIcon from "@mui/icons-material/CreditCardOff";
import { shades } from "../../../style/theme";
import PaymentsOutlinedIcon from "@mui/icons-material/PaymentsOutlined";
import CancelPresentationRoundedIcon from "@mui/icons-material/CancelPresentationRounded";
import {
  //useGetMembresiaQuery,
  useBajaClienteMembresiaMutation,
  useActivarClienteMembresiaMutation,
} from "../../../app/api/users/apiCliente";
import { useDispatch } from "react-redux";
import { editMembresia } from "../../../app/slices/sesion/sesionSlice";

const Membresia = ({ usuario = {} }) => {
  //const { data: membresia } = useGetMembresiaQuery(usuario.id_usuario);
  const dispatch = useDispatch();
  const [meses, setMeses] = React.useState(1);
  const [bajaClienteMembresia] = useBajaClienteMembresiaMutation();
  const [activarClienteMembresia] = useActivarClienteMembresiaMutation();

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header
        title="Membresía"
        subtitle="Aquí puedes ver tu membresía y sus beneficios"
      />
      <Box
        display="grid"
        gridTemplateColumns="repeat(12, 1fr)"
        gridAutoRows="140px"
        gap="20px"
      >
        {usuario?.membresia === 2 ? (
          <>
            <Box
              gridColumn="span 6"
              backgroundColor={shades.primary[400]}
              display="flex"
              alignItems="center"
              justifyContent="center"
              height={"12rem"}
            >
              <StatBox
                title="Membresia Activada"
                subtitle="Estas usando la membresia premium"
                progress="0.75"
                icon={
                  <WorkspacePremiumRoundedIcon
                    sx={{ color: shades.greenAccent[600], fontSize: "100px" }}
                  />
                }
              />
            </Box>
            <Box
              gridColumn="span 6"
              backgroundColor={shades.primary[400]}
              display="flex"
              alignItems="center"
              justifyContent="center"
              height={"12rem"}
            >
              <Box width="100%" m="0 30px">
                <Box display="flex" justifyContent="space-between">
                  <Box>
                    <CancelPresentationRoundedIcon
                      sx={{ color: shades.greenAccent[600], fontSize: "80px" }}
                    />
                    <Typography
                      variant="h4"
                      fontWeight="bold"
                      sx={{ color: shades.grey[100] }}
                    >
                      Cancelar Membresia
                    </Typography>
                  </Box>
                  <Box></Box>
                </Box>
                <Box display="flex" justifyContent="space-between" mt="2px">
                  <Typography
                    variant="h6"
                    sx={{ color: shades.greenAccent[500] }}
                  >
                    Puedes cancelar tu membresia premium
                  </Typography>
                  <Button
                    variant="contained"
                    sx={{
                      backgroundColor: shades.greenAccent[600],
                      marginLeft: "1rem",
                    }}
                    onClick={() => {
                      Swal.fire({
                        title: "Estas seguro?",
                        text: "Tu membresia premium sera cancelada",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Si, cancelar",
                        cancelButtonText: "No, cancelar",
                      }).then(async (result) => {
                        if (result.isConfirmed) {
                          await bajaClienteMembresia({
                            id_usuario: usuario.id_usuario,
                          })
                            .unwrap()
                            .then((res) => {
                              Swal.fire(
                                "Membresia cancelada",
                                "Tu membresia premium ha sido cancelada",
                                "success"
                              );
                              dispatch(editMembresia(1));
                              window.location.reload();
                            })
                            .catch((err) => {
                              Swal.fire(
                                "Error",
                                "No se pudo cancelar tu membresia",
                                "error"
                              );
                            });
                        } else if (
                          result.dismiss === Swal.DismissReason.cancel
                        ) {
                          Swal.fire(
                            "Cancelado",
                            "Tu membresia premium sigue activa",
                            "error"
                          );
                        }
                      });
                    }}
                  >
                    Cancelar
                  </Button>
                </Box>
              </Box>
            </Box>
          </>
        ) : (
          <>
            <Box
              gridColumn="span 6"
              backgroundColor={shades.primary[400]}
              display="flex"
              alignItems="center"
              justifyContent="center"
              height={"12rem"}
            >
              <StatBox
                title="Membresia Gratis"
                subtitle="Estas usando la membresia gratis"
                progress="0.75"
                increase=""
                icon={
                  <CreditCardOffIcon
                    sx={{ color: shades.greenAccent[600], fontSize: "100px" }}
                  />
                }
              />
            </Box>
            <Box
              gridColumn="span 6"
              backgroundColor={shades.primary[400]}
              display="flex"
              alignItems="center"
              justifyContent="center"
              height={"12rem"}
            >
              <Box width="100%" m="0 30px">
                <Box display="flex" justifyContent="space-between">
                  <Box>
                    <PaymentsOutlinedIcon
                      sx={{ color: shades.greenAccent[600], fontSize: "80px" }}
                    />
                    <Typography
                      variant="h4"
                      fontWeight="bold"
                      sx={{ color: shades.grey[100] }}
                    >
                      Adquirir Membresia
                    </Typography>
                  </Box>
                  <Box></Box>
                </Box>
                <Box display="flex" justifyContent="space-between" mt="2px">
                  <Typography
                    variant="h6"
                    sx={{ color: shades.greenAccent[500] }}
                  >
                    Puedes adquirir una membresia premium por solo Q15
                  </Typography>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={meses}
                    onChange={(e) => setMeses(e.target.value)}
                    label="Cantidad de meses"
                  >
                    <MenuItem value={1}>1 Mes</MenuItem>
                    <MenuItem value={2}>2 Meses</MenuItem>
                    <MenuItem value={3}>3 Meses</MenuItem>
                  </Select>
                  <Button
                    variant="contained"
                    sx={{
                      backgroundColor: shades.greenAccent[600],
                      marginLeft: "1rem",
                    }}
                    onClick={() => {
                      Swal.fire({
                        title: "Estas seguro?",
                        text: "Tu membresia premium sera activada",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Si, adquirir",
                        cancelButtonText: "No, adquirir",
                      }).then(async (result) => {
                        if (result.isConfirmed) {
                          await activarClienteMembresia({
                            id_usuario: usuario.id_usuario,
                            meses: meses,
                          })
                            .unwrap()
                            .then((res) => {
                              Swal.fire(
                                "Membresia adquirida",
                                "Tu membresia premium ha sido activada",
                                "success"
                              );
                              dispatch(editMembresia(2));
                              window.location.reload();
                            })
                            .catch((err) => {
                              Swal.fire(
                                "Error",
                                "No tienes suficiente saldo",
                                "error"
                              );
                            });
                        } else if (
                          result.dismiss === Swal.DismissReason.cancel
                        ) {
                          Swal.fire(
                            "Cancelado",
                            "Tu membresia gratis sigue activa",
                            "error"
                          );
                        }
                      });
                    }}
                  >
                    Adquirir
                  </Button>
                </Box>
              </Box>
            </Box>
          </>
        )}
      </Box>
      <div style={{ height: "2rem" }}></div>
    </Box>
  );
};

export default Membresia;
