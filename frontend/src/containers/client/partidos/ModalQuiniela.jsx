import { useState } from "react";
import { Box, TextField, Button, Modal, Typography } from "@mui/material";
import Swal from "sweetalert2";
import { usePostQuinielaMutation } from "../../../app/api/stats/apiQuiniela";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "50%",
  height: "60%",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function ModalQuiniela({ data = {}, open, setOpen, id_usuario }) {
  const handleClose = () => setOpen(false);
  const [datos, setDatos] = useState();
  const [postQuiniela] = usePostQuinielaMutation();

  const handle = async () => {
    const datar = {
      id_partido: data.id_partido,
      id_usuario: id_usuario,
      gol_loc: datos.gol_loc,
      gol_vis: datos.gol_vis,
    };
    await postQuiniela(datar)
      .unwrap()
      .then((res) => {
        Swal.fire({
          icon: "success",
          title: "Quiniela Guardada",
          showConfirmButton: false,
          timer: 1500,
        });
      })
      .catch((err) => {
        Swal.fire({
          icon: "error",
          title: "Error al guardar la quiniela",
          showConfirmButton: false,
          timer: 1500,
        });
      });

    handleClose();
  };

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={style}
          gap="30px"
          display="grid"
          gridTemplateColumns="repeat(4, minmax(0, 1fr))"
        >
          <Typography
            sx={{ gridColumn: "span 4" }}
            id="modal-modal-title"
            variant="h2"
            component="h2"
          >
            Quiniela de Partido
          </Typography>

          <TextField
            sx={{ gridColumn: "span 2" }}
            id="outlined-select-currency-native"
            label="Equipo Local"
            variant="outlined"
            value={data.equipo_loc}
            disabled
          />
          <TextField
            sx={{ gridColumn: "span 2" }}
            id="outlined-select-currency-native"
            label="Equipo Visitante"
            variant="outlined"
            value={data.equipo_vis}
            disabled
          />
          <TextField
            sx={{ gridColumn: "span 2" }}
            id="outlined-select-currency-native"
            label="Goles Local"
            variant="outlined"
            value={datos?.gol_loc}
            onChange={(e) => setDatos({ ...datos, gol_loc: e.target.value })}
          />
          <TextField
            sx={{ gridColumn: "span 2" }}
            id="outlined-select-currency-native"
            label="Goles Visitante"
            variant="outlined"
            value={datos?.gol_vis}
            onChange={(e) => setDatos({ ...datos, gol_vis: e.target.value })}
          />

          <Box
            sx={{ gridColumn: "span 4" }}
            display="flex"
            justifyContent="flex-end"
          >
            <Button
              variant="contained"
              color="primary"
              sx={{ marginRight: "15px" }}
              onClick={handle}
            >
              Guardar Quiniela
            </Button>
            <Button variant="contained" color="secondary" onClick={handleClose}>
              Cancelar
            </Button>
          </Box>
        </Box>
      </Modal>
    </div>
  );
}

export default ModalQuiniela;
