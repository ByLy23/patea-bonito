import React from "react";
import { Box, IconButton } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { shades } from "../../../style/theme";
import Header from "../../../components/Header";
import CircularProgress from "@mui/material/CircularProgress";
import AutoGraphIcon from "@mui/icons-material/AutoGraph";
import ModalQuiniela from "./ModalQuiniela";
import { useDispatch } from "react-redux";
import { fetchPredictSuccess } from "../../../app/slices/sesion/predictSlice";
import { useNavigate } from "react-router-dom";
import RemoveRedEyeRoundedIcon from '@mui/icons-material/RemoveRedEyeRounded';

const Visualizar = ({
  data = [],
  isFetching = false,
  membresia = false,
  id_usuario,
}) => {
  const [open, setOpen] = React.useState(false);
  const [dataModal, setDataModal] = React.useState({});
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const rows = data?.map((item, i) => ({
    ...item,
    id: i + 1,
  }));

  //const fild = (data[0] && Object.keys(data[0])) || [];

  const columns = [
    { field: "id", headerName: "ID", flex: 0.5 },
    { field: "hora", headerName: "Hora", flex: 0.8 },
    { field: "fecha", headerName: "Fecha", flex: 1 },
    { field: "gol_loc", headerName: "Goles Local", flex: 0.5 },
    { field: "gol_vis", headerName: "Goles Visitante", flex: 0.5 },
    { field: "equipo_loc", headerName: "Local", flex: 1 },
    { field: "equipo_vis", headerName: "Visitante", flex: 1 },
    { field: "estadio", headerName: "Estadio", flex: 1 },
    { field: "competencia", headerName: "Competencia", flex: 1 },
    { field: "estado", headerName: "Estado", flex: 0.8 },
    {
      field: "Quiniela",
      headerName: "Quiniela",
      flex: 0.5,
      renderCell: (params) => (
        <>
          {params.row?.quiniela === 1 ? (
            <IconButton
              variant="contained"
              color="primary"
              size="small"
              onClick={() => {
                setOpen(true);
                setDataModal(params.row);
              }}
            >
              <AutoGraphIcon />
            </IconButton>
          ) : (
            <></>
          )}
        </>
      ),
    },
    {
      field: "predi",
      headerName: "Predicción",
      flex: 0.6,
      renderCell: (params) => (
        <>
          <IconButton
            variant="contained"
            color="primary"
            size="small"
            onClick={() => {
              dispatch(fetchPredictSuccess(params.row?.id_partido));
              navigate("/cliente/predicciones");
            }}
          >
            <RemoveRedEyeRoundedIcon />
          </IconButton>
        </>
      ),
    },
  ];

  const columns2 = [
    { field: "id", headerName: "ID", flex: 0.5 },
    { field: "hora", headerName: "Hora", flex: 1 },
    { field: "fecha", headerName: "Fecha", flex: 1 },
    { field: "gol_loc", headerName: "Goles Local", flex: 0.5 },
    { field: "gol_vis", headerName: "Goles Visitante", flex: 0.5 },
    { field: "equipo_loc", headerName: "Local", flex: 1 },
    { field: "equipo_vis", headerName: "Visitante", flex: 1 },
    { field: "estadio", headerName: "Estadio", flex: 1 },
    { field: "competencia", headerName: "Competencia", flex: 1 },
    { field: "estado", headerName: "Estado", flex: 1 },
  ];

  return (
    <>
      <Box m="20px" sx={{ marginTop: "-25px" }}>
        <Header title="Partidos" subtitle="Lista de partidos" />
        {isFetching && (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "60vh",
            }}
          >
            <CircularProgress />
          </Box>
        )}

        {!isFetching && (
          <Box
            m="40px 0 -10 0"
            height="75vh"
            sx={{
              "& .MuiDataGrid-root": {
                border: "none",
              },
              "& .MuiDataGrid-cell": {
                borderBottom: "none",
              },
              "& .nombre-column--cell": {
                color: shades.primary[100],
              },
              "& .MuiDataGrid-columnHeaders": {
                backgroundColor: shades.primary[600],
                borderBottom: "none",
                color: shades.grey[900],
              },
              "& .MuiDataGrid-virtualScroller": {
                backgroundColor: shades.primary[400],
              },
              "& .MuiDataGrid-footerContainer": {
                borderTop: "none",
                backgroundColor: shades.primary[600],
                color: shades.grey[900],
              },
              "& .MuiCheckbox-root": {
                color: `${shades.grey[100]} !important`,
              },
              "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                color: `${shades.grey[100]} !important`,
              },
            }}
          >
            <DataGrid
              rows={rows}
              isRowSelectable={(params) => false}
              columns={membresia ? columns : columns2}
              components={{ Toolbar: GridToolbar }}
            />
          </Box>
        )}
      </Box>
      <ModalQuiniela
        open={open}
        setOpen={setOpen}
        data={dataModal}
        id_usuario={id_usuario}
      />
    </>
  );
};

export default Visualizar;
