import React from "react";
import {
  Box,
  Button,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
} from "@mui/material";
import { Formik } from "formik";
import * as yup from "yup";
import useMediaQuery from "@mui/material/useMediaQuery";
import Header from "../../../components/Header";
import Swal from "sweetalert2";
import {
  useAddSubscripcionMutation,
  useGetEquipoQuery,
} from "../../../app/api/stats/apiSubs";

const initialValues = {
  id_equipo: "",
};

const AddSubs = ({ id_usuario }) => {
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const [isSubmitting, setIsSubmitting] = React.useState(false);
  const [addSubscripcion] = useAddSubscripcionMutation();
  const { data: equipos = [] } = useGetEquipoQuery(id_usuario);

  const handleFormSubmit = async (values, onSubmitProps) => {
    setIsSubmitting(true);
    try {
      const response = await addSubscripcion({
        id_usuario,
        id_equipo: values.id_equipo,
      });
      if (response.data) {
        Swal.fire({
          title: "Subscripcion agregada",
          icon: "success",
          confirmButtonText: "Aceptar",
        });
        onSubmitProps.resetForm();
      }
    } catch (error) {
      Swal.fire({
        title: "Error al subscribirse al equipo",
        icon: "error",
        confirmButtonText: "Aceptar",
      });
    }
    setIsSubmitting(false);
  };

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header
        title="Suscribirse a un equipo"
        subtitle="Se muestran todos los equipos disponibles para suscribirse"
      />
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormSubmit}
        validationSchema={checkoutSchema}
      >
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          setFieldValue,
        }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
              }}
            >
              <FormControl fullWidth sx={{ gridColumn: "span 4" }}>
                <InputLabel id="demo-simple-select-label-19" variant="filled">
                  Equipo
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label-19"
                  label="Equipo"
                  variant="filled"
                  id="demo-simple-select-1"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.id_equipo}
                  name="id_equipo"
                  error={
                    Boolean(touched.id_equipo) && Boolean(errors.id_equipo)
                  }
                  helperText={touched.id_equipo && errors.id_equipo}
                >
                  {equipos.map((equipo) => (
                    <MenuItem value={equipo.id_equipo}>
                      {equipo.nombre}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>

            <Box display="flex" justifyContent="end" mt="20px">
              <Button
                type="submit"
                color="primary"
                variant="contained"
                disabled={isSubmitting}
              >
                Suscribirse
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <div style={{ height: "2rem" }}></div>
    </Box>
  );
};

const checkoutSchema = yup.object().shape({
  id_equipo: yup.string().required("Campo requerido"),
});

export default AddSubs;
