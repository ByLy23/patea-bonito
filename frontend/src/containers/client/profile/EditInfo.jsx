import React from "react";
import {
  Box,
  Button,
  TextField,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
} from "@mui/material";
import { Formik } from "formik";
import * as yup from "yup";
import useMediaQuery from "@mui/material/useMediaQuery";
import Header from "../../../components/Header";
import InputAdornment from "@mui/material/InputAdornment";
import TodayIcon from "@mui/icons-material/Today";
import { useGetPaisesQuery } from "../../../app/api/func/Pais";
import { useUpdateClienteMutation } from "../../../app/api/users/apiCliente";
import Swal from "sweetalert2";
import { editUsuario } from "../../../app/slices/sesion/sesionSlice";
import { useDispatch, useSelector } from "react-redux";

const EditInfo = ({ initialValues = {} }) => {
  const url = useSelector((state) => state.sesion.url);

  const isNonMobile = useMediaQuery("(min-width:600px)");
  const [isSubmitting, setIsSubmitting] = React.useState(false);
  const [updateCliente] = useUpdateClienteMutation();
  const dispatch = useDispatch();
  const { data: paises = [] } = useGetPaisesQuery();

  const handleFormSubmit = async (values) => {
    setIsSubmitting(true);
    console.log(values);
    await updateCliente({
      updatedCliente: values,
      url: url,
    })
      .unwrap()
      .then((data) => {
        Swal.fire({
          icon: "success",
          title: "Actualizado",
          text: "Datos actualizados correctamente",
        });
        dispatch(editUsuario(values));
      })
      .catch((error) => {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "No se pudo actualizar los datos",
        });
      });
    setIsSubmitting(false);
  };

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header title="Editar Perfil" subtitle="Actualiza tus datos personales" />
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormSubmit}
        validationSchema={checkoutSchema}
      >
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
        }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
              }}
            >
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Nombres"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.nombre}
                name="nombre"
                error={!!touched.nombre && !!errors.nombre}
                helperText={touched.nombre && errors.nombre}
                sx={{ gridColumn: "span 2" }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Apellidos"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.apellido}
                name="apellido"
                error={!!touched.apellido && !!errors.apellido}
                helperText={touched.apellido && errors.apellido}
                sx={{ gridColumn: "span 2" }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Correo Electrónico"
                disabled
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.correo}
                name="correo"
                error={!!touched.correo && !!errors.correo}
                helperText={touched.correo && errors.correo}
                sx={{ gridColumn: "span 2" }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Telefono"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.telefono}
                name="telefono"
                error={!!touched.telefono && !!errors.telefono}
                helperText={touched.telefono && errors.telefono}
                sx={{ gridColumn: "span 2" }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Direccion"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.direccion}
                name="direccion"
                error={!!touched.direccion && !!errors.direccion}
                helperText={touched.direccion && errors.direccion}
                sx={{ gridColumn: "span 2" }}
              />
              <FormControl fullWidth sx={{ gridColumn: "span 2" }}>
                <InputLabel id="demo-simple-select-label" variant="filled">
                  Genero
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  label="Genero"
                  variant="filled"
                  id="demo-simple-select"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.id_genero}
                  name="id_genero"
                  error={
                    Boolean(touched.id_genero) && Boolean(errors.id_genero)
                  }
                  helperText={touched.id_genero && errors.id_genero}
                >
                  <MenuItem value="2">Masculino</MenuItem>
                  <MenuItem value="1">Femenino</MenuItem>
                </Select>
              </FormControl>
              <TextField
                label="Fecha de nacimiento"
                onBlur={handleBlur}
                onChange={handleChange}
                disabled
                value={values.fecha_nac}
                name="fecha_nac"
                error={Boolean(touched.fecha_nac) && Boolean(errors.fecha_nac)}
                helperText={touched.fecha_nac && errors.fecha_nac}
                sx={{ gridColumn: "span 2" }}
                fullWidth
                format="dd-mm-aaaa"
                type="text"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <TodayIcon />
                    </InputAdornment>
                  ),
                }}
                variant="filled"
              />
              <FormControl fullWidth sx={{ gridColumn: "span 2" }}>
                <InputLabel id="demo-simple-select-label-1" variant="filled">
                  Pais
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label-1"
                  label="Pais"
                  variant="filled"
                  id="demo-simple-select-1"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  //disabled
                  value={values.id_pais}
                  name="id_pais"
                  error={Boolean(touched.id_pais) && Boolean(errors.id_pais)}
                  helperText={touched.id_pais && errors.id_pais}
                >
                  {paises.map((pais) => (
                    <MenuItem value={pais.id_pais}>{pais.nombre}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>

            <Box display="flex" justifyContent="end" mt="20px">
              <Button
                type="submit"
                color="primary"
                variant="contained"
                disabled={isSubmitting}
              >
                Actualizar datos
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <div style={{ height: "2rem" }}></div>
    </Box>
  );
};

const phoneRegExp =
  /^((\+[1-9]{1,4}[ -]?)|(\([0-9]{2,3}\)[ -]?)|([0-9]{2,4})[ -]?)*?[0-9]{3,4}[ -]?[0-9]{3,4}$/;

const checkoutSchema = yup.object().shape({
  nombre: yup.string().required("Campo requerido"),
  apellido: yup.string().required("Campo requerido"),
  correo: yup.string().email("correo invalido").required("Campo requerido"),
  direccion: yup.string().required("Campo requerido"),
  telefono: yup
    .string()
    .matches(phoneRegExp, "Numero invalido")
    .required("Campo requerido"),
  id_genero: yup.string().required("Campo requerido"),
  fecha_nac: yup.string().required("Campo requerido"),
  id_pais: yup.string().required("Campo requerido"),
  id_rol: yup.string().required("Campo requerido"),
  fotografia: yup.mixed().required("Campo requerido"),
});

export default EditInfo;
