import React from "react";
import {
  Box,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
  Button,
} from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { shades } from "../../../style/theme";
import Header from "../../../components/Header";
import CircularProgress from "@mui/material/CircularProgress";
import { useGetTransferenciasQuery } from "../../../app/api/AccionesEmp/apiTransferencias";

const BitacoraTrans = ({ rol = "", deportistas = [] }) => {
  const [id_deportista, setId_deportista] = React.useState(0);

  const { data = [], isFetching } = useGetTransferenciasQuery({
    id: id_deportista,
    rol: rol === "jugador" ? 2 : 1,
  });

  const fild = (data[0] && Object.keys(data[0])) || [];

  const columns = fild.map((item) => {
    if (item === "id_trans") {
      return {
        field: item,
        headerName: "ID",
        flex: 0.5,
      };
    } else {
      return {
        field: item,
        headerName: item,
        flex: 1,
      };
    }
  });

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header
        title={"Bitacora de transferencias de " + rol}
        subtitle="Aqui podras ver las transferencias que se han realizado en la plataforma"
      />
      <FormControl
        fullWidth
        sx={{ gridColumn: "span 4", marginBottom: "15px" }}
      >
        <InputLabel id="demo-simple-select-label-19" variant="filled">
          {rol === "jugador" ? "Jugador" : "Tecnico"}
        </InputLabel>
        <Select
          labelId="demo-simple-select-label-19"
          label="Equipo"
          variant="filled"
          id="demo-simple-select-1"
          onChange={(e) => setId_deportista(e.target.value)}
          value={id_deportista}
          name="id_deportista"
        >
          {deportistas.map((item) => (
            <MenuItem value={item?.id_jugador || item?.id_tecnico}>
              {item.nombre}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      {isFetching && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "60vh",
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .nombre-column--cell": {
              color: shades.primary[100],
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: shades.primary[600],
              borderBottom: "none",
              color: shades.grey[900],
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: shades.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            "& .MuiCheckbox-root": {
              color: `${shades.grey[100]} !important`,
            },
            "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            getRowId={(row) => row.id_bitacora}
            rows={data}
            isRowSelectable={(params) => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
    </Box>
  );
};

export default BitacoraTrans;
