import React, { useState } from 'react'
import {
  Box,
  IconButton,
  TextField,
  Button,
  Modal,
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Switch,
  FormControlLabel,
} from '@mui/material'
import { DataGrid, GridToolbar } from '@mui/x-data-grid'
import { shades } from '../../../style/theme'
import Header from '../../../components/Header'
import EditIcon from '@mui/icons-material/Edit'
import SportsSoccerTwoToneIcon from '@mui/icons-material/SportsSoccerTwoTone'
import CircularProgress from '@mui/material/CircularProgress'
import Swal from 'sweetalert2'
import { useNavigate } from 'react-router-dom'
import SquareTwoToneIcon from '@mui/icons-material/SquareTwoTone'
import { useGetJugadoresByPartidoQuery } from '../../../app/api/datosDeportivos/apiJugadores'
import WorkspacePremiumIcon from '@mui/icons-material/WorkspacePremium'
import {
  useEstadoIncidenciaMutation,
  useGolIncidenciaMutation,
  useTarjetaIncidenciaMutation,
} from '../../../app/api/AccionesEmp/apiIncidencia'
import VisibilityOutlinedIcon from '@mui/icons-material/VisibilityOutlined'
import { useCrearPrediccionMutation } from '../../../app/api/Algoritmo/apiPrediccion'
import { useDispatch } from 'react-redux'
import { fetchPredictSuccess } from '../../../app/slices/sesion/predictSlice'

const ShowPartidos = ({ data = [], isFetching = false }) => {
  const [openGoles, setOpenGoles] = React.useState(false)
  const [openTarjetas, setOpenTarjetas] = React.useState(false)
  const [openEstado, setOpenEstado] = React.useState(false)
  const [dataModal, setDataModal] = React.useState({})

  const dispatch = useDispatch()
  const navigate = useNavigate()

  const [idPartido, setIdPartido] = React.useState(0)
  const { data: dataJugadores, isFetching: isFetchingJugadores } =
    useGetJugadoresByPartidoQuery(idPartido)

  const [updateGol] = useGolIncidenciaMutation()
  const [updateTarjeta] = useTarjetaIncidenciaMutation()
  const [updateEstado] = useEstadoIncidenciaMutation()

  const [createPrediccion] = useCrearPrediccionMutation()

  const columns = [
    { field: 'id_partido', headerName: 'ID', flex: 0.2 },
    {
      field: 'publico',
      headerName: 'Publico',
      flex: 0.6,
      cellClassnombre: 'nombre-column--cell',
    },
    /*{
      field: "fecha",
      headerName: "Fecha",
      flex: 1,
      cellClassnombre: "nombre-column--cell",
    },*/
    {
      field: 'hora',
      headerName: 'Hora',
      flex: 1,
    },
    {
      field: 'gol_loc',
      headerName: 'Local',
      flex: 0.5,
    },
    {
      field: 'gol_vis',
      headerName: 'Visitante',
      flex: 0.5,
    },
    {
      field: 'equipo_loc',
      headerName: 'Equipo Local',
      flex: 1,
    },
    {
      field: 'equipo_vis',
      headerName: 'Equipo Visitante',
      flex: 1,
    },
    /*{
      field: "estadio",
      headerName: "Estadio",
      flex: 1,
    },
    {
      field: "competencia",
      headerName: "Competencia",
      flex: 1,
    },*/
    {
      field: 'estado',
      headerName: 'Estado',
      flex: 0.8,
    },
    {
      field: 'gol',
      headerName: 'Gol',
      flex: 0.2,
      renderCell: (params) => (
        <strong>
          <IconButton
            variant="contained"
            color="primary"
            size="small"
            onClick={() => {
              setIdPartido(params.row.id_partido)
              setOpenGoles(true)
              setDataModal(params.row)
            }}
          >
            <SportsSoccerTwoToneIcon />
          </IconButton>
        </strong>
      ),
    },
    {
      field: 'tarjeta',
      headerName: 'Tarjeta',
      flex: 0.4,
      renderCell: (params) => (
        <strong>
          <IconButton
            variant="contained"
            color="primary"
            size="small"
            onClick={() => {
              setIdPartido(params.row.id_partido)
              setOpenTarjetas(true)
              setDataModal(params.row)
            }}
          >
            <SquareTwoToneIcon />
          </IconButton>
        </strong>
      ),
    },
    {
      field: 'cambios',
      headerName: 'Estado',
      flex: 0.4,
      renderCell: (params) => (
        <strong>
          <IconButton
            variant="contained"
            color="primary"
            size="small"
            onClick={() => {
              setIdPartido(params.row.id_partido)
              setOpenEstado(true)
              setDataModal(params.row)
            }}
          >
            <EditIcon />
          </IconButton>
        </strong>
      ),
    },
    {
      field: 'prediccion',
      headerName: 'Prediccion',
      flex: 0.5,
      renderCell: (params) => (
        <strong>
          <IconButton
            variant="contained"
            color="primary"
            size="small"
            onClick={() => {
              Swal.fire({
                title: 'Estas seguro?',
                text: 'Realizar la prediccion del partido!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, realizar!',
              }).then((result) => {
                if (result.isConfirmed) {
                  //Swal cargando
                  Swal.fire({
                    title: 'Cargando...',
                    allowOutsideClick: false,
                    didOpen: () => {
                      Swal.showLoading()
                    },
                  })
                  createPrediccion({
                    id_partido: params.row.id_partido,
                    eqp1: params.row.id_equipo_loc,
                    eqp2: params.row.id_equipo_vis,
                  })
                    .unwrap()
                    .then((res) => {
                      console.log(res)
                      Swal.fire(
                        'Prediccion realizada! Gana: ' + res?.Ganador + '',
                        `${res?.Equipo_1} ${res?.Resultado_Equipo_1} - ${res?.Resultado_Equipo_2} ${res?.Equipo_2}`,
                        'success'
                      )
                    })
                    .catch((err) => {
                      console.log(err)
                      Swal.fire('Error!', 'Error al realizar la prediccion', 'error')
                    })
                }
              })
            }}
          >
            <WorkspacePremiumIcon />
          </IconButton>
          <IconButton
            variant="contained"
            color="primary"
            size="small"
            onClick={() => {
              dispatch(fetchPredictSuccess(params.row.id_partido))
              navigate('/empleado/showPredicciones')
            }}
          >
            <VisibilityOutlinedIcon />
          </IconButton>
        </strong>
      ),
    },
  ]

  return (
    <Box m="20px" sx={{ marginTop: '-25px' }}>
      <Header title="Partidos" subtitle="Lista de partidos registrados en el sistema" />
      {isFetching && (
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '60vh',
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            '& .MuiDataGrid-root': {
              border: 'none',
            },
            '& .MuiDataGrid-cell': {
              borderBottom: 'none',
            },
            '& .nombre-column--cell': {
              color: shades.primary[100],
            },
            '& .MuiDataGrid-columnHeaders': {
              backgroundColor: shades.primary[600],
              borderBottom: 'none',
              color: shades.grey[900],
            },
            '& .MuiDataGrid-virtualScroller': {
              backgroundColor: shades.primary[400],
            },
            '& .MuiDataGrid-footerContainer': {
              borderTop: 'none',
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            '& .MuiCheckbox-root': {
              color: `${shades.grey[100]} !important`,
            },
            '& .MuiDataGrid-toolbarContainer .MuiButton-text': {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            getRowId={(row) => row.id_partido}
            rows={data}
            isRowSelectable={(params) => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
      <ModalGol
        data={dataModal}
        jugadores={dataJugadores}
        fetch={isFetchingJugadores}
        open={openGoles}
        setData={setDataModal}
        setOpen={setOpenGoles}
        updateGol={updateGol}
      />
      <ModalTarjeta
        data={dataModal}
        jugadores={dataJugadores}
        fetch={isFetchingJugadores}
        open={openTarjetas}
        setData={setDataModal}
        setOpen={setOpenTarjetas}
        updateTarjeta={updateTarjeta}
      />
      <ModalEstado
        data={dataModal}
        open={openEstado}
        setData={setDataModal}
        setOpen={setOpenEstado}
        updateEstado={updateEstado}
      />
    </Box>
  )
}

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '50%',
  height: '60%',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
}

function ModalGol({ data = {}, jugadores = {}, fetch, updateGol, open, setOpen }) {
  const handleClose = () => setOpen(false)
  const [gol, setGol] = useState({
    id_jugador: 0,
    minuto: '',
    tipo: '',
    distancia: '',
  })
  const [local, setLocal] = useState(false)
  const [visitante, setVisitante] = useState(false)

  const handle = async () => {
    await updateGol({
      ...gol,
      id_partido: data.id_partido,
      id_equipo: local
        ? gol.tipo === '2'
          ? data.id_equipo_vis
          : data.id_equipo_loc
        : gol.tipo === '2'
        ? data.id_equipo_loc
        : data.id_equipo_vis,
    })
      .unwrap()
      .then((res) => {
        handleClose()

        Swal.fire({
          icon: 'success',
          title: 'Gol registrado',
          showConfirmButton: false,
          timer: 1500,
        })
        new Promise((resolve) => setTimeout(resolve, 1000)).then(() => window.location.reload())
      })
      .catch((err) => {
        handleClose()

        Swal.fire({
          icon: 'error',
          title: 'Error al registrar gol',
          showConfirmButton: false,
          timer: 1500,
        })
      })
  }

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style} gap="30px" display="grid" gridTemplateColumns="repeat(4, minmax(0, 1fr))">
          {fetch ? (
            <CircularProgress />
          ) : (
            <>
              <Typography
                sx={{ gridColumn: 'span 4' }}
                id="modal-modal-title"
                variant="h2"
                component="h2"
              >
                Agregar Gol
              </Typography>
              <FormControlLabel
                sx={{ gridColumn: 'span 2' }}
                control={
                  <Switch
                    checked={local}
                    color="primary"
                    onChange={() => {
                      setLocal(!local)
                      setVisitante(false)
                    }}
                    inputProps={{ 'aria-label': 'controlled' }}
                  />
                }
                label="Local"
              />
              <FormControlLabel
                sx={{ gridColumn: 'span 2' }}
                control={
                  <Switch
                    checked={visitante}
                    label="Visitante"
                    onChange={() => {
                      setVisitante(!visitante)
                      setLocal(false)
                    }}
                    inputProps={{ 'aria-label': 'controlled' }}
                  />
                }
                label="Visitante"
              />

              <TextField
                id="outlined-select-currency-native"
                label="Equipo Local"
                variant="outlined"
                value={data.equipo_loc}
                disabled
              />
              <FormControl>
                <InputLabel id="demo-simple-select-label">Jugador Local</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={gol.id_jugador}
                  disabled={!local}
                  label="Jugador Local"
                  onChange={(e) => setGol({ ...gol, id_jugador: e.target.value })}
                >
                  {jugadores?.equipo_local?.map((jugador) => (
                    <MenuItem value={jugador.id_jugador}>{jugador.nombre}</MenuItem>
                  ))}
                </Select>
              </FormControl>

              <TextField
                id="outlined-select-currency-native"
                label="Equipo Visitante"
                variant="outlined"
                value={data.equipo_vis}
                disabled
              />
              <FormControl>
                <InputLabel id="demo-simple-select-label">Jugador Visitante</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={gol.id_jugador}
                  label="Jugador Visitante"
                  disabled={!visitante}
                  onChange={(e) => setGol({ ...gol, id_jugador: e.target.value })}
                >
                  {jugadores?.equipo_visitante?.map((jugador) => (
                    <MenuItem value={jugador.id_jugador}>{jugador.nombre}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <FormControl sx={{ gridColumn: 'span 2' }}>
                <InputLabel id="demo-simple-select-label">Tipo</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={gol.tipo}
                  label="Tipo"
                  onChange={(e) => setGol({ ...gol, tipo: e.target.value })}
                >
                  <MenuItem value="2"> Gol en Contra</MenuItem>
                  <MenuItem value="3"> Tiro Libre</MenuItem>
                  <MenuItem value="4"> Jugada</MenuItem>
                </Select>
              </FormControl>
              <TextField
                id="outlined-select-currency-native"
                label="Minuto"
                variant="outlined"
                value={gol.minuto}
                onChange={(e) => setGol({ ...gol, minuto: e.target.value })}
              />
              <TextField
                id="outlined-select-currency-native"
                label="Distancia"
                variant="outlined"
                value={gol.distancia}
                onChange={(e) => setGol({ ...gol, distancia: e.target.value })}
              />

              <Box sx={{ gridColumn: 'span 4' }} display="flex" justifyContent="flex-end">
                <Button
                  variant="contained"
                  color="primary"
                  sx={{ marginRight: '15px' }}
                  onClick={handle}
                >
                  Guardar Gol
                </Button>
                <Button variant="contained" color="secondary" onClick={handleClose}>
                  Cancelar
                </Button>
              </Box>
            </>
          )}
        </Box>
      </Modal>
    </div>
  )
}

function ModalTarjeta({ data = {}, jugadores = {}, fetch, updateTarjeta, open, setOpen }) {
  const handleClose = () => setOpen(false)
  const [Tarjeta, setTarjeta] = useState({
    id_jugador: 0,
    minuto: '',
    tipo: 1,
  })
  const [local, setLocal] = useState(false)
  const [visitante, setVisitante] = useState(false)

  const handle = async () => {
    await updateTarjeta({
      ...Tarjeta,
      id_partido: data.id_partido,
      id_equipo: local ? data.id_equipo_loc : data.id_equipo_vis,
    })
      .unwrap()
      .then((res) => {
        handleClose()
        Swal.fire({
          icon: 'success',
          title: 'Tarjeta registrada correctamente',
          showConfirmButton: false,
          timer: 1500,
        })
        new Promise((resolve) => setTimeout(resolve, 1000)).then(() => window.location.reload())
        window.location.reload()
      })
      .catch((err) => {
        handleClose()
        Swal.fire({
          icon: 'error',
          title: 'Error al registrar la tarjeta',
          showConfirmButton: false,
          timer: 1500,
        })
      })
  }

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style} gap="30px" display="grid" gridTemplateColumns="repeat(4, minmax(0, 1fr))">
          {fetch ? (
            <CircularProgress />
          ) : (
            <>
              <Typography
                sx={{ gridColumn: 'span 4' }}
                id="modal-modal-title"
                variant="h2"
                component="h2"
              >
                Agregar Tarjeta
              </Typography>
              <FormControlLabel
                sx={{ gridColumn: 'span 2' }}
                control={
                  <Switch
                    checked={local}
                    color="primary"
                    onChange={() => {
                      setLocal(!local)
                      setVisitante(false)
                    }}
                    inputProps={{ 'aria-label': 'controlled' }}
                  />
                }
                label="Local"
              />
              <FormControlLabel
                sx={{ gridColumn: 'span 2' }}
                control={
                  <Switch
                    checked={visitante}
                    label="Visitante"
                    onChange={() => {
                      setVisitante(!visitante)
                      setLocal(false)
                    }}
                    inputProps={{ 'aria-label': 'controlled' }}
                  />
                }
                label="Visitante"
              />

              <TextField
                id="outlined-select-currency-native"
                label="Equipo Local"
                variant="outlined"
                value={data.equipo_loc}
                disabled
              />
              <FormControl>
                <InputLabel id="demo-simple-select-label">Jugador Local</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={Tarjeta.id_jugador}
                  disabled={!local}
                  label="Jugador Local"
                  onChange={(e) => setTarjeta({ ...Tarjeta, id_jugador: e.target.value })}
                >
                  {jugadores?.equipo_local?.map((jugador) => (
                    <MenuItem value={jugador.id_jugador}>{jugador.nombre}</MenuItem>
                  ))}
                </Select>
              </FormControl>

              <TextField
                id="outlined-select-currency-native"
                label="Equipo Visitante"
                variant="outlined"
                value={data.equipo_vis}
                disabled
              />
              <FormControl>
                <InputLabel id="demo-simple-select-label">Jugador Visitante</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={Tarjeta.id_jugador}
                  label="Jugador Visitante"
                  disabled={!visitante}
                  onChange={(e) => setTarjeta({ ...Tarjeta, id_jugador: e.target.value })}
                >
                  {jugadores?.equipo_visitante?.map((jugador) => (
                    <MenuItem value={jugador.id_jugador}>{jugador.nombre}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <TextField
                sx={{ gridColumn: 'span 2' }}
                id="outlined-select-currency-native"
                label="Minuto"
                variant="outlined"
                value={Tarjeta.minuto}
                onChange={(e) => setTarjeta({ ...Tarjeta, minuto: e.target.value })}
              />
              <FormControl sx={{ gridColumn: 'span 2' }}>
                <InputLabel id="demo-simple-select-label">Tarjeta</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={Tarjeta.tipo_tarjeta}
                  label="Tarjeta"
                  onChange={(e) => setTarjeta({ ...Tarjeta, tipo_tarjeta: e.target.value })}
                >
                  <MenuItem value="1">Amarilla</MenuItem>
                  <MenuItem value="2">Roja</MenuItem>
                </Select>
              </FormControl>

              <Box sx={{ gridColumn: 'span 4' }} display="flex" justifyContent="flex-end">
                <Button
                  variant="contained"
                  color="primary"
                  sx={{ marginRight: '15px' }}
                  onClick={handle}
                >
                  Guardar Tarjeta
                </Button>
                <Button variant="contained" color="secondary" onClick={handleClose}>
                  Cancelar
                </Button>
              </Box>
            </>
          )}
        </Box>
      </Modal>
    </div>
  )
}

function ModalEstado({ data = {}, updateEstado, open, setOpen }) {
  const handleClose = () => {
    setOpen(false)
  }

  const [estado, setEstado] = useState(data?.id_estado)

  const handle = async () => {
    await updateEstado({ id_partido: data.id_partido, estado: estado })
      .unwrap()
      .then((res) => {
        Swal.fire({
          icon: 'success',
          title: 'Estado actualizado',
          showConfirmButton: false,
          timer: 1500,
        })
        new Promise((resolve) => setTimeout(resolve, 1000)).then(() => window.location.reload())
        window.location.reload()
      })
      .catch((err) => {
        Swal.fire({
          icon: 'error',
          title: 'Error al actualizar estado',
          showConfirmButton: false,
          timer: 1500,
        })
      })
    handleClose()
  }

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style} gap="30px" display="grid" gridTemplateColumns="repeat(4, minmax(0, 1fr))">
          <Typography
            sx={{ gridColumn: 'span 4' }}
            id="modal-modal-title"
            variant="h2"
            component="h2"
          >
            Cambiar Estado
          </Typography>
          <FormControl sx={{ gridColumn: 'span 4' }}>
            <InputLabel id="demo-simple-select-label">Estado</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={estado}
              label="Estado"
              onChange={(e) => setEstado(e.target.value)}
            >
              <MenuItem value={1}>Primer Tiempo </MenuItem>
              <MenuItem value={2}>Descanso</MenuItem>
              <MenuItem value={3}>Segundo Tiempo</MenuItem>
              <MenuItem value={4}>Finalizado</MenuItem>
              <MenuItem value={5}>Cancelado</MenuItem>
              <MenuItem value={6}>Pendiente</MenuItem>
            </Select>
          </FormControl>

          <Box sx={{ gridColumn: 'span 4' }} display="flex" justifyContent="flex-end">
            <Button
              variant="contained"
              color="primary"
              sx={{ marginRight: '15px' }}
              onClick={handle}
            >
              Guardar Estado
            </Button>
            <Button variant="contained" color="secondary" onClick={handleClose}>
              Cancelar
            </Button>
          </Box>
        </Box>
      </Modal>
    </div>
  )
}

export default ShowPartidos
