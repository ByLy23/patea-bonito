import React from "react";
import {
  Box,
  Button,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
} from "@mui/material";
import { Formik } from "formik";
import * as yup from "yup";
import useMediaQuery from "@mui/material/useMediaQuery";
import Header from "../../../components/Header";
import {
  useGetAsignacionesQuery,
  useAsignarEquipoMutation,
} from "../../../app/api/AccionesEmp/apiAsignacion";
import Swal from "sweetalert2";

const initialValues = {
  id_competencia: "",
  id_equipo: "",
};

export default function ToCompe({ competencias = [] }) {
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const [isSubmitting, setIsSubmitting] = React.useState(false);
  const [id, setId] = React.useState(0);

  const { data: equipos = [] } = useGetAsignacionesQuery({ id, opcion: 2 });
  const [asignarEquipo] = useAsignarEquipoMutation();

  const handleFormSubmit = async (values, onSubmitProps) => {
    setIsSubmitting(true);
    console.log(values);
    console.log(id);
    await asignarEquipo({ id_competencia: id, id_equipo: values.id_equipo })
      .unwrap()
      .then((result) => {
        if (result) {
          Swal.fire({
            title: "Equipo asignado a competencia",
            icon: "success",
            confirmButtonText: "Aceptar",
          });
        }
      })
      .catch((error) => {
        Swal.fire({
          title: "Error al asignar el equipo a la competencia",
          icon: "error",
          confirmButtonText: "Aceptar",
        });
      });
    setIsSubmitting(false);
    onSubmitProps.resetForm();
  };
  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header
        title="Equipo a Competencia"
        subtitle="Se muestran todos los equipos disponibles para agregar a la competencia"
      />
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormSubmit}
        validationSchema={checkoutSchema}
      >
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          setFieldValue,
        }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
              }}
            >
              <FormControl fullWidth sx={{ gridColumn: "span 4" }}>
                <InputLabel id="demo-simple-select-label-19" variant="filled">
                  Competencias
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label-19"
                  label="Competencias"
                  variant="filled"
                  id="demo-simple-select-1"
                  onChange={(e) => {
                    console.log(values);
                    setId(e.target.value);
                  }}
                  value={id}
                  name="id_competencia"
                >
                  {competencias.map((equipo) => (
                    <MenuItem value={equipo.id_competencia}>
                      {equipo.nombre}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <FormControl fullWidth sx={{ gridColumn: "span 4" }}>
                <InputLabel id="demo-simple-select-label-19" variant="filled">
                  Equipo
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label-19"
                  label="Equipo"
                  variant="filled"
                  id="demo-simple-select-1"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.id_equipo}
                  name="id_equipo"
                  error={
                    Boolean(touched.id_equipo) && Boolean(errors.id_equipo)
                  }
                  helperText={touched.id_equipo && errors.id_equipo}
                >
                  {equipos.map((equipo) => (
                    <MenuItem value={equipo.id_equipo}>
                      {equipo.equipo}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>

            <Box display="flex" justifyContent="end" mt="20px">
              <Button
                type="submit"
                color="primary"
                variant="contained"
                disabled={isSubmitting}
              >
                Registrar Asignacion
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <div style={{ height: "2rem" }}></div>
    </Box>
  );
}

const checkoutSchema = yup.object().shape({
  id_equipo: yup.string().required("Campo requerido"),
});
