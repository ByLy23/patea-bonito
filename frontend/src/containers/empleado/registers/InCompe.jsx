import React from "react";
import {
  Box,
  MenuItem,
  IconButton,
  Select,
  FormControl,
  InputLabel,
  TextField,
  Button,
  Modal,
  Typography,
} from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import Header from "../../../components/Header";
import {
  useGetAsignacionesQuery,
  useActualizarAsignacionMutation,
} from "../../../app/api/AccionesEmp/apiAsignacion";
import { shades } from "../../../style/theme";
import EditIcon from "@mui/icons-material/Edit";
import Swal from "sweetalert2";

import CircularProgress from "@mui/material/CircularProgress";

export default function InCompe({ competencias = [] }) {
  const [id, setId] = React.useState(0);
  const [open, setOpen] = React.useState(false);
  const [dataModal, setDataModal] = React.useState({});
  const [actualizarAsignacion] = useActualizarAsignacionMutation();
  const { data = [], isFetching } = useGetAsignacionesQuery({ id, opcion: 1 });

  const columns = [
    { field: "id_equipo", headerName: "ID", flex: 1 },
    { field: "equipo", headerName: "Nombre", flex: 1.2 },
    {
      field: "Acciones",
      headerName: "Acciones",
      flex: 1.2,
      renderCell: (params) => (
        <strong>
          <IconButton
            variant="contained"
            color="primary"
            size="small"
            style={{ marginRight: 16 }}
            onClick={() => {
              setOpen(true);
              setDataModal(params.row);
            }}
          >
            <EditIcon />
          </IconButton>
        </strong>
      ),
    },
  ];

  return (
    <>
      <Box m="20px" sx={{ marginTop: "-25px" }}>
        <Header
          title="Equipos inscritos"
          subtitle="Se muestran todos los equipos inscritos en la competencia"
        />
        <FormControl
          fullWidth
          sx={{ gridColumn: "span 4", marginBottom: "25px" }}
        >
          <InputLabel id="demo-simple-select-label-19" variant="filled">
            Competencias
          </InputLabel>
          <Select
            labelId="demo-simple-select-label-19"
            label="Competencias"
            variant="filled"
            id="demo-simple-select-1"
            onChange={(e) => {
              setId(e.target.value);
            }}
            value={id}
            name="id_competencia"
          >
            {competencias.map((equipo) => (
              <MenuItem value={equipo.id_competencia}>{equipo.nombre}</MenuItem>
            ))}
          </Select>
        </FormControl>
        {isFetching && (
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "60vh",
            }}
          >
            <CircularProgress />
          </Box>
        )}

        {!isFetching && (
          <Box
            m="40px 0 -10 0"
            height="75vh"
            sx={{
              "& .MuiDataGrid-root": {
                border: "none",
              },
              "& .MuiDataGrid-cell": {
                borderBottom: "none",
              },
              "& .nombre-column--cell": {
                color: shades.primary[100],
              },
              "& .MuiDataGrid-columnHeaders": {
                backgroundColor: shades.primary[600],
                borderBottom: "none",
                color: shades.grey[900],
              },
              "& .MuiDataGrid-virtualScroller": {
                backgroundColor: shades.primary[400],
              },
              "& .MuiDataGrid-footerContainer": {
                borderTop: "none",
                backgroundColor: shades.primary[600],
                color: shades.grey[900],
              },
              "& .MuiCheckbox-root": {
                color: `${shades.grey[100]} !important`,
              },
              "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
                color: `${shades.grey[100]} !important`,
              },
            }}
          >
            <DataGrid
              getRowId={(row) => row.id_equipo}
              rows={data}
              isRowSelectable={() => false}
              columns={columns}
              components={{ Toolbar: GridToolbar }}
            />
          </Box>
        )}
      </Box>
      <ModalEstado
        data={dataModal}
        open={open}
        setOpen={setOpen}
        setData={setDataModal}
        handleEdit={actualizarAsignacion}
        id_competencia={id}
      />
    </>
  );
}

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function ModalEstado({
  data = {},
  open,
  setData,
  setOpen,
  handleEdit,
  id_competencia,
}) {
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={style}
          gap="30px"
          display="grid"
          gridTemplateColumns="repeat(4, minmax(0, 1fr))"
        >
          <Typography
            sx={{ gridColumn: "span 4" }}
            id="modal-modal-title"
            variant="h2"
            component="h2"
          >
            Editar Estado Equipo
          </Typography>
          <TextField
            sx={{ gridColumn: "span 4" }}
            fullWidth
            id="outlined-basic"
            label="nombre"
            variant="outlined"
            value={data.equipo}
            disabled
          />
          <FormControl fullWidth sx={{ gridColumn: "span 4" }}>
            <InputLabel id="demo-simple-select-label546" variant="filled">
              Estado
            </InputLabel>
            <Select
              labelId="demo-simple-select-label546"
              label="Estado"
              variant="outlined"
              id="demo-simple-select"
              onChange={(e) => {
                setData({ ...data, id_estado: e.target.value });
              }}
              name="estado"
            >
              <MenuItem value="1">Inscrito</MenuItem>
              <MenuItem value="2">Ganador</MenuItem>
            </Select>
          </FormControl>
          <Box
            sx={{ gridColumn: "span 4" }}
            display="flex"
            justifyContent="flex-end"
          >
            <Button
              variant="contained"
              color="primary"
              sx={{ marginRight: "15px" }}
              onClick={() => {
                handleEdit({
                  ...data,
                  id_competencia,
                })
                  .unwrap()
                  .then((res) => {
                    Swal.fire({
                      icon: "success",
                      title: "Estado actualizado",
                      showConfirmButton: false,
                      timer: 1500,
                    });
                  })
                  .catch((err) => {
                    Swal.fire({
                      icon: "error",
                      title: "Error al actualizar",
                      showConfirmButton: false,
                      timer: 1500,
                    });
                  });
                handleClose();
              }}
            >
              Actualizar Estado
            </Button>
            <Button variant="contained" color="secondary" onClick={handleClose}>
              Cancelar
            </Button>
          </Box>
        </Box>
      </Modal>
    </div>
  );
}
