import React from 'react'
import { Box, IconButton, Typography } from '@mui/material'
import { DataGrid, GridToolbar } from '@mui/x-data-grid'
import { shades } from '../../../style/theme'
import Header from '../../../components/Header'
import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import CircularProgress from '@mui/material/CircularProgress'
import Swal from 'sweetalert2'
import {
  useDeleteArbitroMutation,
  useUpdateArbitroMutation,
} from '../../../app/api/datosDeportivos/apiArbitro'
import ModalArbitro from '../../../components/ModalArbitro'

const ListArbitro = ({ data = [], isFetching = true }) => {
  const [updateUsuario] = useUpdateArbitroMutation()
  const [deleteJugador] = useDeleteArbitroMutation()

  const [open, setOpen] = React.useState(false)
  const [dataModal, setDataModal] = React.useState({})

  const [openEstado, setOpenEstado] = React.useState(false)

  const columns = [
    { field: 'id_arbitro', headerName: 'ID', flex: 0.5 },

    {
      field: 'fecha_nac',
      headerName: 'Fecha Nacimiento',
      flex: 1,
    },
    {
      field: 'nombre',
      headerName: 'Nombre',
      flex: 1,
      cellClassnombre: 'nombre-column--cell',
    },
    {
      field: 'pais',
      headerName: 'Pais',
      flex: 1,
    },
    {
      field: 'Acciones',
      headerName: 'Acciones',
      flex: 1.2,
      renderCell: (params) => (
        <strong>
          <IconButton
            variant="contained"
            color="primary"
            size="small"
            style={{ marginRight: 16 }}
            onClick={() => {
              setOpen(true)
              setDataModal(params.row)
            }}
          >
            <EditIcon />
          </IconButton>
          <IconButton
            variant="contained"
            color="danger"
            size="small"
            onClick={() => {
              Swal.fire({
                title: '¿Estas seguro?',
                text: 'No podras revertir esta accion!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminar!',
              }).then((result) => {
                if (result.isConfirmed) {
                  deleteJugador(params.row.id_arbitro)
                    .unwrap()
                    .then(() => {
                      Swal.fire('Eliminado!', 'El arbitro ha sido eliminado.', 'success')
                    })
                }
              })
            }}
          >
            <DeleteIcon />
          </IconButton>
        </strong>
      ),
    },
  ]

  return (
    <Box m="20px" sx={{ marginTop: '-25px' }}>
      <Header title="Arbitros" subtitle="Lista de arbitros registrados en el sistema" />
      {isFetching && (
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '60vh',
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            '& .MuiDataGrid-root': {
              border: 'none',
            },
            '& .MuiDataGrid-cell': {
              borderBottom: 'none',
            },
            '& .nombre-column--cell': {
              color: shades.primary[100],
            },
            '& .MuiDataGrid-columnHeaders': {
              backgroundColor: shades.primary[600],
              borderBottom: 'none',
              color: shades.grey[900],
            },
            '& .MuiDataGrid-virtualScroller': {
              backgroundColor: shades.primary[400],
            },
            '& .MuiDataGrid-footerContainer': {
              borderTop: 'none',
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            '& .MuiCheckbox-root': {
              color: `${shades.grey[100]} !important`,
            },
            '& .MuiDataGrid-toolbarContainer .MuiButton-text': {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            getRowId={(row) => row.id_arbitro}
            rows={data}
            isRowSelectable={(params) => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
      <ModalArbitro
        open={open}
        setOpen={setOpen}
        data={dataModal}
        setData={setDataModal}
        handleEdit={async (data) => {
          setOpen(false)
          Swal.fire('Actualizando...')
          console.log(data)
          updateUsuario(data)
            .unwrap()
            .then((res) => {
              Swal.fire('Actualizado', 'El Arbitro ha sido actualizado', 'success')
            })
            .catch((err) => {
              Swal.fire('Error', 'No se pudo actualizar el arbitro', 'error')
            })
        }}
      />
    </Box>
  )
}

export default ListArbitro
