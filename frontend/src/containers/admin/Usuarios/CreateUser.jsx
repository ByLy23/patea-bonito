import React from "react";
import {
  Box,
  Button,
  TextField,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
  useTheme,
  Typography,
} from "@mui/material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import FlexBetween from "../../../components/Flex";
import { Formik } from "formik";
import * as yup from "yup";
import useMediaQuery from "@mui/material/useMediaQuery";
import Header from "../../../components/Header";
import InputAdornment from "@mui/material/InputAdornment";
import TodayIcon from "@mui/icons-material/Today";
import Dropzone from "react-dropzone";
import { useCreateUsuarioMutation } from "../../../app/api/users/apiUsuario";
import { useGetPaisesQuery } from "../../../app/api/func/Pais";
import Swal from "sweetalert2";
import { useSelector } from "react-redux";

const initialValues = {
  apellido: "",
  nombre: "",
  correo: "",
  direccion: "",
  telefono: "",
  clave_acceso: "",
  id_genero: "",
  fecha_nac: "",
  id_pais: "",
  id_rol: "3",
  picture: "",
  fotografia: "",
  estado: "Alta",
};

const CreateUser = () => {
  const isNonMobile = useMediaQuery("(min-width:600px)");
  const { palette } = useTheme();
  const [isSubmitting, setIsSubmitting] = React.useState(false);
  const url = useSelector((state) => state.sesion.url);
  const num_grupo = useSelector((state) => state.sesion.num_grupo);

  const [createUsuario] = useCreateUsuarioMutation();

  const { data: paises = [] } = useGetPaisesQuery();

  const handleFormSubmit = async (values, onSubmitProps) => {
    setIsSubmitting(true);
    const data = {
      ...values,
      telefono: parseInt(values.telefono),
      id_pais: parseInt(values.id_pais),
      id_genero: parseInt(values.id_genero),
      id_rol: parseInt(values.id_rol),
      picture: "",
      num_grupo: num_grupo,
    }
    console.log(data);
    await createUsuario({
      newUsuario: data,
      url: url,
    })
      .unwrap()
      .then((res) => {
        Swal.fire({
          title: "Usuario Creado",
          text: "El usuario ha sido creado con exito",
          icon: "success",
          confirmButtonText: "Ok",
        });
        // borrar los valores del formulario
        setIsSubmitting(false);
        onSubmitProps.resetForm();
      })
      .catch((err) => {
        Swal.fire({
          title: "Error",
          text: "Ha ocurrido un error al crear el usuario",
          icon: "error",
          confirmButtonText: "Ok",
        });
        setIsSubmitting(false);
      });
  };

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header
        title="Crear Usuario"
        subtitle="Crear un nuevo perfil de usuario"
      />
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormSubmit}
        validationSchema={checkoutSchema}
      >
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          setFieldValue,
        }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                "& > div": { gridColumn: isNonMobile ? undefined : "span 4" },
              }}
            >
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Nombres"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.nombre}
                name="nombre"
                error={!!touched.nombre && !!errors.nombre}
                helperText={touched.nombre && errors.nombre}
                sx={{ gridColumn: "span 2" }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Apellidos"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.apellido}
                name="apellido"
                error={!!touched.apellido && !!errors.apellido}
                helperText={touched.apellido && errors.apellido}
                sx={{ gridColumn: "span 2" }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Correo Electrónico"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.correo}
                name="correo"
                error={!!touched.correo && !!errors.correo}
                helperText={touched.correo && errors.correo}
                sx={{ gridColumn: "span 2" }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Telefono"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.telefono}
                name="telefono"
                error={!!touched.telefono && !!errors.telefono}
                helperText={touched.telefono && errors.telefono}
                sx={{ gridColumn: "span 2" }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Direccion"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.direccion}
                name="direccion"
                error={!!touched.direccion && !!errors.direccion}
                helperText={touched.direccion && errors.direccion}
                sx={{ gridColumn: "span 2" }}
              />
              <FormControl fullWidth sx={{ gridColumn: "span 2" }}>
                <InputLabel id="demo-simple-select-label" variant="filled">
                  Genero
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  label="Genero"
                  variant="filled"
                  id="demo-simple-select"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.id_genero}
                  name="id_genero"
                  error={
                    Boolean(touched.id_genero) && Boolean(errors.id_genero)
                  }
                  helperText={touched.id_genero && errors.id_genero}
                >
                  <MenuItem value={2}>Masculino</MenuItem>
                  <MenuItem value={1}>Femenino</MenuItem>
                </Select>
              </FormControl>
              <TextField
                label="Fecha de nacimiento"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.fecha_nac}
                name="fecha_nac"
                error={Boolean(touched.fecha_nac) && Boolean(errors.fecha_nac)}
                helperText={touched.fecha_nac && errors.fecha_nac}
                sx={{ gridColumn: "span 2" }}
                fullWidth
                format="dd-MM-yyyy"
                type="date"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <TodayIcon />
                    </InputAdornment>
                  ),
                }}
                variant="filled"
              />
              <FormControl fullWidth sx={{ gridColumn: "span 2" }}>
                <InputLabel id="demo-simple-select-label-1" variant="filled">
                  Pais
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label-1"
                  label="Pais"
                  variant="filled"
                  id="demo-simple-select-1"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.id_pais}
                  name="id_pais"
                  error={Boolean(touched.id_pais) && Boolean(errors.id_pais)}
                  helperText={touched.id_pais && errors.id_pais}
                >
                  {paises.map((pais) => (
                    <MenuItem value={pais.id_pais}>{pais.nombre}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <TextField
                fullWidth
                variant="filled"
                type="password"
                label="Contraseña"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.clave_acceso}
                name="clave_acceso"
                error={!!touched.clave_acceso && !!errors.clave_acceso}
                helperText={touched.clave_acceso && errors.clave_acceso}
                sx={{ gridColumn: "span 2" }}
              />
              <FormControl fullWidth sx={{ gridColumn: "span 2" }}>
                <InputLabel id="demo-simple-select-label-1" variant="filled">
                  Tipo de usuario
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label-1"
                  label="Tipo de usuario"
                  variant="filled"
                  id="demo-simple-select-1"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.id_rol}
                  name="id_rol"
                  error={Boolean(touched.id_rol) && Boolean(errors.id_rol)}
                  helperText={touched.id_rol && errors.id_rol}
                >
                  <MenuItem value={1}>Admin</MenuItem>
                  <MenuItem value={2}>Empleado</MenuItem>
                  <MenuItem value={3}>Cliente</MenuItem>
                </Select>
              </FormControl>
              <Box
                gridColumn="span 4"
                borderBottom={`2px solid ${palette.neutral.medium}`}
                borderRadius="5px"
                p="1px"
                style={{ backgroundColor: palette.neutral.light }}
              >
                <Dropzone
                  acceptedFiles=".jpg,.jpeg,.png"
                  multiple={false}
                  onDrop={(acceptedFiles) =>
                    // Picture in base64
                    new Promise((resolve, reject) => {
                      const fileReader = new FileReader();
                      fileReader.readAsDataURL(acceptedFiles[0]);
                      fileReader.onload = () => {
                        setFieldValue(
                          "picture",
                          acceptedFiles[0],
                          setFieldValue(
                            "fotografia",
                            fileReader.result.split(",")[1]
                          )
                        );
                      };
                      fileReader.onerror = (error) => {
                        reject(error);
                      };
                    })
                  }
                >
                  {({ getRootProps, getInputProps }) => (
                    <Box
                      {...getRootProps()}
                      p="2px"
                      sx={{ "&:hover": { cursor: "pointer" } }}
                      style={{ backgroundColor: palette.neutral.light }}
                    >
                      <input
                        {...getInputProps()}
                        error={
                          Boolean(touched.picture) && Boolean(errors.picture)
                        }
                        helperText={touched.picture && errors.picture}
                      />
                      {!values.picture ? (
                        <p>Agregar fotografia aqui</p>
                      ) : (
                        <FlexBetween>
                          <Typography>{values.picture.name}</Typography>
                          <EditOutlinedIcon />
                        </FlexBetween>
                      )}
                    </Box>
                  )}
                </Dropzone>
              </Box>
            </Box>

            <Box display="flex" justifyContent="end" mt="20px">
              <Button
                type="submit"
                color="primary"
                variant="contained"
                disabled={isSubmitting}
              >
                Crear Nuevo Usuario
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <div style={{ height: "2rem" }}></div>
    </Box>
  );
};

const phoneRegExp =
  /^((\+[1-9]{1,4}[ -]?)|(\([0-9]{2,3}\)[ -]?)|([0-9]{2,4})[ -]?)*?[0-9]{3,4}[ -]?[0-9]{3,4}$/;

const checkoutSchema = yup.object().shape({
  nombre: yup.string().required("Campo requerido"),
  apellido: yup.string().required("Campo requerido"),
  correo: yup.string().email("correo invalido").required("Campo requerido"),
  direccion: yup.string().required("Campo requerido"),
  telefono: yup
    .string()
    .matches(phoneRegExp, "Numero invalido")
    .required("Campo requerido"),
  clave_acceso: yup.string().required("Campo requerido"),
  id_genero: yup.string().required("Campo requerido"),
  fecha_nac: yup.string().required("Campo requerido"),
  id_pais: yup.string().required("Campo requerido"),
  id_rol: yup.string().required("Campo requerido"),
  fotografia: yup.mixed().required("Campo requerido"),
});

export default CreateUser;
