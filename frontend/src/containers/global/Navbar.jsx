import { Box, IconButton } from "@mui/material";
import { PersonOutline } from "@mui/icons-material";
import HomeIcon from "@mui/icons-material/Home";
import { useNavigate } from "react-router-dom";
import { shades } from "../../style/theme";
import logo from "../../assets/Logo_con_slogan.png";
import ExplicitIcon from '@mui/icons-material/Explicit';

function Navbar({ params }) {
  const navigate = useNavigate();

  return (
    <Box
      display="flex"
      alignItems="center"
      width="100%"
      height="70px"
      backgroundColor="rgba(255, 255, 255, 0.95)"
      color="black"
      position="fixed"
      top="0"
      left="0"
      zIndex="1"
    >
      <Box
        width="80%"
        margin="auto"
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        marginTop="-30px"
      >
        <Box
          onClick={() => navigate("/")}
          sx={{ "&:hover": { cursor: "pointer" } }}
          color={shades.primary[400]}
        >
          <img src={logo} width="140" height="125" />
        </Box>
        <Box
          display="flex"
          justifyContent="space-between"
          columnGap="20px"
          zIndex="2"
        >
          {params ? (
            <IconButton
              sx={{ color: "black", "&:hover": { cursor: "pointer" } }}
              onClick={() => navigate("/login")}
            >
              <PersonOutline />
            </IconButton>
          ) : (
            <>
              <IconButton
                sx={{ color: "black", "&:hover": { cursor: "pointer" } }}
                onClick={() => navigate("/")}
              >
                <HomeIcon />
              </IconButton>
              <IconButton
                sx={{ color: "black", "&:hover": { cursor: "pointer" } }}
                onClick={() => navigate("/ESB")}
              >
                <ExplicitIcon />
              </IconButton>
            </>
          )}
        </Box>
      </Box>
    </Box>
  );
}

export default Navbar;
