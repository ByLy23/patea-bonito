import React from 'react'
import {
  Box,
  Button,
  TextField,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
  useTheme,
} from '@mui/material'
import { Formik } from 'formik'
import * as yup from 'yup'
import useMediaQuery from '@mui/material/useMediaQuery'
import Header from '../../components/Header'
import InputAdornment from '@mui/material/InputAdornment'
import TodayIcon from '@mui/icons-material/Today'
import { useGetPaisesQuery } from '../../app/api/func/Pais'
import Swal from 'sweetalert2'
import { useCreateEquipoMutation } from '../../app/api/datosDeportivos/apiEquipo'

const initialValues = {
  nombre: '',
  fecha: '',
  id_pais: '',
  tipo: 1,
}

const CreateEquipo = () => {
  const isNonMobile = useMediaQuery('(min-width:600px)')
  const { palette } = useTheme()
  const [isSubmitting, setIsSubmitting] = React.useState(false)

  const [createEquipo] = useCreateEquipoMutation()

  const { data: paises = [] } = useGetPaisesQuery()

  const handleFormSubmit = async (values) => {
    setIsSubmitting(true)
    const data = {
      ...values,
      id_pais: parseInt(values.id_pais),
      nombre: values.nombre,
    }
    console.log(data)
    await createEquipo(data)
      .unwrap()
      .then((res) => {
        Swal.fire({
          title: 'Equipo Creado',
          text: 'El Equipo ha sido creado con exito',
          icon: 'success',
          confirmButtonText: 'Ok',
        })
      })
      .catch((err) => {
        console.log(err)
        Swal.fire({
          title: 'Error',
          text: 'Ha ocurrido un error al crear el equipo',
          icon: 'error',
          confirmButtonText: 'Ok',
        })
      })
    // borrar los valores del formulario
    setIsSubmitting(false)
    values = initialValues
  }

  return (
    <Box m="20px" sx={{ marginTop: '-25px' }}>
      <Header title="Crear Equipo" subtitle="Crear un nuevo equipo" />
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormSubmit}
        validationSchema={checkoutSchema}
      >
        {({ values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                '& > div': { gridColumn: isNonMobile ? undefined : 'span 4' },
              }}
            >
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Nombre del equipo"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.nombre}
                name="nombre"
                error={!!touched.nombre && !!errors.nombre}
                helperText={touched.nombre && errors.nombre}
                sx={{ gridColumn: 'span 2' }}
              />
              <FormControl fullWidth sx={{ gridColumn: 'span 2' }}>
                <InputLabel id="demo-simple-select-label" variant="filled">
                  Tipo de equipo
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  label="Tipo de Equipo"
                  variant="filled"
                  id="demo-simple-select"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.tipo}
                  name="tipo"
                  error={Boolean(touched.tipo) && Boolean(errors.tipo)}
                  helperText={touched.tipo && errors.tipo}
                >
                  <MenuItem value={2}>Club</MenuItem>
                  <MenuItem value={1}>Nacional</MenuItem>
                </Select>
              </FormControl>
              <TextField
                label="Fecha de creacion"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.fecha}
                name="fecha"
                error={Boolean(touched.fecha) && Boolean(errors.fecha)}
                helperText={touched.fecha && errors.fecha}
                sx={{ gridColumn: 'span 2' }}
                fullWidth
                format="dd-MM-yyyy"
                type="date"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <TodayIcon />
                    </InputAdornment>
                  ),
                }}
                variant="filled"
              />
              <FormControl fullWidth sx={{ gridColumn: 'span 2' }}>
                <InputLabel id="demo-simple-select-label-1" variant="filled">
                  Pais
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label-1"
                  label="Pais"
                  variant="filled"
                  id="demo-simple-select-1"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.id_pais}
                  name="id_pais"
                  error={Boolean(touched.id_pais) && Boolean(errors.id_pais)}
                  helperText={touched.id_pais && errors.id_pais}
                >
                  {paises.map((pais) => (
                    <MenuItem value={pais.id_pais}>{pais.nombre}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>

            <Box display="flex" justifyContent="end" mt="20px">
              <Button type="submit" color="primary" variant="contained" disabled={isSubmitting}>
                Crear Nuevo Equipo
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <div style={{ height: '2rem' }}></div>
    </Box>
  )
}

const phoneRegExp =
  /^((\+[1-9]{1,4}[ -]?)|(\([0-9]{2,3}\)[ -]?)|([0-9]{2,4})[ -]?)*?[0-9]{3,4}[ -]?[0-9]{3,4}$/

const checkoutSchema = yup.object().shape({
  nombre: yup.string().required('Campo requerido'),
  tipo: yup.string().required('Campo requerido'),
  fecha: yup.string().required('Campo requerido'),
})

export default CreateEquipo
