import React from 'react'
import { Box, Button, TextField, MenuItem, Select, FormControl, InputLabel } from '@mui/material'
import { Formik } from 'formik'
import * as yup from 'yup'
import useMediaQuery from '@mui/material/useMediaQuery'
import Header from '../../components/Header'
import { useGetPaisesQuery } from '../../app/api/func/Pais'
import Swal from 'sweetalert2'
import { useCreateEstadioMutation } from '../../app/api/datosDeportivos/ApiEstadio'

const initialValues = {
  nombre: '',
  capacidad: '',
  id_pais: '',
}

const CreateEstadio = () => {
  const isNonMobile = useMediaQuery('(min-width:600px)')
  const [isSubmitting, setIsSubmitting] = React.useState(false)

  const [createEstadio] = useCreateEstadioMutation()

  const { data: paises = [] } = useGetPaisesQuery()

  const handleFormSubmit = async (values) => {
    setIsSubmitting(true)
    const data = {
      ...values,
      nombre: values.nombre,
      capacidad: parseInt(values.capacidad),
      id_pais: parseInt(values.id_pais),
    }
    await createEstadio(data)
      .unwrap()
      .then((res) => {
        Swal.fire({
          title: 'Estadio Creado',
          text: 'El Estadio ha sido creado con exito',
          icon: 'success',
          confirmButtonText: 'Ok',
        })
      })
      .catch((err) => {
        Swal.fire({
          title: 'Error',
          text: 'Ha ocurrido un error al crear el estadio',
          icon: 'error',
          confirmButtonText: 'Ok',
        })
      })
    // borrar los valores del formulario
    setIsSubmitting(false)
    values = initialValues
  }

  return (
    <Box m="20px" sx={{ marginTop: '-25px' }}>
      <Header title="Crear Estadio" subtitle="Crear un nuevo estadio" />
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormSubmit}
        validationSchema={checkoutSchema}
      >
        {({ values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue }) => (
          <form onSubmit={handleSubmit}>
            <Box
              display="grid"
              gap="30px"
              gridTemplateColumns="repeat(4, minmax(0, 1fr))"
              sx={{
                '& > div': { gridColumn: isNonMobile ? undefined : 'span 4' },
              }}
            >
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Nombre Estadio"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.nombre}
                name="nombre"
                error={!!touched.nombre && !!errors.nombre}
                helperText={touched.nombre && errors.nombre}
                sx={{ gridColumn: 'span 2' }}
              />
              <TextField
                fullWidth
                variant="filled"
                type="text"
                label="Capacidad del Estadio"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.capacidad}
                name="capacidad"
                error={!!touched.capacidad && !!errors.capacidad}
                helperText={touched.capacidad && errors.capacidad}
                sx={{ gridColumn: 'span 2' }}
              />
              <FormControl fullWidth sx={{ gridColumn: 'span 2' }}>
                <InputLabel id="demo-simple-select-label-1" variant="filled">
                  Pais
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label-1"
                  label="Pais"
                  variant="filled"
                  id="demo-simple-select-1"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.id_pais}
                  name="id_pais"
                  error={Boolean(touched.id_pais) && Boolean(errors.id_pais)}
                  helperText={touched.id_pais && errors.id_pais}
                >
                  {paises.map((pais) => (
                    <MenuItem value={pais.id_pais}>{pais.nombre}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>

            <Box display="flex" justifyContent="end" mt="20px">
              <Button type="submit" color="primary" variant="contained" disabled={isSubmitting}>
                Crear Nuevo Estadio
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <div style={{ height: '2rem' }}></div>
    </Box>
  )
}

const phoneRegExp =
  /^((\+[1-9]{1,4}[ -]?)|(\([0-9]{2,3}\)[ -]?)|([0-9]{2,4})[ -]?)*?[0-9]{3,4}[ -]?[0-9]{3,4}$/

const checkoutSchema = yup.object().shape({
  nombre: yup.string().required('Campo requerido'),
  capacidad: yup.string().required('Campo requerido'),
})

export default CreateEstadio
