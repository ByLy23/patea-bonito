import React from "react";
import {
  Box,
  IconButton,
  TextField,
  Button,
  Modal,
  Typography,
} from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { shades } from "../../style/theme";
import Header from "../../components/Header";
import DeleteIcon from "@mui/icons-material/Delete";
import CircularProgress from "@mui/material/CircularProgress";
import Swal from "sweetalert2";
import {
  useUpdatePartidoMutation,
  useDeletePartidoMutation,
} from "../../app/api/datosDeportivos/apiPartido";

const ListPartido = ({ data = [], isFetching = false }) => {
  const [open, setOpen] = React.useState(false);
  const [dataModal, setDataModal] = React.useState({});

  const [updatePartido] = useUpdatePartidoMutation();
  const [deletePartido] = useDeletePartidoMutation();

  const columns = [
    { field: "id_partido", headerName: "ID", flex: 0.2 },
    {
      field: "publico",
      headerName: "Publico",
      flex: 0.6,
      cellClassnombre: "nombre-column--cell",
    },
    /*{
      field: "fecha",
      headerName: "Fecha",
      flex: 1,
      cellClassnombre: "nombre-column--cell",
    },*/
    {
      field: "hora",
      headerName: "Hora",
      flex: 1.2,
    },
    {
      field: "gol_loc",
      headerName: "Local",
      flex: 0.5,
    },
    {
      field: "gol_vis",
      headerName: "Visitante",
      flex: 0.5,
    },
    {
      field: "equipo_loc",
      headerName: "Equipo Local",
      flex: 1,
    },
    {
      field: "equipo_vis",
      headerName: "Equipo Visitante",
      flex: 1,
    },
    {
      field: "estadio",
      headerName: "Estadio",
      flex: 1,
    },
    {
      field: "competencia",
      headerName: "Competencia",
      flex: 1,
    },
    {
      field: "estado",
      headerName: "Estado",
      flex: 0.8,
    },
    {
      field: "Acciones",
      headerName: "Acciones",
      flex: 1,
      renderCell: (params) => (
        <strong>
          {/*<IconButton
            variant="contained"
            color="primary"
            size="small"
            style={{ marginRight: 16 }}
            onClick={() => {
              setOpen(true);
              setDataModal(params.row);
            }}
          >
            <EditIcon />
          </IconButton>*/}
          <IconButton
            variant="contained"
            color="danger"
            size="small"
            onClick={() => {
              Swal.fire({
                title: "¿Estas seguro?",
                text: "No podras revertir esta accion!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Si, eliminar!",
              }).then(async (result) => {
                if (result.isConfirmed) {
                  console.log(params.row.id_partido);
                  const id = params.row.id_partido;
                  await deletePartido({id})
                    .unwrap()
                    .then(() => {
                      Swal.fire(
                        "Eliminado!",
                        "El Partido ha sido eliminado.",
                        "success"
                      );
                    });
                }
              });
            }}
          >
            <DeleteIcon />
          </IconButton>
        </strong>
      ),
    },
  ];

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header
        title="Partidos"
        subtitle="Lista de partidos registrados en el sistema"
      />
      {isFetching && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "60vh",
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .nombre-column--cell": {
              color: shades.primary[100],
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: shades.primary[600],
              borderBottom: "none",
              color: shades.grey[900],
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: shades.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            "& .MuiCheckbox-root": {
              color: `${shades.grey[100]} !important`,
            },
            "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            getRowId={(row) => row.id_partido}
            rows={data}
            isRowSelectable={(params) => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
      <ModalEdit
        open={open}
        setOpen={setOpen}
        data={dataModal}
        setData={setDataModal}
        handleEdit={async (data) => {
          setOpen(false);
          Swal.fire("Actualizando...");
          console.log(data);
          updatePartido(data)
            .unwrap()
            .then((res) => {
              Swal.fire(
                "Actualizado",
                "El Partido ha sido actualizado",
                "success"
              );
            })
            .catch((err) => {
              Swal.fire("Error", "No se pudo actualizar el Partido", "error");
            });
        }}
      />
    </Box>
  );
};

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function ModalEdit({ data = {}, open, setData, setOpen, handleEdit }) {
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={style}
          gap="30px"
          display="grid"
          gridTemplateColumns="repeat(4, minmax(0, 1fr))"
        >
          <Typography
            sx={{ gridColumn: "span 4" }}
            id="modal-modal-title"
            variant="h2"
            component="h2"
          >
            Editar Partido
          </Typography>
          <TextField
            sx={{ gridColumn: "span 4" }}
            fullWidth
            id="outlined-basic"
            label="fecha"
            variant="outlined"
            onChange={(e) => {
              setData({ ...data, fecha: e.target.value });
            }}
            value={data.fecha}
          />
          <TextField
            sx={{ gridColumn: "span 4" }}
            fullWidth
            id="outlined-basic"
            label="hora"
            variant="outlined"
            onChange={(e) => {
              setData({ ...data, hora: e.target.value });
            }}
            value={data.hora}
          />

          <Box
            sx={{ gridColumn: "span 4" }}
            display="flex"
            justifyContent="flex-end"
          >
            <Button
              variant="contained"
              color="primary"
              sx={{ marginRight: "15px" }}
              onClick={() => handleEdit(data)}
            >
              Actualizar
            </Button>
            <Button variant="contained" color="secondary" onClick={handleClose}>
              Cancelar
            </Button>
          </Box>
        </Box>
      </Modal>
    </div>
  );
}

export default ListPartido;
