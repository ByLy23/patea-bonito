import React from "react";
import { Box, IconButton } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { shades } from "../../style/theme";
import Header from "../../components/Header";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import CircularProgress from "@mui/material/CircularProgress";
import ModalEditJugador from "./ModalEditJugador";
import Swal from "sweetalert2";
import {
  useUpdateJugadorMutation,
  useDeleteJugadorMutation,
} from "../../app/api/datosDeportivos/apiJugadores";
import { useGetPaisesQuery } from "../../app/api/func/Pais";
import { useGetPosicionesQuery } from "../../app/api/func/Posicion";

const ListJugador = ({ data = [], isFetching = false }) => {
  const [updateJugador] = useUpdateJugadorMutation();
  const [deleteJugador] = useDeleteJugadorMutation();

  const [open, setOpen] = React.useState(false);
  const [dataModal, setDataModal] = React.useState({});

  const { data: paises = [] } = useGetPaisesQuery();

  const { data: posiciones = [] } = useGetPosicionesQuery();

  const columns = [
    { field: "id_jugador", headerName: "ID", flex: 0.5 },
    {
      field: "nombre",
      headerName: "Nombre",
      flex: 1.2,
      cellClassnombre: "nombre-column--cell",
    },
    {
      field: "fecha_nac",
      headerName: "Fecha Nacimiento",
      flex: 1.4,
      cellClassnombre: "nombre-column--cell",
    },
    {
      field: "pais",
      headerName: "Pais",
      flex: 1,
    },
    {
      field: "posicion",
      headerName: "Posicion",
      flex: 1,
    },
    {
      field: "equipo",
      headerName: "Equipo",
      flex: 1,
    },
    {
      field: "estado",
      headerName: "Estado",
      flex: 1,
    },
    {
      field: "Acciones",
      headerName: "Acciones",
      flex: 1.2,
      renderCell: (params) => (
        <strong>
          <IconButton
            variant="contained"
            color="primary"
            size="small"
            style={{ marginRight: 16 }}
            onClick={() => {
              setOpen(true);
              setDataModal(params.row);
            }}
          >
            <EditIcon />
          </IconButton>
          <IconButton
            variant="contained"
            color="danger"
            size="small"
            onClick={() => {
              Swal.fire({
                title: "¿Estas seguro?",
                text: "No podras revertir esta accion!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Si, eliminar!",
              }).then((result) => {
                if (result.isConfirmed) {
                  deleteJugador(params.row.id_jugador)
                    .unwrap()
                    .then(() => {
                      Swal.fire(
                        "Eliminado!",
                        "El jugador ha sido eliminado.",
                        "success"
                      );
                    });
                }
              });
            }}
          >
            <DeleteIcon />
          </IconButton>
        </strong>
      ),
    },
  ];

  return (
    <Box m="20px" sx={{ marginTop: "-25px" }}>
      <Header
        title="Jugadores"
        subtitle="Lista de jugadores registrados en el sistema"
      />
      {isFetching && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "60vh",
          }}
        >
          <CircularProgress />
        </Box>
      )}

      {!isFetching && (
        <Box
          m="40px 0 -10 0"
          height="75vh"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .nombre-column--cell": {
              color: shades.primary[100],
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: shades.primary[600],
              borderBottom: "none",
              color: shades.grey[900],
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: shades.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: shades.primary[600],
              color: shades.grey[900],
            },
            "& .MuiCheckbox-root": {
              color: `${shades.grey[100]} !important`,
            },
            "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
              color: `${shades.grey[100]} !important`,
            },
          }}
        >
          <DataGrid
            getRowId={(row) => row.id_jugador}
            rows={data}
            isRowSelectable={(params) => false}
            columns={columns}
            components={{ Toolbar: GridToolbar }}
          />
        </Box>
      )}
      <ModalEditJugador
        open={open}
        setOpen={setOpen}
        data={dataModal}
        setData={setDataModal}
        paises={paises}
        posiciones={posiciones}
        handleEdit={async (data) => {
          setOpen(false);
          Swal.fire("Actualizando...");
          console.log(data);
          updateJugador(data)
            .unwrap()
            .then((res) => {
              Swal.fire(
                "Actualizado",
                "El Jugador ha sido actualizado",
                "success"
              );
            })
            .catch((err) => {
              Swal.fire("Error", "No se pudo actualizar el Jugador", "error");
            });
        }}
      />
    </Box>
  );
};

export default ListJugador;
