import Modal from "@mui/material/Modal";
import * as React from "react";
import Typography from "@mui/material/Typography";
import { TextField, Box, Button, FormControl, InputLabel, MenuItem, Select } from "@mui/material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function ModalEditJugador({
  paises = [],
  posiciones = [],
  data = {},
  open,
  setData,
  setOpen,
  handleEdit,
}) {
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={style}
          gap="30px"
          display="grid"
          gridTemplateColumns="repeat(4, minmax(0, 1fr))"
        >
          <Typography
            sx={{ gridColumn: "span 4" }}
            id="modal-modal-title"
            variant="h2"
            component="h2"
          >
            Editar Jugador
          </Typography>
          <TextField
            sx={{ gridColumn: "span 2" }}
            fullWidth
            id="outlined-basic"
            label="nombre"
            variant="outlined"
            onChange={(e) => {
              setData({ ...data, nombre: e.target.value });
            }}
            value={data.nombre}
          />
          <FormControl sx={{ gridColumn: "span 2" }} fullWidth>
            <InputLabel id="demo-simple-select-label">Pais</InputLabel>
          
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              disabled
              value={data.id_pais}
              label="Pais"
              onChange={(e) => {
                setData({ ...data, id_pais: e.target.value });
              }}
            >
              {paises.map((pais) => (
                <MenuItem key={pais.id_pais} value={pais.id_pais}>
                  {pais.nombre}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <TextField
            fullWidth
            id="outlined-basic"
            type={"date"}
            label="fecha"
            disabled
            variant="outlined"
            value={data.fecha_nac}
            sx={{ gridColumn: "span 2" }}
            onChange={(e) => {
              setData({ ...data, fecha_nac: e.target.value });
            }}
          />
         <TextField
            fullWidth
            id="outlined-basic"
            type="text"
            label="Posicion"
            disabled
            variant="outlined"
            value={data.posicion}
            sx={{ gridColumn: "span 2" }}
          />
              

          <Box
            sx={{ gridColumn: "span 4" }}
            display="flex"
            justifyContent="flex-end"
          >
            <Button
              variant="contained"
              color="primary"
              sx={{ marginRight: "15px" }}
              onClick={() => handleEdit(data)}
            >
              Actualizar
            </Button>
            <Button variant="contained" color="secondary" onClick={handleClose}>
              Cancelar
            </Button>
          </Box>
        </Box>
      </Modal>
    </div>
  );
}
