import { useState, useEffect } from 'react'
import { Routes, Route } from 'react-router-dom'
import SidebarEmpleado from '../../pages/dashboardEmpleado/SidebarEmpleado'
import Topbar from '../../pages/dashboardAdmin/Topbar'
import { useSelector } from 'react-redux'
import CreateNoticia from '../../containers/empleado/noticias/CreateNoticias'
import ListNoticias from '../../containers/empleado/noticias/ListNoticias'
import CreateJugador from '../../containers/shared/CreateJugador'
import ListJugador from '../../containers/shared/ListJugador'
import { useGetNoticiasQuery } from '../../app/api/func/Noticias'
import CreatePartido from '../../containers/shared/CreatePartido'
import ListPartidos from '../../containers/shared/ListPartidos'
import { useGetJugadoresQuery } from '../../app/api/datosDeportivos/apiJugadores'
import { useGetPartidosQuery } from '../../app/api/datosDeportivos/apiPartido'
import TransJugador from '../../containers/empleado/transferencias/TransJugador'
import TransTecnico from '../../containers/empleado/transferencias/TransTecnico'
import BitacoraTrans from '../../containers/empleado/transferencias/Bitacora'
import ShowPartidos from '../../containers/empleado/partidos/ShowPartidos'
import CreateEquipo from '../../containers/shared/CreateEquipo'
import ListEquipo from '../../containers/shared/ListEquipo'
import { useGetEquiposQuery } from '../../app/api/datosDeportivos/apiEquipo'
import { useGetEstadiosQuery } from '../../app/api/datosDeportivos/ApiEstadio'
import CreateArbitro from '../../containers/admin/DatosDeportivos/CreateArbitro'
import { useGetArbitrosQuery } from '../../app/api/datosDeportivos/apiArbitro'
import ListCompetencia from '../../containers/shared/ListCompetencia'
import { useGetCompetenciasQuery } from '../../app/api/datosDeportivos/apiCompetencia'
import ListArbitro from '../../containers/admin/DatosDeportivos/ListArbitro'
import CreateEstadio from '../../containers/shared/CreateEstadio'
import ListEstadio from '../../containers/shared/ListEstadio'
import CreateCompetencia from '../../containers/shared/CreateCompetencia'
import { useGetTecnicosQuery } from '../../app/api/datosDeportivos/apiTecnico'
import CreateTecnico from '../../containers/shared/CreateTecnico'
import ListTecnico from '../../containers/shared/ListTecnico'
import ToCompe from '../../containers/empleado/registers/ToCompe'
import InCompe from '../../containers/empleado/registers/InCompe'
import RecoveryModal from './RecoveryModa'
import ListPredict from '../../containers/shared/ListPredicts'

function AppEmpleado() {
  const [isSidebar, setIsSidebar] = useState(true)
  const usuario = useSelector((state) => state.sesion.usuario)
  const recover = useSelector((state) => state.sesion.recover)
  const { data: noticias = [], isFetching } = useGetNoticiasQuery()
  const { data: jugadores, isFetching: fetchJugadores } = useGetJugadoresQuery()
  const { data: partidos, isFetching: fetchPartidos } = useGetPartidosQuery()

  const { data: estadios, isFetching: fetchEstadios } = useGetEstadiosQuery()
  const { data: arbitros, isFetching: fecthArbitros } = useGetArbitrosQuery()
  const { data: equipos, isFetching: fetchEquipos } = useGetEquiposQuery()
  const { data: competencias, isFetching: fetchCompetencias } = useGetCompetenciasQuery()
  const { data: tecnicos, isFetching: fetchTecnicos } = useGetTecnicosQuery()

  const [open, setOpen] = useState(false)

  useEffect(() => {
    if (recover) {
      setOpen(recover === 2 ? true : false)
    }
  }, [recover])

  return (
    <div className="app">
      <SidebarEmpleado isSidebar={isSidebar} />
      <main className="content">
        <Topbar setIsSidebar={setIsSidebar} />
        <Routes>
          <Route
            path=""
            element={
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '60vh',
                }}
              >
                <h1>Hola Bienvenido</h1>
                <RecoveryModal open={open} setOpen={setOpen} usuario={usuario} />
              </div>
            }
          />
          <Route path="addJugador" element={<CreateJugador />} />
          <Route
            path="showJugadores"
            element={<ListJugador data={jugadores} isFetching={fetchJugadores} />}
          />
          <Route path="addNoticia" element={<CreateNoticia usuario={usuario} />} />
          <Route
            path="showNoticias"
            element={<ListNoticias data={noticias} isFetching={isFetching} />}
          />
          <Route path="addPartido" element={<CreatePartido />} />
          <Route path="addEquipo" element={<CreateEquipo />} />
          <Route path="addArbitro" element={<CreateArbitro />} />
          <Route path="addEstadio" element={<CreateEstadio />} />
          <Route path="addTecnico" element={<CreateTecnico />} />

          <Route path="addCompetencia" element={<CreateCompetencia />} />
          <Route
            path="showEstadios"
            element={<ListEstadio data={estadios} isFetching={fetchEstadios} />}
          />
          <Route
            path="showTecnicos"
            element={<ListTecnico data={tecnicos} isFetching={fetchTecnicos} />}
          />
          <Route
            path="showEquipos"
            element={<ListEquipo data={equipos} isFetching={fetchEquipos} />}
          />
          <Route
            path="showCompetencias"
            element={<ListCompetencia data={competencias} isFetching={fetchCompetencias} />}
          />
          <Route
            path="showArbitros"
            element={<ListArbitro data={arbitros} isFetching={fecthArbitros} />}
          />
          <Route
            path="showPartidos"
            element={<ListPartidos data={partidos} isFetching={fetchPartidos} />}
          />
          <Route path="showPredicciones" element={<ListPredict />} />
          <Route path="transferirJugador" element={<TransJugador />} />
          <Route
            path="bitacoraJugador"
            element={<BitacoraTrans deportistas={jugadores} rol={'jugador'} />}
          />
          <Route path="transferirEntrenador" element={<TransTecnico />} />
          <Route
            path="bitacoraEntrenador"
            element={<BitacoraTrans deportistas={tecnicos} rol={'tecnico'} />}
          />
          <Route
            path="incidenciasPartidos"
            element={<ShowPartidos data={partidos} isFetching={fetchPartidos} />}
          />
          <Route
            path="equipoCompe"
            element={<ToCompe equipos={equipos} competencias={competencias} />}
          />
          <Route path="equipoEnCompe" element={<InCompe competencias={competencias} />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </main>
    </div>
  )
}

function NotFound() {
  return (
    <>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '75vh',
        }}
      >
        <h1>404: Not Found!</h1>
      </div>
    </>
  )
}

export default AppEmpleado
