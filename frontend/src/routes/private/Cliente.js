import { useEffect, useState } from "react";
import { Routes, Route } from "react-router-dom";
import Sidebar from "../../pages/dashboardCliente/Sidebar";
import Topbar from "../../pages/dashboardAdmin/Topbar";
import EditInfo from "../../containers/client/profile/EditInfo";
import { useSelector } from "react-redux";
import Membresia from "../../containers/client/membresia/Membresia";
import Historial from "../../containers/client/membresia/Historial";
import { useGethistorialMembresiaQuery } from "../../app/api/users/apiCliente";
import Visualizar from "../../containers/client/partidos/Visualizar";
import { useGetVisualizarPartidosQuery } from "../../app/api/users/apiCliente";
import { ProtectedRoute } from "../Protected";
import {
  useGetEstadisticas2Query,
  useGetEstadisticasQuery,
  useGetEstadisticasEquipoQuery,
  useGetEstadisticasEstadioQuery,
  useGetEstadisticasPartidoQuery,
  useGetEstadisticasTecnicoQuery,
} from "../../app/api/stats/apiEstadisticas";
import { useGetSubscripcionesQuery } from "../../app/api/stats/apiSubs";
import AddSubs from "../../containers/client/premium/AddSubs";
import Estadisticas from "../../containers/client/premium/Estadisticas";
import StatsPremium from "../../containers/client/premium/StatsPremium";
import { useGetJugadoresQuery } from "../../app/api/datosDeportivos/apiJugadores";
import { useGetTecnicosQuery } from "../../app/api/datosDeportivos/apiTecnico";
import { useGetCompetenciasQuery } from "../../app/api/datosDeportivos/apiCompetencia";
import { useGetEquiposQuery } from "../../app/api/datosDeportivos/apiEquipo";
import Deportistas from "../../containers/client/premium/Deportistas";
import Compe2 from "../../containers/client/premium/Compe2";
import RecoveryModal from "./RecoveryModa";
import ListPredict from "../../containers/shared/ListPredicts";
import Quiniela from "../../containers/client/premium/Quiniela";

function AppCliente() {
  const [isSidebar, setIsSidebar] = useState(true);
  const usuario = useSelector((state) => state.sesion.usuario);
  const recover = useSelector((state) => state.sesion.recover);
  const url = useSelector((state) => state.sesion.url);

  const { data: historialMembresia, isFetching } =
    useGethistorialMembresiaQuery({
      id: usuario.id_usuario,
      url: url,
    });

  const { data: visualizarPartidos, isFetching: fetch } =
    useGetVisualizarPartidosQuery(url);

  const { data: estadisticasJugador, isFetching: fetch1 } =
    useGetEstadisticasQuery(url);

  const { data: estadisticasJugador2, isFetching: fetch2 } =
    useGetEstadisticas2Query();
  const { data: estadisticasTecnico, isFetching: fetch4 } =
    useGetEstadisticasTecnicoQuery();
  const { data: estadisticasEstadio, isFetching: fetch5 } =
    useGetEstadisticasEstadioQuery();
  const { data: estadisticasEquipo, isFetching: fetch6 } =
    useGetEstadisticasEquipoQuery();
  const { data: estadisticasPartido, isFetching: fetch9 } =
    useGetEstadisticasPartidoQuery();

  const { data: subscripciones, isFetching: fetchSubs } =
    useGetSubscripcionesQuery(usuario.id_usuario);

  const { data: jugadores, isFetching: fetchJugadores } =
    useGetJugadoresQuery();
  const { data: tecnicos, isFetching: fetchTecnicos } = useGetTecnicosQuery();
  const { data: competencias } = useGetCompetenciasQuery();
  const { data: equipos } = useGetEquiposQuery();

  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (recover) {
      setOpen(recover === 2 ? true : false);
    }
  }, [recover]);

  return (
    <div className="app">
      <Sidebar isSidebar={isSidebar} membresia={usuario.membresia} />
      <main className="content">
        <Topbar setIsSidebar={setIsSidebar} />
        <Routes>
          <Route
            path=""
            element={
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  height: "60vh",
                }}
              >
                <h1>Hola Bienvenido</h1>
                <RecoveryModal
                  open={open}
                  setOpen={setOpen}
                  usuario={usuario}
                />
              </div>
            }
          />
          <Route
            path="editCliente"
            element={<EditInfo initialValues={usuario} />}
          />
          <Route path="membresia" element={<Membresia usuario={usuario} />} />
          <Route
            path="historialMembresias"
            element={
              <Historial
                data={historialMembresia?.data}
                isFetching={isFetching}
              />
            }
          />
          <Route
            path="partidos"
            element={
              <Visualizar
                data={visualizarPartidos?.data}
                isFetching={fetch}
                membresia={usuario.membresia === 2}
                id_usuario={usuario.id_usuario}
              />
            }
          />
          <Route path="predicciones" element={<ListPredict />} />
          <Route
            element={
              <ProtectedRoute
                isAllowed={usuario.membresia === 2}
                redirectTo="cliente/notAllowed"
              />
            }
          >
            <Route
              path="suscribirse"
              element={<AddSubs id_usuario={usuario.id_usuario} />}
            />
            <Route
              path="noticias"
              element={
                <Estadisticas
                  data={subscripciones}
                  isFetching={fetchSubs}
                  title="Noticias"
                  subtitle={"Noticias de tus equipos"}
                />
              }
            />
            <Route
              path="quinielas"
              element={<Quiniela id_usuario={usuario.id_usuario} />}
            />
            <Route
              path="reporteJugadores1"
              element={
                <Estadisticas
                  data={estadisticasJugador?.data}
                  isFetching={fetch1}
                  title="Reporte de Jugadores"
                  subtitle={
                    "Jugadores de X equipos, Jugadores de mayores/menores años"
                  }
                />
              }
            />
            <Route
              path="reporteJugadores2"
              element={
                <StatsPremium
                  data={estadisticasJugador2}
                  isFetching={fetch2}
                  title="Reporte de Jugadores"
                  subtitle={
                    "Jugadores de X incidencias en Y competencias, Z años"
                  }
                />
              }
            />
            <Route
              path="reporteJugadores3"
              element={
                <Deportistas
                  deportista={jugadores}
                  isFetching={fetchJugadores}
                  title={"Reporte de Jugadores"}
                  subtitle={"Equipos de X jugadores"}
                  tipo={1}
                />
              }
            />

            <Route
              path="reporteTecnicoE"
              element={
                <Deportistas
                  title={"Reporte de Tecnico"}
                  deportista={tecnicos}
                  isFetching={fetchTecnicos}
                  subtitle={"Equipos de X tecnicos"}
                  tipo={2}
                />
              }
            />
            <Route
              path="reporteTecnico"
              element={
                <Estadisticas
                  isFetching={fetch4}
                  data={estadisticasTecnico}
                  title="Reporte de Técnicos"
                  subtitle={
                    "Técnicos de X equipos, Técnicos de mayores/menores años"
                  }
                />
              }
            />
            <Route
              path="reportePartidos"
              element={
                <StatsPremium
                  isFetching={fetch9}
                  data={estadisticasPartido}
                  title="Reporte de Partidos"
                  subtitle={
                    "Historico de partidos, Partidos donde hubo al menos X goles, partidos en X años, X equipos, Y equipos"
                  }
                />
              }
            />
            <Route
              path="reporteEstadios"
              element={
                <Estadisticas
                  isFetching={fetch5}
                  data={estadisticasEstadio}
                  title="Reporte de Estadios"
                  subtitle={
                    "Estadios de X pais, con capacidad menor o igual a X"
                  }
                />
              }
            />
            <Route
              path="reporteEquipos"
              element={
                <Estadisticas
                  isFetching={fetch6}
                  data={estadisticasEquipo}
                  title="Reporte de Equipos"
                  subtitle={"Equipos de X pais, con X años"}
                />
              }
            />
            <Route
              path="reporteCompetencias1"
              element={
                <Compe2
                  lista={competencias}
                  title={"Reporte de Competencias"}
                  subtitle={"Equipos de X competencias"}
                  tipo={1}
                />
              }
            />
            <Route
              path="reporteCompetencias2"
              element={
                <Compe2
                  lista={equipos}
                  title={"Reporte de Competencias"}
                  subtitle={
                    "Cantidad de X competiciones que ha ganado Y equipo"
                  }
                  tipo={2}
                />
              }
            />
          </Route>
          <Route path="cliente/notAllowed" element={<NotAllowed />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </main>
    </div>
  );
}

function NotFound() {
  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "75vh",
        }}
      >
        <h1>404: Not Found!</h1>
      </div>
    </>
  );
}

function NotAllowed() {
  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "75vh",
        }}
      >
        <h1>403: Not Allowed!</h1>
      </div>
    </>
  );
}

export default AppCliente;
