import { createSlice } from "@reduxjs/toolkit";

export const sesionSlice = createSlice({
  name: "sesion",
  initialState: {
    sesion: true,
    usuario: null,
    rol: 1,
    token: null,
    recover: null,
    url: "http://service-esb-g8.tk",
    num_grupo: 8,
  },
  reducers: {
    iniciarSesion: (state, action) => {
      state.sesion = true;
      state.usuario = action.payload.usuario;
      state.rol = action.payload.rol;
      state.token = action.payload.token;
      state.recover = action.payload.recover;
    },
    cerrarSesion: (state) => {
      state.sesion = false;
      state.usuario = null;
      state.rol = 0;
      state.token = null;
      state.recover = null;
    },
    editUsuario: (state, action) => {
      state.usuario = action.payload;
    },
    editMembresia: (state, action) => {
      state.usuario.membresia = action.payload;
    },
    editRecover: (state, action) => {
      state.recover = action.payload;
    },
    editUrl: (state, action) => {
      state.url = action.payload.url;
      state.num_grupo = action.payload.num_grupo;
    },
  },
});

export const {
  editUsuario,
  iniciarSesion,
  cerrarSesion,
  editMembresia,
  editRecover,
  editUrl,
} = sesionSlice.actions;
export default sesionSlice.reducer;
