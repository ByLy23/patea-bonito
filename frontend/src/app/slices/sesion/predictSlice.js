import { createSlice } from "@reduxjs/toolkit";

export const predictSlice = createSlice({
    name: "predict",
    initialState: {
        predict: 0,
        isFetching: false,
        error: null,
    },
    reducers: {
        fetchPredictSuccess: (state, action) => {
            state.predict = action.payload;
        }
    },
});

export const { fetchPredictSuccess } = predictSlice.actions;

export default predictSlice.reducer;

