import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiUsuario = createApi({
  reducerPath: "apiUsuario",
  baseQuery: fetchBaseQuery({
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getClientes: builder.query({
      query: () => `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/usuario/3`,
      providesTags: ["Usuarios"],
    }),
    getAdmin : builder.query({
      query: () => `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/usuario/1`,
      providesTags: ["Usuarios"],
    }),
    getEmpleados: builder.query({
      query: () => `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/usuario/2`,
      providesTags: ["Usuarios"],
    }),
    createUsuario: builder.mutation({
      query: ({newUsuario, url}) => ({
        url: `${url}/api/admin/crear/`,
        method: "POST",
        body: newUsuario,
      }),
      invalidatesTags: ["Usuarios"],
    }),
    updateUsuario: builder.mutation({
      query: (updatedUsuario) => ({
        url: `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/usuario/`, 
        method: "PUT",
        body: updatedUsuario,
      }),
      invalidatesTags: ["Usuarios"],
    }),
    updateUsuarioEstado: builder.mutation({
      query: (updatedUsuario) => ({
        url: `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/usuario/estado`,
        method: "PUT",
        body: updatedUsuario,
      }),
      invalidatesTags: ["Usuarios"],
    }),
    // no se usa
    deleteUsuario: builder.mutation({
      query: () => ({
        url: `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/usuario/`,
        method: "DELETE",
      }),
      invalidatesTags: ["Usuarios"],
    }),
  }),
});

export const {
  useGetClientesQuery,
  useGetAdminQuery,
  useGetEmpleadosQuery,
  useCreateUsuarioMutation,
  useUpdateUsuarioMutation,
  useDeleteUsuarioMutation,
  useUpdateUsuarioEstadoMutation,
} = apiUsuario;
