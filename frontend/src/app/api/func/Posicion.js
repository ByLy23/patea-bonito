import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiPosicion = createApi({
    reducerPath: "apiPosicion",
    baseQuery: fetchBaseQuery({
        baseUrl: `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/posiciones`,
        prepareHeaders: (headers, { getState }) => {
            const token = getState().sesion.token;
            if (token) {
                headers.set("authorization", `Bearer ${token}`);
            }
            return headers;
        }
    }),
    endpoints: (builder) => ({
        getPosiciones: builder.query({
            query: () => `/`,
            providesTags: ["Posiciones"]
        })
    })
});

export const { useGetPosicionesQuery } = apiPosicion;

    
