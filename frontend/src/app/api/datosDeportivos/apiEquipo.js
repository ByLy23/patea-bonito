import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const apiEquipo = createApi({
  reducerPath: 'apiEquipo',
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_EMPLEADO_URL}/empleado/equipo`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token
      if (token) {
        headers.set('authorization', `Bearer ${token}`)
      }
      return headers
    },
  }),
  endpoints: (builder) => ({
    getEquipos: builder.query({
      query: () => `/`,
      providesTags: ['Equipos'],
    }),
    updateEquipo: builder.mutation({
      query: (updatedEquipo) => ({
        url: '/',
        method: 'PUT',
        body: updatedEquipo,
      }),

      invalidatesTags: ['Equipos'],
    }),
    createEquipo: builder.mutation({
      query: (newEquipo) => ({
        url: '/',
        method: 'POST',
        body: newEquipo,
      }),
      invalidatesTags: ['Equipos'],
    }),
    deleteEquipo: builder.mutation({
      query: (equipo) => ({
        url: `/${equipo}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['Equipos'],
    }),
  }),
})

export const {
  useGetEquiposQuery,
  useUpdateEquipoMutation,
  useCreateEquipoMutation,
  useDeleteEquipoMutation,
} = apiEquipo
