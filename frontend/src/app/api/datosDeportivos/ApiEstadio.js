import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const apiEstadio = createApi({
  reducerPath: 'apiEstadio',
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_ADMIN_URL}/admin/estadio`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token
      if (token) {
        headers.set('authorization', `Bearer ${token}`)
      }
      return headers
    },
  }),
  endpoints: (builder) => ({
    getEstadios: builder.query({
      query: () => `/`,
      providesTags: ['Estadios'],
    }),
    updateEstadio: builder.mutation({
      query: (updatedEstadio) => ({
        url: '/',
        method: 'PUT',
        body: updatedEstadio,
      }),
      invalidatesTags: ['Estadios'],
    }),
    createEstadio: builder.mutation({
      query: (newEstadio) => ({
        url: '/',
        method: 'POST',
        body: newEstadio,
      }),
      invalidatesTags: ['Estadios'],
    }),
    deleteEstadio: builder.mutation({
      query: (estadio) => ({
        url: `/${estadio}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['Estadios'],
    }),
  }),
})

export const {
  useGetEstadiosQuery,
  useUpdateEstadioMutation,
  useCreateEstadioMutation,
  useDeleteEstadioMutation,
} = apiEstadio

//------
