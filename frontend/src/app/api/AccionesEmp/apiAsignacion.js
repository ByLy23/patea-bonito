import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiAsignacion = createApi({
    reducerPath: "apiAsignacion",
    baseQuery: fetchBaseQuery({
        baseUrl: `${process.env.REACT_APP_SERVER_EMPLEADO_URL}/empleado/competencia`,
        prepareHeaders: (headers, { getState }) => {
            const token = getState().sesion.token;
            if (token) {
                headers.set("authorization", `Bearer ${token}`);
            }
            return headers;
        }
    }),
    endpoints: (builder) => ({
        getAsignaciones: builder.query({
            query: ({opcion, id}) => `equipos/${opcion}/${id}`,
            providesTags: ["EquiposAsignados"]
        }),
        asignarEquipo: builder.mutation({
            query: (asignacion) => ({
                url: "/equipo",
                method: "POST",
                body: asignacion
            }),
            invalidatesTags: ["EquiposAsignados"]
        }),
        actualizarAsignacion: builder.mutation({
            query: (asignacion) => ({
                url: "/equipo",
                method: "PUT",
                body: asignacion
            }),
            invalidatesTags: ["EquiposAsignados"]
        })
    })
});

export const {
    useGetAsignacionesQuery,
    useAsignarEquipoMutation,
    useActualizarAsignacionMutation
} = apiAsignacion;

