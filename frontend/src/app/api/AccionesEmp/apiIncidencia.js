import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiIncidencia = createApi({
  reducerPath: "apiIncidencia",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_EMPLEADO_URL}/empleado/incidencia`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    estadoIncidencia: builder.mutation({
      query: (incidencia) => ({
        url: "/estado/",
        method: "PUT",
        body: incidencia,
      }),
      invalidatesTags: ["Partidos"],
    }),
    golIncidencia: builder.mutation({
      query: (incidencia) => ({
        url: "/gol",
        method: "POST",
        body: incidencia,
      }),
      invalidatesTags: ["Partidos"],
    }),
    tarjetaIncidencia: builder.mutation({
      query: (incidencia) => ({
        url: "/tarjeta",
        method: "POST",
        body: incidencia,
      }),
      invalidatesTags: ["Partidos"],
    }),
  }),
});

export const {
  useEstadoIncidenciaMutation,
  useGolIncidenciaMutation,
  useTarjetaIncidenciaMutation,
} = apiIncidencia;
