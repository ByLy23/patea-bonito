import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const bitacoraAdminApi = createApi({
  reducerPath: "bitacoraAdminApi",
  baseQuery: fetchBaseQuery({
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getBitacora: builder.query({
      query: ({ id, url }) => `${url}/admin/bitacora/${id}`,
      providesTags: ["Bitacora"],
    }),
  }),
});

export const { useGetBitacoraQuery } = bitacoraAdminApi;

// Path: frontend\src\app\api\report\reportesAdmin.js
