import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiSubs = createApi({
  reducerPath: "apiSubs",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SERVER_CLIENTE_URL}/cliente/suscripcion`,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().sesion.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getSubscripciones: builder.query({
      query: (id) => `noticias/${id}`,
      providesTags: ["Subs"],
    }),
    getEquipo: builder.query({
      query: (id) => `equipo/${id}`,
      providesTags: ["EquipoSubs"],
    }),
    addSubscripcion: builder.mutation({
      query: (subscripcion) => ({
        url: "/",
        method: "POST",
        body: subscripcion,
      }),
      invalidatesTags: ["EquipoSubs", "Subs"],
    }),
   
  }),
});

export const {
  useGetSubscripcionesQuery,
  useAddSubscripcionMutation,
  useGetEquipoQuery,
} = apiSubs;
