#PROC001 CREAR USUARIO
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crear_usuario
DELIMITER //
CREATE PROCEDURE crear_usuario(
	IN correoIN VARCHAR(100),
    IN clave_accesoIN VARCHAR(256),
    IN nombreIN VARCHAR(100),
    IN apellidoIN VARCHAR(100),
    IN telefonoIN VARCHAR(10),
    IN fotografiaIN VARCHAR(200),
    IN fecha_nacIN VARCHAR(12),
    IN direccionIN VARCHAR(200),
    IN rolIN INT,
    IN generoIN INT,
	IN paisIN INT
)
this_proc:BEGIN
	DECLARE id_usuario INT;
    DECLARE msgError VARCHAR(100) DEFAULT CONCAT('El usuario con el correo: ',correoIN,' ya existe.');
    DECLARE fecha_nac DATETIME DEFAULT STR_TO_DATE(fecha_nacIN,'%d-%m-%Y');
    DECLARE fecha_reg DATETIME DEFAULT NOW();
    #---------------------------------------------------------------------------
	SELECT a.id_usuario INTO id_usuario FROM usuario a
    WHERE LOWER(a.correo) = LOWER(correoIN);
    #---------------------------------------------------------------------------
    IF id_usuario IS NOT NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msgError;
    END IF;
    #---------------------------------------------------------------------------
    #CREAR USUARIO DEPENDIENDO DEL ROL
    INSERT INTO usuario (clave_acceso,nombre,apellido,correo,telefono,fotografia,
    fecha_nac,fecha_reg,direccion,id_rol,id_genero,id_pais,id_e_u)
    VALUES
    (clave_accesoIN,nombreIN,apellidoIN,correoIN,telefonoIN,fotografiaIN,fecha_nac,
    fecha_reg,direccionIN,rolIN,generoIN,paisIN,3);
    #---------------------------------------------------------------------------
    #RECUPERAR USUARIO CREADO
	SELECT a.id_usuario INTO id_usuario FROM usuario a
    WHERE LOWER(a.correo) = LOWER(correoIN);    
    #---------------------------------------------------------------------------
    #CREAR MEMBRESIA DEPENDIENDO DEL ROL
    IF rolIN = 3 THEN
		CALL crear_membresia(id_usuario,1,0);
	ELSE 
		CALL crear_membresia(id_usuario,2,0);
	END IF;
    #---------------------------------------------------------------------------
    SELECT 'Usuario creado con éxito' as message;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC002 CREAR MEMBRESIA
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crear_membresia
DELIMITER //
CREATE PROCEDURE crear_membresia(
	IN id_usuarioIN INT,
	IN tipo_membresiaIN INT,
    IN duracionIN INT
)
this_proc:BEGIN
    DECLARE fecha_creacion DATETIME DEFAULT NOW();
    DECLARE fecha_final DATETIME DEFAULT DATE_ADD(fecha_creacion, INTERVAL duracionIN MONTH);
    DECLARE monto NUMERIC(6,2) DEFAULT duracionIN * 15;
    #---------------------------------------------------------------------------
    #CANCELAR MEMBRESIAS ACTUALES
    CALL cancelar_membresia(id_usuarioIN);
    #---------------------------------------------------------------------------
    #CREAR MEMEBRESIA SEGUN EL TIPO DE MEMBRESIA
    IF tipo_membresiaIN = 1 THEN
		#MEMBRESIA GRATUITA
        INSERT INTO h_m (fecha_inicio,fecha_fin,estado_logico,monto,id_membresia,id_usuario)
        VALUES
        (fecha_creacion,fecha_final,1,monto,1,id_usuarioIN);
    ELSE
        INSERT INTO h_m (fecha_inicio,fecha_fin,estado_logico,monto,id_membresia,id_usuario)
        VALUES
        (fecha_creacion,fecha_final,1,monto,2,id_usuarioIN);    
		#MEMBRESIA DE PAGO
    END IF;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC003 CANCELAR MEMBRESIA
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS cancelar_membresia
DELIMITER //
CREATE PROCEDURE cancelar_membresia(
	IN id_usuarioIN INT
)
this_proc:BEGIN
    DECLARE fecha_final DATETIME DEFAULT NOW();
    DECLARE id_membresia INT;
    #---------------------------------------------------------------------------
    SELECT id_h_m INTO id_membresia FROM h_m
    WHERE id_usuario = id_usuarioIN
    AND estado_logico = 1;
    #---------------------------------------------------------------------------
    UPDATE h_m
    SET estado_logico = 0
    WHERE id_usuario = id_usuarioIN;
    
    UPDATE h_m
    SET fecha_fin = fecha_final
    WHERE id_h_m = id_membresia;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC004 LOGIN
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS login
DELIMITER //
CREATE PROCEDURE login(
	IN correo VARCHAR(100)
)
this_proc:BEGIN
	DECLARE id_usuario INT;
    DECLARE estado_usuario INT;
    DECLARE msgError VARCHAR(100) DEFAULT CONCAT('El usuario con el correo: ',correo,' no existe.');
    DECLARE msgError2 VARCHAR(100) DEFAULT CONCAT('La cuenta del usuario con el correo: ',correo,' no está activada o esta congelada.');
    #---------------------------------------------------------------------------
	SELECT a.id_usuario, id_e_u INTO id_usuario, estado_usuario FROM usuario a
    WHERE LOWER(a.correo) = LOWER(correo);
    #---------------------------------------------------------------------------
    IF id_usuario IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msgError;
    END IF;
    
    IF estado_usuario = 3 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msgError2;
    END IF;
    #---------------------------------------------------------------------------
    SELECT a.clave_acceso, a.nombre, a.apellido, a.correo, a.telefono,
    a.fotografia, a.fecha_nac, a.fecha_reg, a.direccion, a.id_rol,
    b.nombre as rol, a.id_genero, c.nombre as genero, a.id_pais, d.nombre as pais,
    a.id_e_u as id_estado, e.nombre as estado, g.nombre as membresia FROM usuario a 
    /*Conseguir ROL*/
    INNER JOIN rol b
    ON b.id_rol = a.id_rol
    /*Conseguir GENERO*/
    INNER JOIN genero c
    ON c.id_genero = a.id_genero
    /*Conseguir PAIS*/
    INNER JOIN pais d
    ON d.id_pais = a.id_pais
    /*Conseguir ESTADO*/
    INNER JOIN e_u e
    ON e.id_e_u = a.id_e_u
    /*Conseguir ID MEMBRESIA*/
    INNER JOIN h_m f
    ON f.id_usuario = a.id_usuario
    /*Conseguir NOMBRE MEMBRESIA*/
    INNER JOIN membresia g
    ON g.id_membresia = f.id_membresia
    WHERE a.id_usuario = id_usuario
    AND f.estado_logico = 1
    ;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#PROC005 ACTUALIZAR ESTADO DEL USUARIO
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS actualizar_estado_usuario
DELIMITER //
CREATE PROCEDURE actualizar_estado_usuario(
	IN id_usuarioIN INT,
    IN id_estadoIN INT
)
this_proc:BEGIN
    #---------------------------------------------------------------------------
	UPDATE usuario
    SET id_e_u = id_estadoIN
    WHERE id_usuario = id_usuarioIN;
    #---------------------------------------------------------------------------
    SELECT 'Estado de usuario actualizado con éxito.' as message;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#PROC006 ACTUALIZAR INFORMACION DEL USUARIO
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS actualizar_informacion_usuario
DELIMITER //
CREATE PROCEDURE actualizar_informacion_usuario(
	IN id_usuarioIN INT,
    IN nombreIN VARCHAR(100),
    IN apellidoIN VARCHAR(100),
    IN telefonoIN VARCHAR(12),
    IN direccionIN VARCHAR(200)
)
this_proc:BEGIN
	DECLARE correo VARCHAR(50);
    #---------------------------------------------------------------------------
    SELECT a.correo INTO correo FROM usuario a
    WHERE id_usuario = id_usuarioIN;
    #---------------------------------------------------------------------------
	UPDATE usuario
    SET nombre = nombreIN,
    apellido = apellidoIN,
    telefono = telefonoIN,
    direccion = direccionIN
    WHERE id_usuario = id_usuarioIN;
    #---------------------------------------------------------------------------
    CALL login(correo);
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#PROC007 MODIFICAR INFORMACION DEL USUARIO
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtener_usuarios_clientes
DELIMITER //
CREATE PROCEDURE obtener_usuarios_clientes(
)
this_proc:BEGIN
    #---------------------------------------------------------------------------
	SELECT a.clave_acceso, a.nombre, a.apellido, a.correo, a.telefono,
    a.fotografia, a.fecha_nac, a.fecha_reg, a.direccion, a.id_rol,
    b.nombre as rol, a.id_genero, c.nombre as genero, a.id_pais, d.nombre as pais,
    a.id_e_u as id_estado, e.nombre as estado, g.nombre as membresia FROM usuario a 
    /*Conseguir ROL*/
    INNER JOIN rol b
    ON b.id_rol = a.id_rol
    /*Conseguir GENERO*/
    INNER JOIN genero c
    ON c.id_genero = a.id_genero
    /*Conseguir PAIS*/
    INNER JOIN pais d
    ON d.id_pais = a.id_pais
    /*Conseguir ESTADO*/
    INNER JOIN e_u e
    ON e.id_e_u = a.id_e_u
    /*Conseguir ID MEMBRESIA*/
    INNER JOIN h_m f
    ON f.id_usuario = a.id_usuario
    /*Conseguir NOMBRE MEMBRESIA*/
    INNER JOIN membresia g
    ON g.id_membresia = f.id_membresia
    WHERE f.estado_logico = 1
    AND a.id_rol = 3;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#PROC008 ADQUIRIR MEMBRESIA
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS adquirir_membresia
DELIMITER //
CREATE PROCEDURE adquirir_membresia(
	IN id_usuarioIN INT,
    IN duracionIN INT
)
this_proc:BEGIN
    #---------------------------------------------------------------------------
    #ELIMINAR LAS ANTERIORES Y CREAR UNA NUEVA
    CALL cancelar_membresia(id_usuarioIN);
	CALL crear_membresia(id_usuarioIN,2,duracionIN);
    #---------------------------------------------------------------------------
    SELECT 'Membresía creada con éxito.' as message;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#PROC009 OBTENER PAISES
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtener_paises
DELIMITER //
CREATE PROCEDURE obtener_paises(
)
this_proc:BEGIN
    #---------------------------------------------------------------------------
    SELECT * FROM pais;
    #---------------------------------------------------------------------------

END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#PROC010 CANCELAR MEMBRESIA
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS dar_baja_membresia
DELIMITER //
CREATE PROCEDURE dar_baja_membresia(
	IN id_usuarioIN INT
)
this_proc:BEGIN
    #---------------------------------------------------------------------------
    CALL cancelar_membresia(id_usuarioIN);
    CALL crear_membresia(id_usuarioIN,1,0);
    #---------------------------------------------------------------------------
	SELECT 'Membresía eliminada con éxito' as message;
	#---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#PROC011 VER HISTORIAL DE MEMBRESIAS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtener_historial_membresias
DELIMITER //
CREATE PROCEDURE obtener_historial_membresias(
	IN id_usuarioIN INT
)
this_proc:BEGIN
    #---------------------------------------------------------------------------
    SELECT fecha_inicio,fecha_fin, CASE
		WHEN estado_logico = 1 THEN 'Activa'
		ELSE'Finalizada'
	END as estado, b.nombre as tipo, a.monto
    FROM H_M a 
    /*Conseguir nombre MEMBRESIA*/
    INNER JOIN MEMBRESIA b
    ON b.id_membresia = a.id_membresia
    WHERE id_usuario = id_usuarioIN;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#PROC012 CREAR ESTADIOS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crear_estadio
DELIMITER //
CREATE PROCEDURE crear_estadio(
	IN nombreIN VARCHAR(100),
    IN capacidadIN INT,
    IN id_paisIN INT
)
this_proc:BEGIN
	DECLARE existe INT;
	DECLARE msgError VARCHAR(100) DEFAULT CONCAT('El estadio con el nombre: ',nombreIN,' ya existe.');
    #---------------------------------------------------------------------------
	SELECT id_estadio INTO existe FROM estadio 
    WHERE LOWER(nombre) = LOWER(nombreIN)
    AND estado_logico = 1;
    #---------------------------------------------------------------------------
	IF existe IS NOT NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msgError;
    END IF;
    #---------------------------------------------------------------------------
	INSERT INTO estadio (nombre,capacidad,id_pais,estado_logico)
	VALUES (nombreIN,capacidadIN,id_paisIN,1);
    #---------------------------------------------------------------------------
    SELECT 'Estadio creado con éxito.' as message;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC013 ACTUALIZAR ESTADIOS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS actualizar_estadio
DELIMITER //
CREATE PROCEDURE actualizar_estadio(
	IN id_estadioIN INT,
	IN nombreIN VARCHAR(100),
    IN capacidadIN INT
)
this_proc:BEGIN
	DECLARE existe INT;
	DECLARE msgError VARCHAR(100) DEFAULT CONCAT('El estadio con el nombre: ',nombreIN,' ya existe.');
    #---------------------------------------------------------------------------
	SELECT id_estadio INTO existe FROM estadio 
    WHERE LOWER(nombre) = LOWER(nombreIN)
    AND estado_logico = 1;
    #---------------------------------------------------------------------------
	IF existe IS NOT NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msgError;
    END IF;
    #---------------------------------------------------------------------------
	UPDATE estadio
    SET nombre = nombreIN,
    capacidad = capacidadIN
    WHERE id_estadio = id_estadioIN;
    #---------------------------------------------------------------------------
    SELECT 'Estadio actualizado con éxito.' as message;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC014 ELIMINAR ESTADIOS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS eliminar_estadio
DELIMITER //
CREATE PROCEDURE eliminar_estadio(
	IN id_estadioIN INT
)
this_proc:BEGIN
	UPDATE estadio
    SET estado_logico = 0
    WHERE id_estadio = id_estadioIN;
    #---------------------------------------------------------------------------
    SELECT 'Estadio eliminado con éxito.' as message;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC015 OBTENER ESTADIOS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtener_estadios
DELIMITER //
CREATE PROCEDURE obtener_estadios(
)
this_proc:BEGIN
	SELECT a.nombre, a.capacidad, b.nombre as pais FROM estadio a
    /*obtener PAIS*/
    INNER JOIN pais b
    ON a.id_pais = b.id_pais
    WHERE estado_logico = 1;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#PROC016 CREAR ARBITROS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crear_arbitro
DELIMITER //
CREATE PROCEDURE crear_arbitro(
	fecha_nacIN VARCHAR(15),
    nombreIN VARCHAR(100),
    id_paisIN INT
)
this_proc:BEGIN
	DECLARE existe INT;
    DECLARE msgError VARCHAR(100) DEFAULT CONCAT('El arbitro con el nombre: ',nombreIN,' ya existe.');
	DECLARE fecha_nac DATETIME DEFAULT STR_TO_DATE(fecha_nacIN,'%d-%m-%Y');
    #---------------------------------------------------------------------------
	SELECT a.id_arbitro INTO existe FROM arbitro a 
    WHERE LOWER(nombre) = LOWER(nombreIN)
    AND estado_logico = 1;
    #---------------------------------------------------------------------------
	IF existe IS NOT NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msgError;
    END IF;
    #---------------------------------------------------------------------------
	INSERT INTO arbitro (fecha_nac,nombre,id_pais,estado_logico)
    VALUES (fecha_nac,nombreIN,id_paisIN,1);
    #---------------------------------------------------------------------------
    SELECT 'Árbitro creado con éxito.' as message;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC017 OBTENER ARBITROS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtener_arbitros
DELIMITER //
CREATE PROCEDURE obtener_arbitros(
)
this_proc:BEGIN
	SELECT a.nombre, a.fecha_nac, b.nombre as pais FROM arbitro a
    /*obtener PAIS*/
    INNER JOIN pais b
    ON a.id_pais = b.id_pais
    WHERE estado_logico = 1;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC018 ACTUALIZAR ARBITROS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS actualizar_arbitro
DELIMITER //
CREATE PROCEDURE actualizar_arbitro(
	IN nombreIN VARCHAR(100),
	IN id_arbitroIN INT
)
this_proc:BEGIN
	DECLARE existe INT;
	DECLARE msgError VARCHAR(100) DEFAULT CONCAT('El árbitro con el nombre: ',nombreIN,' ya existe.');
    #---------------------------------------------------------------------------
	SELECT id_arbitro INTO existe FROM arbitro 
    WHERE LOWER(nombre) = LOWER(nombreIN)
    AND estado_logico = 1;
    #---------------------------------------------------------------------------
	IF existe IS NOT NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msgError;
    END IF;
    #---------------------------------------------------------------------------
	UPDATE arbitro
    SET nombre = nombreIN
    WHERE id_arbitro = id_arbitroIN;
    #---------------------------------------------------------------------------
    SELECT 'Árbitro actualizado con éxito.' as message;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC019 ELIMINAR ARBITROS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS eliminar_arbitro
DELIMITER //
CREATE PROCEDURE eliminar_arbitro(
	IN id_arbitroIN INT
)
this_proc:BEGIN
	UPDATE arbitro
    SET estado_logico = 0
    WHERE id_arbitro = id_arbitroIN;
    #---------------------------------------------------------------------------
    SELECT 'Árbitro eliminado con éxito.' as message;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#PROC020 CREAR EQUIPOS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crear_equipo
DELIMITER //
CREATE PROCEDURE crear_equipo(
	IN nombreIN VARCHAR(100),
    IN id_paisIN INT,
    IN fecha_fundacionIN VARCHAR(12),
    IN tipoIN INT
)
this_proc:BEGIN
	DECLARE existe INT;
	DECLARE msgError VARCHAR(100) DEFAULT CONCAT('El equipo con el nombre: ',nombreIN,' ya existe.');
    DECLARE fecha_fundacion DATETIME DEFAULT STR_TO_DATE(fecha_fundacionIN,'%d-%m-%Y');
    #---------------------------------------------------------------------------
	SELECT id_equipo INTO existe FROM equipo 
    WHERE LOWER(nombre) = LOWER(nombreIN)
    AND estado_logico = 1;
    #---------------------------------------------------------------------------
	IF existe IS NOT NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msgError;
    END IF;
    #---------------------------------------------------------------------------
	INSERT INTO equipo (nombre,id_pais,id_t_e,fecha_fundacion,estado_logico)
	VALUES (nombreIN,id_paisIN,tipoIN,fecha_fundacion,1);
    #---------------------------------------------------------------------------
    SELECT 'Equipo creado con éxito.' as message;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC021 ACTUALIZAR EQUIPOS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS actualizar_equipo
DELIMITER //
CREATE PROCEDURE actualizar_equipo(
	IN id_equipoIN INT,
	IN nombreIN VARCHAR(100)
)
this_proc:BEGIN
	DECLARE existe INT;
	DECLARE msgError VARCHAR(100) DEFAULT CONCAT('El equipo con el nombre: ',nombreIN,' ya existe.');
    #---------------------------------------------------------------------------
	SELECT id_equipo INTO existe FROM equipo 
    WHERE LOWER(nombre) = LOWER(nombreIN)
    AND estado_logico = 1;
    #---------------------------------------------------------------------------
	IF existe IS NOT NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msgError;
    END IF;
    #---------------------------------------------------------------------------
	UPDATE equipo
    SET nombre = nombreIN
    WHERE id_equipo = id_equipoIN;
    #---------------------------------------------------------------------------
    SELECT 'Equipo actualizado con éxito.' as message;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC022 ELIMINAR EQUIPOS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS eliminar_equipo
DELIMITER //
CREATE PROCEDURE eliminar_equipo(
	IN id_equipoIN INT
)
this_proc:BEGIN
	UPDATE equipo
    SET estado_logico = 0
    WHERE id_equipo = id_equipoIN;
    #---------------------------------------------------------------------------
    SELECT 'Equipo eliminado con éxito.' as message;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC023 OBTENER EQUIPOS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtener_equipos
DELIMITER //
CREATE PROCEDURE obtener_equipos(
)
this_proc:BEGIN
	SELECT a.nombre, a.fecha_fundacion, b.nombre as pais, c.nombre as tipo FROM equipo a
    /*obtener PAIS*/
    INNER JOIN pais b
    ON a.id_pais = b.id_pais
    /*obtener TIPO*/
    INNER JOIN t_e c
    ON c.id_t_e = a.id_t_e
    WHERE estado_logico = 1;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////





#PROC024 CREAR EQUIPOS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crear_competencia
DELIMITER //
CREATE PROCEDURE crear_competencia(
	IN nombreIN VARCHAR(100),
	IN fechaIN VARCHAR(12),
    IN id_paisIN INT,
    IN estadoIN INT
)
this_proc:BEGIN
	DECLARE existe INT;
	DECLARE msgError VARCHAR(100) DEFAULT CONCAT('La competencia con el nombre: ',nombreIN,' ya existe.');
    DECLARE fecha DATETIME DEFAULT STR_TO_DATE(fechaIN,'%d-%m-%Y');
    #---------------------------------------------------------------------------
	SELECT id_competencia INTO existe FROM competencia 
    WHERE LOWER(nombre) = LOWER(nombreIN)
    AND estado_logico = 1;
    #---------------------------------------------------------------------------
	IF existe IS NOT NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msgError;
    END IF;
    #---------------------------------------------------------------------------
	INSERT INTO competencia (nombre,fecha,id_pais,id_e_c,estado_logico)
    VALUES (nombreIN,fecha,id_paisIN,estadoIN,1);
    #---------------------------------------------------------------------------
    SELECT 'Competencia creada con éxito.' as message;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC025 ACTUALIZAR EQUIPOS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS actualizar_competencia
DELIMITER //
CREATE PROCEDURE actualizar_competencia(
	IN id_competenciaIN INT,
	IN nombreIN VARCHAR(100),
    IN estadoIN INT
)
this_proc:BEGIN
	DECLARE existe INT;
	DECLARE msgError VARCHAR(100) DEFAULT CONCAT('La competencia con el nombre: ',nombreIN,' ya existe.');
    #---------------------------------------------------------------------------
	SELECT id_competencia INTO existe FROM competencia 
    WHERE LOWER(nombre) = LOWER(nombreIN)
    AND estado_logico = 1;
    #---------------------------------------------------------------------------
	IF existe IS NOT NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msgError;
    END IF;
    #---------------------------------------------------------------------------
	UPDATE competencia
    SET nombre = nombreIN,
    id_e_c = estadoIN
    WHERE id_competencia = id_competenciaIN;
    #---------------------------------------------------------------------------
    SELECT 'competencia actualizada con éxito.' as message;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC026 ELIMINAR EQUIPOS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS eliminar_competencia
DELIMITER //
CREATE PROCEDURE eliminar_competencia(
	IN id_competenciaIN INT
)
this_proc:BEGIN
	UPDATE competencia
    SET estado_logico = 0
    WHERE id_competencia = id_competenciaIN;
    #---------------------------------------------------------------------------
    SELECT 'Competencia eliminada con éxito.' as message;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////

#PROC027 OBTENER COMPETENCIAS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtener_competencias
DELIMITER //
CREATE PROCEDURE obtener_competencias(
)
this_proc:BEGIN
	SELECT a.nombre, a.fecha, b.nombre as pais, c.nombre as estado FROM competencia a
    /*obtener PAIS*/
    INNER JOIN pais b
    ON a.id_pais = b.id_pais
    /*obtener ESTADO*/
    INNER JOIN e_c c
    ON c.id_e_c = a.id_e_c
    WHERE estado_logico = 1;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#PROC028 CREAR NOTICIA
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS crear_noticia
DELIMITER //
CREATE PROCEDURE crear_noticia(
	IN descripcionIN VARCHAR(200),
    IN id_equipoIN INT,
    IN id_usuarioIN INT
)
this_proc:BEGIN
	DECLARE equipo VARCHAR(50);
    #---------------------------------------------------------------------------
    SELECT nombre INTO equipo FROM equipo 
    WHERE id_equipo = id_equipoIN;
    #---------------------------------------------------------------------------
	INSERT INTO noticia (descripcion,id_equipo,id_usuario)
    VALUES (descripcionIN,id_equipoIN,id_usuarioIN);
    #---------------------------------------------------------------------------
    SELECT CONCAT('Noticia sobre el equipo: ',equipo,' creada con éxito.') as message;
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////



#PROC029 OBTENER NOTICIAS
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS obtener_noticias
DELIMITER //
CREATE PROCEDURE obtener_noticias(
)
this_proc:BEGIN
	SELECT a.id_equipo, b.nombre as equipo, a.id_usuario, 
    CONCAT(c.nombre,' ',c.apellido) as usuario,
    a.descripcion FROM noticia a
    /*obtener nombre de EQUIPO*/
    INNER JOIN equipo b
    ON a.id_equipo = b.id_equipo
    /*obtener nombre de USUARIO*/
    INNER JOIN usuario c
    ON c.id_usuario = a.id_usuario;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////


#PROC030 OBTENER MEMBRESIA
#//////////////////////////////////////////////////////////////////////////////////////
DROP PROCEDURE IF EXISTS ver_membresia
DELIMITER //
CREATE PROCEDURE ver_membresia(
	IN id_usuarioIN INT
)
this_proc:BEGIN
	SELECT a.fecha_inicio,a.fecha_fin,a.id_membresia, b.nombre as membresia 
    ,a.monto FROM h_m a
    /*obtener nombre de tipo de MEMBRESIA*/
	INNER JOIN membresia b
    ON b.id_membresia = a.id_membresia
    WHERE id_usuario = id_usuarioIN
    AND estado_logico = 1;
    #---------------------------------------------------------------------------
END //;
DELIMITER ;
#//////////////////////////////////////////////////////////////////////////////////////