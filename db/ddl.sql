DROP DATABASE sa_proyecto;
CREATE DATABASE sa_proyecto;
USE sa_proyecto;


CREATE TABLE arbitro (
    id_arbitro    INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    fecha_nac     DATETIME NOT NULL,
    nombre        VARCHAR(100) NOT NULL,
    id_pais  INTEGER NOT NULL,
    estado_logico INTEGER NOT NULL
);


CREATE TABLE c_e_c (
    id_c_e_c            INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    id_equipo           INTEGER NOT NULL,
    id_competencia 		INTEGER NOT NULL,
    id_e_e_c            INTEGER NOT NULL
);


CREATE TABLE c_e_j (
    id_c_e_j           INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    fecha_inicio       DATETIME NOT NULL,
    fecha_fin          DATETIME NOT NULL,
    id_jugador 		   INTEGER NOT NULL,
    id_equipo   	   INTEGER NOT NULL
);



CREATE TABLE c_e_t (
    id_c_e_t           INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    fecha_inicio       DATETIME NOT NULL,
    fecha_fin          DATETIME NOT NULL,
    id_tecnico 		   INTEGER NOT NULL,
    id_equipo   	   INTEGER NOT NULL
);



CREATE TABLE color (
    id_color INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre   VARCHAR(50) NOT NULL
);


CREATE TABLE competencia (
    id_competencia INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre         VARCHAR(100) NOT NULL,
    fecha          DATETIME NOT NULL,
    id_pais   	   INTEGER NOT NULL,
    id_e_c     	   INTEGER NOT NULL,
    estado_logico  INTEGER NOT NULL
);



CREATE TABLE e_c (
    id_e_c INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL
);



CREATE TABLE e_e_c (
    id_e_e_c INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre   VARCHAR(50) NOT NULL
);


CREATE TABLE e_j (
    id_e_j INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL
);


CREATE TABLE e_p (
    id_e_p INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL
);



CREATE TABLE e_s (
    id_e_s INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL
);



CREATE TABLE e_u (
    id_e_u INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL
);


CREATE TABLE equipo (
    id_equipo       INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre          VARCHAR(100) NOT NULL,
    id_pais    	    INTEGER NOT NULL,
    id_t_e      	INTEGER NOT NULL,
    fecha_fundacion DATETIME NOT NULL,
    estado_logico   INTEGER NOT NULL
);


CREATE TABLE estadio (
    id_estadio   INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre       VARCHAR(50) NOT NULL,
    capacidad    INTEGER NOT NULL,
    id_pais 	 INTEGER NOT NULL,
    estado_logico INTEGER NOT NULL
);



CREATE TABLE genero (
    id_genero INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre    VARCHAR(50) NOT NULL
);


CREATE TABLE gol (
    id_incidencia  INTEGER NOT NULL PRIMARY KEY,
    distancia      INTEGER NOT NULL
);


CREATE TABLE h_m (
    id_h_m                 INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    fecha_inicio           DATETIME NOT NULL,
    fecha_fin              DATETIME NOT NULL,
    estado_logico          INTEGER NOT NULL,
    id_usuario    		   INTEGER NOT NULL,
	id_membresia 		   INTEGER NOT NULL,
    monto                  NUMERIC(6, 2) NOT NULL
);


CREATE TABLE incidencia (
    id_incidencia      INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    minuto             INTEGER NOT NULL,
    id_jugador 		   INTEGER NOT NULL,
    id_t_i         	   INTEGER NOT NULL,
    id_partido 		   INTEGER NOT NULL
);



CREATE TABLE jugador (
    id_jugador           INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre               VARCHAR(100) NOT NULL,
    fecha_nac            DATETIME NOT NULL,
    id_pais         	 INTEGER NOT NULL,
    id_posicion 		 INTEGER NOT NULL,
    id_e_j           	 INTEGER NOT NULL,
    estado_logico        INTEGER NOT NULL
);



CREATE TABLE log (
    id_log             INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    descripcion        JSON NOT NULL,
    id_usuario 	       INTEGER NOT NULL
);

CREATE TABLE membresia (
    id_membresia INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre       VARCHAR(50) NOT NULL
);



CREATE TABLE noticia (
    id_noticia         INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    descripcion        VARCHAR(200) NOT NULL,
    id_equipo   	   INTEGER NOT NULL,
    id_usuario 		   INTEGER NOT NULL
);


CREATE TABLE pais (
    id_pais INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre  VARCHAR(50) NOT NULL
);


CREATE TABLE partido (
    id_partido                 INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    publico                    INTEGER NOT NULL,
    hora                       DATETIME NOT NULL,
    fecha                      DATETIME NOT NULL,
    id_estadio         		   INTEGER NOT NULL,
    eq_local           	   	   INTEGER NOT NULL,
    eq_visit          	   	   INTEGER NOT NULL,
    id_arbitro         		   INTEGER NOT NULL,
    id_competencia 			   INTEGER NOT NULL,
    id_e_p                 	   INTEGER NOT NULL,
    gol_loc                    INTEGER NOT NULL,
    gol_vis                    INTEGER NOT NULL,
    estado_logico              INTEGER NOT NULL
);


CREATE TABLE penal (
    id_incidencia       INTEGER NOT NULL PRIMARY KEY,
    jug_sufre  			INTEGER NOT NULL,
    jug_comete 			INTEGER NOT NULL
);



CREATE TABLE posicion (
    id_posicion INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre      VARCHAR(50) NOT NULL
);


CREATE TABLE quiniela (
    id_quiniela        INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    gol_loc            INTEGER NOT NULL,
    gol_vis            INTEGER NOT NULL,
    id_usuario 		   INTEGER NOT NULL,
    id_partido 	 	   INTEGER NOT NULL
);



CREATE TABLE rol (
    id_rol INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL
);


CREATE TABLE suscripcion (
    id_suscripcion     INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    id_equipo   	   INTEGER NOT NULL,
    id_e_s        	   INTEGER NOT NULL,
    id_usuario 		   INTEGER NOT NULL
);


CREATE TABLE t_e (
    id_t_e INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL
);


CREATE TABLE t_i (
    id_t_i INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL
);



CREATE TABLE tarjeta (
    id_incidencia  INTEGER NOT NULL PRIMARY KEY,
    id_color 	   INTEGER NOT NULL
);


CREATE TABLE tecnico (
    id_tecnico    INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre        VARCHAR(100) NOT NULL,
    fecha_nac     DATETIME NOT NULL,
    estado_logico INTEGER NOT NULL
);


CREATE TABLE usuario (
    id_usuario             INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    clave_acceso           VARCHAR(32) NOT NULL,
    nombre                 VARCHAR(100) NOT NULL,
    apellido               VARCHAR(100) NOT NULL,
    correo                 VARCHAR(100) NOT NULL,
    telefono               VARCHAR(8) NOT NULL,
    fotografia             VARCHAR(200) NOT NULL,
    fecha_nac              DATETIME NOT NULL,
    fecha_reg              DATETIME NOT NULL,
    direccion              VARCHAR(200) NOT NULL,
    id_rol             	   INTEGER NOT NULL,
    id_genero       	   INTEGER NOT NULL,
    id_pais           	   INTEGER NOT NULL,
    id_e_u             	   INTEGER NOT NULL
);



ALTER TABLE arbitro
    ADD CONSTRAINT arbitro_pais_fk FOREIGN KEY ( id_pais )
        REFERENCES pais ( id_pais );

ALTER TABLE c_e_c
    ADD CONSTRAINT c_e_c_competencia_fk FOREIGN KEY ( id_competencia )
        REFERENCES competencia ( id_competencia )
            ON DELETE CASCADE;

ALTER TABLE c_e_c
    ADD CONSTRAINT c_e_c_e_e_c_fk FOREIGN KEY ( id_e_e_c )
        REFERENCES e_e_c ( id_e_e_c )
            ON DELETE CASCADE;

ALTER TABLE c_e_c
    ADD CONSTRAINT c_e_c_equipo_fk FOREIGN KEY ( id_equipo )
        REFERENCES equipo ( id_equipo )
            ON DELETE CASCADE;

ALTER TABLE c_e_j
    ADD CONSTRAINT c_e_j_equipo_fk FOREIGN KEY ( id_equipo )
        REFERENCES equipo ( id_equipo )
            ON DELETE CASCADE;

ALTER TABLE c_e_j
    ADD CONSTRAINT c_e_j_jugador_fk FOREIGN KEY ( id_jugador )
        REFERENCES jugador ( id_jugador )
            ON DELETE CASCADE;

ALTER TABLE c_e_t
    ADD CONSTRAINT c_e_t_equipo_fk FOREIGN KEY ( id_equipo )
        REFERENCES equipo ( id_equipo )
            ON DELETE CASCADE;

ALTER TABLE c_e_t
    ADD CONSTRAINT c_e_t_tecnico_fk FOREIGN KEY ( id_tecnico )
        REFERENCES tecnico ( id_tecnico )
            ON DELETE CASCADE;


ALTER TABLE competencia
    ADD CONSTRAINT competencia_e_c_fk FOREIGN KEY ( id_e_c )
        REFERENCES e_c ( id_e_c )
            ON DELETE CASCADE;

ALTER TABLE competencia
    ADD CONSTRAINT competencia_pais_fk FOREIGN KEY ( id_pais )
        REFERENCES pais ( id_pais )
            ON DELETE CASCADE;

ALTER TABLE equipo
    ADD CONSTRAINT equipo_pais_fk FOREIGN KEY ( id_pais )
        REFERENCES pais ( id_pais )
            ON DELETE CASCADE;

ALTER TABLE equipo
    ADD CONSTRAINT equipo_t_e_fk FOREIGN KEY ( id_t_e )
        REFERENCES t_e ( id_t_e )
            ON DELETE CASCADE;

ALTER TABLE estadio
    ADD CONSTRAINT estadio_pais_fk FOREIGN KEY ( id_pais )
        REFERENCES pais ( id_pais )
            ON DELETE CASCADE;

ALTER TABLE gol
    ADD CONSTRAINT gol_incidencia_fk FOREIGN KEY ( id_incidencia )
        REFERENCES incidencia ( id_incidencia );


ALTER TABLE h_m
    ADD CONSTRAINT h_m_membresia_fk FOREIGN KEY ( id_membresia )
        REFERENCES membresia ( id_membresia )
            ON DELETE CASCADE;

ALTER TABLE h_m
    ADD CONSTRAINT h_m_usuario_fk FOREIGN KEY ( id_usuario )
        REFERENCES usuario ( id_usuario )
            ON DELETE CASCADE;

ALTER TABLE incidencia
    ADD CONSTRAINT incidencia_jugador_fk FOREIGN KEY ( id_jugador )
        REFERENCES jugador ( id_jugador )
            ON DELETE CASCADE;

ALTER TABLE incidencia
    ADD CONSTRAINT incidencia_partido_fk FOREIGN KEY ( id_partido )
        REFERENCES partido ( id_partido )
            ON DELETE CASCADE;

ALTER TABLE incidencia
    ADD CONSTRAINT incidencia_t_i_fk FOREIGN KEY ( id_t_i )
        REFERENCES t_i ( id_t_i )
            ON DELETE CASCADE;

ALTER TABLE jugador
    ADD CONSTRAINT jugador_e_j_fk FOREIGN KEY ( id_e_j )
        REFERENCES e_j ( id_e_j )
            ON DELETE CASCADE;

ALTER TABLE jugador
    ADD CONSTRAINT jugador_pais_fk FOREIGN KEY ( id_pais )
        REFERENCES pais ( id_pais )
            ON DELETE CASCADE;

ALTER TABLE jugador
    ADD CONSTRAINT jugador_posicion_fk FOREIGN KEY ( id_posicion )
        REFERENCES posicion ( id_posicion )
            ON DELETE CASCADE;

ALTER TABLE log
    ADD CONSTRAINT log_usuario_fk FOREIGN KEY ( id_usuario )
        REFERENCES usuario ( id_usuario )
            ON DELETE CASCADE;

ALTER TABLE noticia
    ADD CONSTRAINT noticia_equipo_fk FOREIGN KEY ( id_equipo )
        REFERENCES equipo ( id_equipo )
            ON DELETE CASCADE;

ALTER TABLE noticia
    ADD CONSTRAINT noticia_usuario_fk FOREIGN KEY ( id_usuario )
        REFERENCES usuario ( id_usuario )
            ON DELETE CASCADE;

ALTER TABLE partido
    ADD CONSTRAINT partido_arbitro_fk FOREIGN KEY ( id_arbitro )
        REFERENCES arbitro ( id_arbitro )
            ON DELETE CASCADE;

ALTER TABLE partido
    ADD CONSTRAINT partido_competencia_fk FOREIGN KEY ( id_competencia )
        REFERENCES competencia ( id_competencia )
            ON DELETE CASCADE;

ALTER TABLE partido
    ADD CONSTRAINT partido_e_p_fk FOREIGN KEY ( id_e_p )
        REFERENCES e_p ( id_e_p )
            ON DELETE CASCADE;

ALTER TABLE partido
    ADD CONSTRAINT partido_eq_locla_fk FOREIGN KEY ( eq_local )
        REFERENCES equipo ( id_equipo )
            ON DELETE CASCADE;

ALTER TABLE partido
    ADD CONSTRAINT partido_eq_visit_fk FOREIGN KEY ( eq_visit )
        REFERENCES equipo ( id_equipo )
            ON DELETE CASCADE;

ALTER TABLE partido
    ADD CONSTRAINT partido_estadio_fk FOREIGN KEY ( id_estadio )
        REFERENCES estadio ( id_estadio )
            ON DELETE CASCADE;

ALTER TABLE penal
    ADD CONSTRAINT penal_gol_fk FOREIGN KEY ( id_incidencia )
        REFERENCES gol ( id_incidencia );

ALTER TABLE penal
    ADD CONSTRAINT penal_jug_sufre_fk FOREIGN KEY ( jug_sufre )
        REFERENCES jugador ( id_jugador )
            ON DELETE CASCADE;

ALTER TABLE penal
    ADD CONSTRAINT penal_jug_comete_fk FOREIGN KEY ( jug_comete )
        REFERENCES jugador ( id_jugador )
            ON DELETE CASCADE;

ALTER TABLE quiniela
    ADD CONSTRAINT quiniela_partido_fk FOREIGN KEY ( id_partido )
        REFERENCES partido ( id_partido );

ALTER TABLE quiniela
    ADD CONSTRAINT quiniela_usuario_fk FOREIGN KEY ( id_usuario )
        REFERENCES usuario ( id_usuario )
            ON DELETE CASCADE;

ALTER TABLE suscripcion
    ADD CONSTRAINT suscripcion_e_s_fk FOREIGN KEY ( id_e_s )
        REFERENCES e_s ( id_e_s )
            ON DELETE CASCADE;

ALTER TABLE suscripcion
    ADD CONSTRAINT suscripcion_equipo_fk FOREIGN KEY ( id_equipo )
        REFERENCES equipo ( id_equipo )
            ON DELETE CASCADE;

ALTER TABLE suscripcion
    ADD CONSTRAINT suscripcion_usuario_fk FOREIGN KEY ( id_usuario )
        REFERENCES usuario ( id_usuario )
            ON DELETE CASCADE;

ALTER TABLE tarjeta
    ADD CONSTRAINT tarjeta_color_fk FOREIGN KEY ( id_color )
        REFERENCES color ( id_color )
            ON DELETE CASCADE;

ALTER TABLE tarjeta
    ADD CONSTRAINT tarjeta_incidencia_fk FOREIGN KEY ( id_incidencia )
        REFERENCES incidencia ( id_incidencia );

ALTER TABLE usuario
    ADD CONSTRAINT usuario_e_u_fk FOREIGN KEY ( id_e_u )
        REFERENCES e_u ( id_e_u )
            ON DELETE CASCADE;

ALTER TABLE usuario
    ADD CONSTRAINT usuario_genero_fk FOREIGN KEY ( id_genero )
        REFERENCES genero ( id_genero )
            ON DELETE CASCADE;

ALTER TABLE usuario
    ADD CONSTRAINT usuario_pais_fk FOREIGN KEY ( id_pais )
        REFERENCES pais ( id_pais )
            ON DELETE CASCADE;

ALTER TABLE usuario
    ADD CONSTRAINT usuario_rol_fk FOREIGN KEY ( id_rol )
        REFERENCES rol ( id_rol )
            ON DELETE CASCADE;
