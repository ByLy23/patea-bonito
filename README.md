# Proyecto - G8 - Software Avanzado - Diciembre 2022

> ## Integrantes

|Nombre|Carné|
|:- |:- |
|Christtopher Jose Chitay Coutino|201113851|
|Joel Estuardo Rodríguez Santos|201115018|
|Byron Antonio Orellana Alburez|201700733|
|Marvin Daniel Rodriguez Felix |201709450|
|Audrie Annelisse del Cid Ochoa|201801263|


> ## Tabla de contenidos
- [Actividades](./file/files/actividades_diagramas.pdf)
- [Algoritmo de predicción](./file/documentacion/algoritmo.md)
- [Arquitectura a implementar](./file/files/Arqui%20SA.pdf)
- [CDU](./file/files/cdu.pdf)
- [Diagrama de Datos](./file/files/relacional.pdf)
- [Endpoints](https://docs.google.com/spreadsheets/d/1OoENL-3JinLMuTC_8aLfJNRAaeG4ZsZ1rK_Ze9_R5KY/edit#gid=0)
- [Herramientas de desarrollo](./file/documentacion/herramientas_desarrollo.md)
- [Merramientas de metodología](./file/documentacion/herramientas_metodologia.md)
- [Herramientas de control de tiempo de trabajo](./file/documentacion/herramientas_tiempo.md)
- [Historias de usuario](./file/files/Historias%20de%20Usuario.pdf)
- [Lenguajes de programación](./file/documentacion/lenguaje_programacion.md)
- [Mockups](./file/files/mockups.pdf)
- [Pruebas a implementar](./file/documentacion/pruebas.md)
- [Resumen de proyecto](#resumen)
- [Requerimientos](./file/documentacion/requerimientos.md)
- [Swagger](https://app.swaggerhub.com/apis/Software-avanzado-g8/endpoints/1.0.0)






<a name="resumen"/>

> ### Vídeo
https://youtu.be/uV16zE1yqRk

> ## Resumen Aplicación "Pateo Bonito"
Esta aplicación fue creada con el proposito de permitir la disposición de datos a los usuarios, mediante la implementación de un sistema óptimo para un negocio. 

Esta aplicación se presenta mediante un sistema web, el cual cuenta con diversos modulos como la pagina de inicio, la cual contiene la información relevante de la empresa y video de presentación, así como la información de contacto. También se cuenta con un apartado para el inicio de sesión de los usuarios y el registro de estos. 

Dicha página cuenta con la opción de acceso para diferentes roles, como lo son: El administrador del Sitio, Empleado y cliente. 

Para los diversos usuarios con estos roles, la aplicación les brinda una variedad de herramientas y accesos diferentes. 

Para los usuarios podemos encontrar la opción de registro, inicio de sesión, visualización de información, así como las ventajas de organización que cuenta esta aplicación, permite gestionar sus datos de registros y adquirir una membresía. 

Los beneficios que se pueden encontrar en la aplicación al adquirir una mmembresía se detallan a continuación:

Con la membresía los usuarios podran consultar predicciones, realizar quinielas, modificarlas, además que pueden ganar premios al acertar quinielas, también pueden ver noticias de los equipos, seguir a sus equipos favoritos, entre muchas cosas más. 

Al activar tu membresía, la aplicació también te da las opciones de cancelación en cualquier momento, así como formas de pago para continuar con dicha membresía. 

Una vez descritas las opciones que los usuarios pueden contar, también nos encontramos con el módulo de empleados, aquí el sistema permite a los usuarios gestionar información estadística relacionados al futbol, transferir jugadores a diversos equipos, ver historial de equipos, etc. 
En otras palabras el sistema facilita al empleado el ingreso y gestión de todos los datos que los usuarios pueden consumir en la aplicación. 

Por último también tenemos el módulo de administradores, en este módulo permite la facilidad de administración y gestión de accesos para los nuevos integrantes de la aplicación, así como de los empleados. También como los empleados en este módulo se permite la libre gestión e ingreso de todos los datos que los usuarios pueden consumir en la aplicación.

Para finalizar esta aplicación cuenta con tecnologías y herramientas que le permiten brindar un servicio óptimo, seguro y rápido. 

