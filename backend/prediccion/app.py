from bd import obtener_conexion
# from app import app
# from config import mysql
from flask import jsonify
from flask import Flask, request, make_response
from flask_cors import CORS, cross_origin
from algoritmo import predict
class Nodo:
    
    def __init__(self, nombre, resultado,golesFavor,golesContra,equiposCombatidos):
        self.nombre=nombre
        self.resultado=resultado
        self.golesFavor=golesFavor
        self.golesContra=golesContra
        self.equiposCombatidos=equiposCombatidos


app= Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route("/", methods=['GET'])
def index():
    return 'OK!'


@app.route("/algoritmo/prediccion/<int:id_partido>", methods=['GET'])
def quiniela_obtencion(id_partido):
    try:
        partido=  id_partido
        print(partido)
        conexion = obtener_conexion()
        result =[]
        with conexion.cursor() as cursor:
            cursor.execute("CALL obtener_prediccion(%s)",(partido))
            result = cursor.fetchall()
            data = []
            for row in result:
                data.append({
                    'id_prediccion': row[0],
                    'gol_loc': row[1],
                    'gol_vis': row[2],
                    'equipo_loc': row[3],
                    'equipo_vis': row[4],
                })
        conexion.close()
        return jsonify(data)
    except Exception as e:
        return make_response(jsonify({'error': str(e)}), 500)

@app.route("/algoritmo/prediccion/", methods=['POST'])
def quiniela_prediccion():
    try:
        conexion = obtener_conexion()
        result =[]
        _json = request.json
        eqp1 = _json['eqp1']# 5
        eqp2 = _json['eqp2']# 8
        partido = _json['id_partido']# 3
        print(eqp1)
        print(eqp2)
        print(partido)
        #AQUI SE HACEN LAS PETICIONES PARA OBTENER LOS DATOS -----------------------
        with conexion.cursor() as cursor:
            cursor.execute("CALL obtener_datos_prediccion(%s,%s)",(eqp1,eqp2))
            result = cursor.fetchall()
            data= result[0]
        conexion.close()
        print(data)
        nombreEqp1 = data[0]
        nombreEqp2 = data[5]
        resultadosEqp1 = int(data[3])
        resultadosEqp2 = int(data[8])
        golesFavorEqp1 = int(data[1])
        golesFavorEqp2 = int(data[6])
        golesContraEqp1 = int(data[2])
        golesContraEqp2 = int(data[7])
        equiposCombatidosEqp1 = int(data[4])
        equiposCombatidosEqp2 = int(data[9])
        nodo1=Nodo(nombreEqp1,resultadosEqp1,golesFavorEqp1,golesContraEqp1,equiposCombatidosEqp1)
        nodo2=Nodo(nombreEqp2,resultadosEqp2,golesFavorEqp2,golesContraEqp2,equiposCombatidosEqp2)
        #AQUI SE HACE LA PREDICCION ----------------------------------------------
        resultadoPrediccion = predict(nodo1, nodo2)
        respuesta={
            "Equipo_1":resultadoPrediccion.equipo1,
            "Equipo_2":resultadoPrediccion.equipo2,
            "Resultado_Equipo_1":resultadoPrediccion.resultadoEquipo1,
            "Resultado_Equipo_2":resultadoPrediccion.resultadoEquipo2,
            "Ganador":resultadoPrediccion.ganador
        }
        print(respuesta)
        conexion = obtener_conexion()
        with conexion.cursor() as cursor:
            cursor.execute("CALL registrar_prediccion(%s,%s,%s)",(partido,resultadoPrediccion.resultadoEquipo1,resultadoPrediccion.resultadoEquipo2))
            conexion.commit()
        conexion.close()
        
        # Eqp1 = result[1].split()
        # Eqp2 = result[2].split()        
        # respuesta ={
        #     "Equipo1": eqp1, 
        #     "Resultado_eqp1": round(float(Eqp1[2])), 
        #     "Equipo2": eqp2,
        #     "Resultado_eqp2": round(float(Eqp2[2])) 
        #     }
        # print(respuesta)
        
        return jsonify(respuesta)
        
    except Exception as e:
        print(e)
        return make_response(jsonify({'error': str(e)}), 500)
   



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=3001, debug=True)