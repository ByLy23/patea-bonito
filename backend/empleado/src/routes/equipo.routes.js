const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')

const equipo = require('../controllers/equipo.controller')

router.get('/', equipo.obtenerEquipo)
router.post('/', equipo.crearEquipo)
router.put('/', equipo.actualizarEquipo)
router.delete('/:id_equipo', equipo.eliminarEquipo)

module.exports = router
