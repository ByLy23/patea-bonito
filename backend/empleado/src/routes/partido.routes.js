const express = require("express");
const router = express.Router();

const partido = require("../controllers/partido.controller");

router.get("/", partido.obtenerPartido);

router.post("/", partido.crearPartido);

router.put("/:id_partido", partido.actualizarPartido);

router.delete("/:id_partido", partido.eliminarPartido);

module.exports = router;
