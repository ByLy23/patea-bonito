require('dotenv/config')
const bcrypt = require('bcrypt')
const db = require('../db/conexion')
const jwt = require('jsonwebtoken')
const moment = require('moment')
/**
 * @param {*} fecha_nac date,
 * @param {*} nombre VARCHAR(100),
 * @param {*} id_pais INT
 */
exports.crearArbitro = async (req, res) => {
  const { fecha_nac, nombre, id_pais } = req.body

  if (!fecha_nac || !nombre || !id_pais) {
    return res.status(400).json({ message: 'Faltan datos' })
  }
  // let fecha = moment(fecha_nac).format("DD-MM-YYYY");
  try {
    const sql = `CALL crear_arbitro(?,?,?)`
    const params = [fecha_nac, nombre, id_pais]
    const result = await db.query(sql, params)
    res.status(200).json({ message: result[0][0].message })
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al crear arbitro' })
  }
}

exports.obtenerArbitro = async (req, res) => {
  try {
    const sql = `CALL obtener_arbitros()`
    const result = await db.query(sql)
    res.status(200).json(result[0])
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al obtener arbitros' })
  }
}

/**
 * @param {*} nombre VARCHAR(100),
 * @param {*} id_arbitro INT
 */
exports.actualizarArbitro = async (req, res) => {
  const { nombre, id_arbitro } = req.body

  if (!nombre || !id_arbitro) {
    return res.status(400).json({ message: 'Faltan datos' })
  }

  try {
    const sql = `CALL actualizar_arbitro(?,?)`
    const params = [nombre, id_arbitro]
    const result = await db.query(sql, params)
    res.status(200).json({ message: result[0][0].message })
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al actualizar arbitro' })
  }
}

/**
 * @param {*} id_arbitro INT
 */
exports.eliminarArbitro = async (req, res) => {
  const { id_arbitro } = req.body

  if (!id_arbitro) {
    return res.status(400).json({ message: 'Faltan datos' })
  }

  try {
    const sql = `CALL eliminar_arbitro(?)`
    const params = [id_arbitro]
    const result = await db.query(sql, params)
    res.status(200).json({ message: result[0][0].message })
  } catch (error) {
    console.log(error)
    res.status(400).json({ message: 'Error al eliminar arbitro' })
  }
}
