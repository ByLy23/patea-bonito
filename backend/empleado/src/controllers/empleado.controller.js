const db = require("../db/conexion");

/**
 *  @param {*} rol INT,
 *  @body {*}  id_jugador || id_tecnico INT,
 *  @body {*}  id_equipo INT,
 *  /empleado/transferir/:rol
 */

exports.transferir = async (req, res) => {
  const rol = req.params.rol;
  const id = req.body.id_jugador || req.body.id_tecnico;
  const { id_equipo } = req.body;

  if (!id || !id_equipo || !rol) {
    console.log(rol, id, id_equipo);
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL transferir_jugador_tecnico(?,?,?)`;
    const params = [rol, id, id_equipo];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al transferir" });
  }
};

/**
 * @param {*} rol INT
 * /empleado/bitacora/:rol
 */

exports.bitacora = async (req, res) => {
  const { rol, id_deportista } = req.params;
  console.log(rol, id_deportista);
  if (!rol) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL obtener_bitacora_tecnico_jugador(?,?)`;
    const params = [rol, id_deportista];
    const result = await db.query(sql, params);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener bitacora" });
  }
};

/**
 *
 * @param {*} id_equipo int
 */
exports.jugador_equipo = async (req, res) => {
  const { id_equipo } = req.params;

  if (!id_equipo) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL obtener_jugadores_equipo(?)`;
    const params = [id_equipo];
    const result = await db.query(sql, params);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener jugadores" });
  }
};

/**
 * @body {*} id_partido : int
 * @body {*} estado: int <<1: primer, 2:descanso, 3: segundo 4: completado, 5: suspendido, 6: sin iniciar >>
 * /empleado/incidencia/estado
 */
exports.estadoPartido = async (req, res) => {
  const { id_partido, estado } = req.body;
  if (!id_partido || !estado) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL cambiar_estado_partido(?,?)`;
    const params = [id_partido, estado];
    const result = await db.query(sql, params);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al cambiar estado" });
  }
};

/** AGREGAR GOL
 * id_partido int,
 * id_jugador int,
 * minuto int,
 * id_equipo int,
 * tipo int, <<2: autogol, 3: tiro, 4: jugada>>
 * distancia: int,
 * agregar_gol_partido
 */

exports.agregar_gol = async (req, res) => {
  const { id_partido, id_jugador, minuto, id_equipo, tipo, distancia } =
    req.body;
  if (
    !id_partido ||
    !id_jugador ||
    !minuto ||
    !id_equipo ||
    !tipo ||
    !distancia
  ) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL agregar_gol_partido(?,?,?,?,?,?)`;
    const params = [id_partido, id_jugador, minuto, id_equipo, tipo, distancia];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al agregar gol" });
  }
};

/** AGREGAR INCIDENCIA TARJETA
 * id_partido int,
 * id_jugador int,
 * minuto int,
 * id_equipo int,
 * tipo_tarjeta int, <<1: amarilla, 2: roja>>
 * tipo: 1
 * registrar_tarjeta_partido
 */
exports.agregar_tarjeta = async (req, res) => {
    const { id_partido, id_jugador, minuto, id_equipo, tipo_tarjeta } = req.body;
    if (!id_partido || !id_jugador || !minuto || !id_equipo || !tipo_tarjeta) {
        return res.status(400).json({ message: "Faltan datos" });
        }
    try {
        const sql = `CALL registrar_tarjeta_partido(?,?,?,?,?)`;
        const params = [id_partido, id_jugador, minuto, id_equipo, tipo_tarjeta];
        const result = await db.query(sql, params);

        res.status(200).json({ message: result[0][0].message });
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "Error al agregar tarjeta" });
    }
}


/** AGREGAR EQUIPO COMPETENCIA
 * id_equipo int,
 * id_competencia int
 * registrar_equipo_competencia
 **/
exports.agregar_equipo_competencia = async (req, res) => {
    const { id_equipo, id_competencia } = req.body;
    if (!id_equipo || !id_competencia) {
        return res.status(400).json({ message: "Faltan datos" });
        }
    try {
        const sql = `CALL registrar_equipo_competencia(?,?)`;
        const params = [id_equipo, id_competencia];
        const result = await db.query(sql, params);
        res.status(200).json({ message: result[0][0].message });
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "Error al agregar equipo a competencia" });
    }
}


/**
 * id_equipo int,
 * id_competencia int,
 * id_estado int <<1: Participante, 2: Ganador>>
 * actualizar_estado_equipo_competencia
 */
exports.actualizar_equipo_competencia = async (req, res) => {
    const { id_equipo,id_competencia, id_estado } = req.body;
    if (!id_equipo || !id_competencia || !id_estado) {
        return res.status(400).json({ message: "Faltan datos" });
        }
    try {
        const sql = `CALL actualizar_estado_equipo_competencia(?,?,?)`;
        const params = [id_equipo,id_competencia, id_estado];
        const result = await db.query(sql, params);
        res.status(200).json({ message: result[0][0].message });
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "Error al actualizar estado equipo competencia" });
    }
}


/**
 * @param {*} id_competencia : int
 * @param {*} opcion : int <<1: si esta, 2: no esta>>
 * obtener_equipos_competencia
 */
exports.obtener_equipos_competencia = async (req, res) => {
    const { id_competencia, opcion } = req.params;
    if (!id_competencia || !opcion) {
        return res.status(400).json({ message: "Faltan datos" });
        }
    try {
        const sql = `CALL obtener_equipos_competencia(?,?)`;
        const params = [id_competencia, opcion];
        const result = await db.query(sql, params);
        res.status(200).json(result[0]);
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "Error al obtener equipos competencia" });
    }
}

/** CREAR PREDICCION
 * 
 */