const req = require("supertest");
const { SHA256 } = require("crypto-js");


describe("PRUEBA UNITARIA MEMBRESIA /", () => {
  let token = "";
  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/sesion/iniciar")
      .send({
        email: "bbowserl@go.com",
        password:"DsHkCbO7ojB",
        num_grupo: 8,
      })
      .end((err, response) => {
       //console.log(response.body);
        token = response.body.token; // save the token!
        done();
      });
  });
    test("Este test verificara el historial de una membresia", async () => {
      const result = await req("http://localhost:3002")
        .get("/cliente/visualizar/membresias/2")
        .set("Authorization", `Bearer ${token}`)
        .send();
      //expect(typeof result.body.user.id_usuario).toBe("number");
      expect(result.statusCode).toBe(200);
    });
  });
  
  describe("PRUEBA UNITARIA MEMBRESIA /", () => {
    let token = "";
  beforeAll((done) => {
    req("http://localhost:3005")
      .post("/api/sesion/iniciar")
      .send({
        email: "bbowserl@go.com",
        password:"DsHkCbO7ojB",
        num_grupo: 8,
      })
      .end((err, response) => {
        //console.log(response.body);
        token = response.body.token; // save the token!
        done();
      });
  });
    test("Este test verificara la membresia de un usuario", async () => {
      const result = await req("http://localhost:3002")
        .get("/cliente/membresia/2")
        .set("Authorization" , `Bearer ${token}`)
        .send();
        expect(result.statusCode).toBe(200);
    })
})