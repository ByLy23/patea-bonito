require("dotenv/config");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");

/**
 * @param {*} id_usuario INT
 */
exports.get_EquipoNoSubscrito = async (req, res) => {
  const { id_usuario } = req.params;
  if (!id_usuario) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL obtener_equipo_no_suscritos(?)`;
    const params = [id_usuario];
    const result = await db.query(sql, params);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener lista equipos" });
  }
};

/**
 * @param {*} id_equipo INT,
 * @param {*} id_usuario INT
 */
exports.SubscripcionEquipo = async (req, res) => {
  const { id_equipo, id_usuario } = req.body;
  console.log(req.body);
  if (!id_equipo || !id_usuario) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL suscribirse_a_equipo(?,?)`;
    const params = [id_usuario, id_equipo];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al subscribir" });
  }
};

/**
 * @param {*} id_usuario INT
 * ver_membresia
 */
exports.verNoticia_Subscripcion = async (req, res) => {
  const { id_usuario } = req.params;
  if (!id_usuario) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL obtener_noticias_de_suscripciones(?)`;
    const params = [id_usuario];
    const result = await db.query(sql, params);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener Noticias" });
  }
};
