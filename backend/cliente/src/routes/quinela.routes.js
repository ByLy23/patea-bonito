const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const quinela = require("../controllers/quinela.controller");

router.post("/", quinela.crear_Quinelas);

router.put("/", quinela.actualizar_Quinelas);

router.get("/:id_usuario", quinela.obtener_Quinela);

// router.get("/jugador2", estadistica.consultar_Jugadores2);

// router.get(
//   "/jugador3/:id_deportista/:tipo_deportista",
//   estadistica.consultar_Jugadores3
// );

// router.get("/tecnico", estadistica.consultar_tecnico);

// router.get("/partido", estadistica.consultar_partido);

// router.get("/estadio", estadistica.consultar_estadio);

// router.get("/equipo", estadistica.consultar_equipo);

// router.get("/competencia/:id_competencia", estadistica.consultar_competencia1);

module.exports = router;
