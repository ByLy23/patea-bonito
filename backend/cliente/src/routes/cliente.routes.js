const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const cliente = require("../controllers/cliente.controller");

router.put("/actualizar/:id_usuario", cliente.actualizar);

router.get("/visualizar/partidos", cliente.visualizar_partidos);

module.exports = router;