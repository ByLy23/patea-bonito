const express = require("express");
const router = express.Router();

const esb = require("../controllers/esb.controller");

router.get("/:grupo",esb.obtener_ips)

module.exports = router;