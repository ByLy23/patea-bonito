const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const register = require("../controllers/register.controller");

router.post("/crear", register.register);

module.exports = router;
