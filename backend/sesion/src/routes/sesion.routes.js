const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const login = require("../controllers/sesion.controller");

router.post("/iniciar", login.logIn);

router.post("/crear", login.register);

router.post("/recovery", login.forgotPassword);

router.get("/confirmar/:token", login.activeAccount);

router.post("/recuperar", login.forgotPassword);

router.put("/updatePass",login.update_password)

module.exports = router;