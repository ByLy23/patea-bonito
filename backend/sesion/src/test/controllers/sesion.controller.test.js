const req = require("supertest");
const { SHA256 } = require("crypto-js");

describe("PRUEBA INICIO SESION", () => {
  test("Este test verifica el inicio de sesion", async () => {
    const result = await req("http://localhost:3005")
      .post("/api/sesion/iniciar")
      .send({
        email: "admin@gmail.com",
        password: "admin",
        num_grupo: 8,
      });
    console.log(result.body);
    expect(result.body.message).toBe("Inicio de sesion exitoso");
    expect(result.statusCode).toBe(200);
  });
});

////
