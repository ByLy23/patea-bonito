const db = require("../db/conexion2");

/**
 * obtener_ips
 */
exports.obtener_ips = async (req, res) => {
  try {
    const { grupo } = req.params;
    const sql = `CALL obtener_direccion(?)`;
    const result = await db.query(sql, [grupo]);
    res.json({url: result[0][0].direccion, grupo: result[0][0].num_grupo});
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: error.sqlMessage });
  }
};
