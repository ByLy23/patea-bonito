require("dotenv/config");
const bcrypt = require("bcrypt");
const db = require("../db/conexion");
const jwt = require("jsonwebtoken");

/**
 * @param {*} nombre VARCHAR(100),
 * @param {*} capacidad INT,
 * @param {*} id_pais INT
 */
exports.crearEstadios = async (req, res) => {
  const { nombre, capacidad, id_pais } = req.body;

  if (!nombre || !capacidad || !id_pais) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL crear_estadio(?,?,?)`;
    const params = [nombre, capacidad, id_pais];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al crear estadio" });
  }
};

exports.obtenerEstadio = async (req, res) => {
  try {
    const sql = `CALL obtener_estadios()`;
    const result = await db.query(sql);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener estadios" });
  }
};

/**
 * @param {*} nombre VARCHAR(100),
 * @param {*} capacidad INT,
 * @param {*} id_estadio INT
 */
exports.actualizarEstadio = async (req, res) => {
  const { nombre, capacidad, id_estadio } = req.body;

  if (!nombre || !capacidad || !id_estadio) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL actualizar_estadio(?,?,?)`;
    const params = [id_estadio, nombre, capacidad];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al actualizar estadio" });
  }
};

/**
 * @param {*} id_estadio INT
 */
exports.eliminarEstadio = async (req, res) => {
  const { id_estadio } = req.params;

  if (!id_estadio) {
    return res.status(400).json({ message: "Faltan datos" });
  }

  try {
    const sql = `CALL eliminar_estadio(?)`;
    const params = [id_estadio];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al eliminar estadio" });
  }
};
