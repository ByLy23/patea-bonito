require("dotenv").config();
const db = require("../db/conexion");


/**
 * @param {*} id_grupo : int
 * obtener_reporte_admin
 */
exports.get = async (req, res,next) => {
    const { id_grupo } = req.params;
    if(!id_grupo) return res.status(400).json({ message: "Faltan datos" })
    try {
        const sql = `CALL obtener_reporte_admin(?)`;
        const params = [id_grupo];
        const result = await db.query(sql, params);
        res.status(200).json({ message: "reporte obtenido con exito", data: result[0]})
        req.accion = `Administrador consulto el reporte de admin del grupo ${id_grupo}`
        req.num_grupo = req.grupo;
        next();
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: "Error al obtener el reporte" })
    }

}