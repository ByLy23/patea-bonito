require("dotenv/config");
const aws = require("aws-sdk");
const { awsKeyS3 } = require("../credentials/credentials");
const db = require("../db/conexion");
const { SHA256 } = require("crypto-js");

/**
 * @param {*} id_usuarioIN INT,
 * @param {*} id_estadoIN INT int << 1 : de alta, 2: de baja, 3: congelada
 */
exports.updateEstado = async (req, res,next) => {
  const { id_usuario, id_estado } = req.body;
  if (!id_usuario || !id_estado) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL actualizar_estado_usuario(?,?)`;
    const params = [id_usuario, id_estado];
    const result = await db.query(sql, params);
    res.status(200).json({ message: result[0][0].message });
    req.accion = `Se actualizo el estado del usuario ${id_usuario}`
    req.num_grupo = req.grupo;
    next();
  } catch (error) {
    console.log(error);
    res
      .status(400)
      .json({ message: "Error al actualizar el estado del usuario" });
  }
};

/**
 * @param {*} id_usuario INT,
 * @param {*} nombre VARCHAR(100),
 * @param {*} apellido VARCHAR(100),
 * @param {*} telefono VARCHAR(12),
 * @param {*} direccion VARCHAR(200)
 * actualizar_informacion_usuario
 */

exports.update = async (req, res,next) => {
  const { id_usuario, nombre, apellido, telefono, direccion,num_grupo } = req.body;
  if (!id_usuario || !nombre || !apellido || !telefono || !direccion) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL actualizar_informacion_usuario(?,?,?,?,?)`;
    const params = [id_usuario, nombre, apellido, telefono, direccion];
    const result = await db.query(sql, params);
    //console.log(result[1].affectedRows);
    res.status(200).json({ message: "Usuario Actualizado con Exito" });
    req.accion = `Se actualizo informacion de usuario ${id_usuario}`
    req.num_grupo = req.grupo;
    next();
  } catch (error) {
    console.log(error);
    res
      .status(400)
      .json({ message: "Error al actualizar la informacion del usuario" });
  }
};

/**
 * @param {*} rol INT,
 * obtener_usuarios
 */
exports.get = async (req, res) => {
  const { rol } = req.params;
  if (!rol) {
    return res.status(400).json({ message: "Faltan datos" });
  }
  try {
    const sql = `CALL obtener_usuarios(?)`;
    const params = [rol];
    const result = await db.query(sql, params);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: "Error al obtener los usuarios" });
  }
};

const uploadFile = async (image, imageExt, correo) => {
  try {
    const nombre = correo.split(".");
    const path = `${nombre[0]}_foto_perfil.${imageExt}`;
    var fullPath = path.replace("@", "%40");
    fullPath = "https://tallerpi.s3.amazonaws.com/" + fullPath;
    let buff = new Buffer.from(image, "base64");

    aws.config.update({
      region: awsKeyS3.region,
      accessKeyId: awsKeyS3.accessKeyId,
      secretAccessKey: awsKeyS3.secretAccessKey,
    });

    var s3 = new aws.S3();
    const params = {
      Bucket: "tallerpi",
      Key: path,
      Body: buff,
      ContentType: "image",
    };

    const result = s3.putObject(params).promise();
    return fullPath;
  } catch (error) {
    console.log(error);
    return "error";
  }
};
