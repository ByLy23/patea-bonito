const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const pais = require("../controllers/pais.controller");

router.get("/", pais.get);


module.exports = router;
