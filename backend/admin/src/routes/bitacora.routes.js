const express = require('express')
const router = express.Router()

const bitacora = require('../controllers/bitacora.controller')

router.get('/:id_grupo', bitacora.get)


module.exports = router