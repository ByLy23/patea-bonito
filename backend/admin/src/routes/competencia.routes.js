const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')

const competencia = require('../controllers/competencia.controller')

router.get('/', competencia.obtenerCompetencia)
router.post('/', competencia.crearCompetencia)
router.put('/', competencia.actualizarCompetencia)
router.delete('/:id_competencia', competencia.eliminarCompetencia)

module.exports = router
