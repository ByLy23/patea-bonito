const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const estadio = require("../controllers/estadio.controller");

router.post("/", estadio.crearEstadios);
router.get("/", estadio.obtenerEstadio);
router.put("/", estadio.actualizarEstadio);
router.delete("/:id_estadio", estadio.eliminarEstadio);

module.exports = router;
