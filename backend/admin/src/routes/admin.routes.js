const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

const admin = require("../controllers/admin.controller");

router.put("/estado", admin.updateEstado);

// router.post("/",admin.register);

router.put("/", admin.update);

router.get("/:rol", admin.get);

module.exports = router;
